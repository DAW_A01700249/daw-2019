<?php
    session_start();
    require_once("../Models/modelRegistroEquipo.php");
    $arrayJugadoresTentativo= htmlspecialchars($_POST["arrayJugadoresTentativo"]);
    $tamañoEquipoTentativo= htmlspecialchars($_POST["tamañoEquipoTentativo"]);
    $arrayNuevoTentativo = json_decode($arrayJugadoresTentativo, true);
    $hoyoInicialTentativo=0;
    echo registrarEquipoTentativo($arrayNuevoTentativo,$hoyoInicialTentativo,$tamañoEquipoTentativo);
?>