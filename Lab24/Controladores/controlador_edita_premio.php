<?php
session_start();
require_once("../Models/modelEditarPremios.php");

//Envías toda la info que tomaste de la vista agregar, lo que está dentro del post es el id
$tipo = htmlspecialchars($_POST["tipoEdit"]);
$descripcion = htmlspecialchars($_POST["descripcionEdit"]);
$posicion = htmlspecialchars($_POST["posicionEdit"]);
$hoyo = htmlspecialchars($_POST["hoyoEdit"]);
$idTorneo = htmlspecialchars($_POST["idTorneo"]);
$idPremio = htmlspecialchars($_POST["idPremio"]);

    editarPremio($idPremio, $tipo, $descripcion, $posicion, $hoyo, $idTorneo);

?>