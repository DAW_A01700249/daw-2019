<?php
    session_start();
    require_once("../Models/modelRegistro.php");
    $nombre = htmlspecialchars($_POST["nombre"]);
    $apellidoP = htmlspecialchars($_POST["apellidoP"]);
    $apellidoM = htmlspecialchars($_POST["apellidoM"]);
    $telefono = htmlspecialchars($_POST["telefono"]);
    $email = htmlspecialchars($_POST["email"]);
    $username = htmlspecialchars($_POST["username"]);
    $password = password_hash($_POST["password"],PASSWORD_DEFAULT);
    echo insertarOrganizador($nombre,$apellidoP,$apellidoM,$telefono,$email,$username,$password);
?>