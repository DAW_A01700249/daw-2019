<?php
  session_start();
  require_once("../Models/modelEditarPremios.php");  

  $torneo = htmlspecialchars($_POST["torneoAgregar"]);
  $tipoPremio = htmlspecialchars($_POST["tipoPremioAgregar"]);

  $descripcion = htmlspecialchars($_POST["descripcionAgregar"]);
  $posicion = htmlspecialchars($_POST["posicionAgregar"]);
  $hoyo = htmlspecialchars($_POST["hoyoAgregar"]);

  echo agregarPremio($tipoPremio, $descripcion, $hoyo, $posicion, $torneo);

?>