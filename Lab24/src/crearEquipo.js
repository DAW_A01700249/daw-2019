

let contador = 0;
let jugadores = [];
let infoJugadores = [];

function validarEquipo() {
   let textoFaltante = "";
   let flag = 1;
   let hoyoInicial = $("#hoyoInicial").val();
   if (hoyoInicial.length < 1) {
      textoFaltante += "<li>Introducir el hoyo inicial</li>";
      flag = 0;
   }
   if (jugadores.length == 0) {
      textoFaltante += "<li>Introducir participantes al equipo</li>";
      flag = 0;
   }
   if (flag == 0) {
      let mensaje = document.getElementById('registro');
      let botonConfirmar = document.getElementById('registrarEquipo');
      mensaje.innerHTML = "Por favor corregir los siguientes campos:";
      document.getElementById('contenidoConfirmacion').innerHTML = textoFaltante;
      mensaje.style.display = "block";
      botonConfirmar.style.display = "none";

   } else {
      let botonConfirmar = document.getElementById('registrarEquipo');
      let mensaje = document.getElementById('registro');
      mensaje.innerHTML = "¿Confirmar inscripción?";
      document.getElementById('contenidoConfirmacion').innerHTML = "Presione confirmar para realizar la inscripcion";
      botonConfirmar.style.display = "block";
   }
}
function actualizarSeleccionados() {
   for (let x in jugadores) {
      let idBoton = "boton";
      idBoton += jugadores[x];
      let idTabla = "participante";
      idTabla += jugadores[x];
      let rowParticipante= document.getElementById(idTabla);
      let botonAgregar = document.getElementById(idBoton);
      if (botonAgregar != null) {
         botonAgregar.style.display = "none";
         rowParticipante.style.backgroundColor = "rgb(240, 238, 238)"
      }
   }
}
function mostrarRegistros() {
   $.post("../Controladores/controlador_mostrar_registro.php", {
      consulta: $("#caja_busqueda").val(),
   }).done(function (data) {
      let ajaxResponse = document.getElementById('tablaParticipantes');
      ajaxResponse.innerHTML = data;
      actualizarSeleccionados();
   });
}

function leerConsulta() {
   mostrarRegistros();
}

function actualizarEquipo() {
   let $result = "";
   let x = 0;
   while (x != jugadores.length) {
      $result += infoJugadores[x];
      x = x + 1;
   }
   document.getElementById("integrantesEquipoTexto").innerHTML = $result;
}
function añadirJugador(idP, nombre) {
   if (jugadores.indexOf(idP) == -1) {
      let $info = '<li class="collection-item" id="campoJugadorEquipo">';
      $info += idP;
      $info += '-';
      $info += nombre;
      $info += '<a id="campoJugadorEquipoBoton" class="btn btn-small waves-effect waves-light pink darken-1 right align"><i onclick="eliminarJugador('
      $info += idP;
      $info += ')' + '"';
      $info += ' class="material-icons">remove_circle</i></a>';
      $info += "</li>";
      infoJugadores.push($info);
      jugadores.push(idP);
      let idTabla = "participante";
      idTabla += idP;
      let idBoton = "boton";
      idBoton += idP;
      document.getElementById(idBoton).style.display = "none";
      document.getElementById(idTabla).style.backgroundColor = "rgb(240, 238, 238)";
      actualizarEquipo();
   }
}
function eliminarJugador(idP) {
   if (jugadores.length == 1 || jugadores.indexOf(idP) == 0) {
      jugadores.shift();
      infoJugadores.shift();
   } else {
      let index1 = jugadores.indexOf(idP);
      infoJugadores.splice(index1, 1);
      jugadores.splice(index1, 1);
   }
   let idTabla = "participante";
   idTabla += idP;
   let idBoton = "boton";
   idBoton += idP;
   document.getElementById(idBoton).style.display = "block";
   document.getElementById(idTabla).style.backgroundColor = "white";
   actualizarEquipo();
}
function registrarEquipo() {
   let circulo = document.getElementById("circuloCarga_InscipcionEquipo");
   let forma = document.getElementById('formaEquipo');
   forma.style.display = "none";
   circulo.style.display = "block";

   let jugadoresEquipo = JSON.stringify(jugadores);
   $.post("../Controladores/controlador_insertar_equipo.php", {
      arrayJugadores: jugadoresEquipo,
      tamañoEquipo: jugadores.length,
      hoyoInicial: $("#hoyoInicial").val(),
   }).done(function (data) {
      console.log(data);

      if (data == 1) {
         let ajaxResponse = document.getElementById('mensajeEquipo');
         circulo.style.display = "none";
         ajaxResponse.style.display = "block";
      } else {
         let ajaxResponse = document.getElementById('mensajeEquipo');
         let titulo = document.getElementById('titulo');
         let subtitulo = document.getElementById('subtitulo');
         let confirmacion = document.getElementById('confirmacion');
         let confirmacion2 = document.getElementById('confirmacion2');
         circulo.style.display = "none";
         ajaxResponse.style.display = "block";
         titulo.innerHTML = "Error al inscribir jugador";
         subtitulo.innerHTML = "Intentelo más tarde o verifique su información."
         confirmacion.innerHTML = "-Error interno al inscribir jugador-";
         confirmacion2.innerHTML = "";
      }
   });
}

