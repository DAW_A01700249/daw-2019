
let _fotos="";
let _sexo="";
let _facturacion="";
let _metodoPago="";
var validacionDatosP=false;
var validacionDatosAdministrativos=false;
var validacionDatosJuego=false;
var validacionDatosFacturacion=false; 
$(document).ready(function(){
    var $radios = $('input[name="metodoPago"]');
    $radios.change(function () {
        _metodoPago = $radios.filter(':checked');
    });
});
$(document).ready(function () {
    var $radios = $('input[name="sexo"]');
    $radios.change(function () {
        _sexo = $radios.filter(':checked');
    });
});
$(document).ready(function () {
    var $radios = $('input[name="factura"]');
    $radios.change(function(){
        _facturacion= $radios.filter(':checked');
    });
});
$(document).ready(function () {
    var $radios = $('input[name="autorizarFotos"]');
    $radios.change(function () {
        _fotos = $radios.filter(':checked');
    });
});
function getFactura() { 
    var ele = document.getElementsByName('factura');
    let result; 
    for(i = 0; i < ele.length; i++) { 
        if(ele[i].checked){
            result=ele[i].value;
            break;
        } 
        
    }
    return result;
}
function unirDireccionFiscal(factCalle,factExterior,factInterior,factColonia,factCiudad,factEstado,factCP) {
    var res = factCalle.concat(factExterior,', ',factInterior,', ',factColonia,', ',factCiudad,', ',factEstado,', ',factCP);
    return res;
}

function validarInscripcion() {

    let textoFaltante = "<br>";
    let flag = 1;
    let nombre = $("#nombre").val();
    let aP = $("#apellidoPaterno").val();
    let aM = $("#apellidoMaterno").val();
    let sexo = _sexo;
    let edad = $("#edad").val();
    let telefono = $("#telefono").val();
    let email = $("#email").val();
    let handicap = $("#handicap").val();
    let metPago = _metodoPago;
    let fac = _facturacion;
    let autoFotos = _fotos;
    let factRS = $("#facturacion_razonSocial").val();
    let factRFC = $("#facturacion_RFC").val();
    let factCa = $("#facturacion_calle").val();
    let factExt = $("#facturacion_noExterior").val();
    let factCol = $("#facturacion_colonia").val();
    let factCd = $("#facturacion_ciudad").val();
    let factEs = $("#facturacion_estado").val();
    let factCP = $("#facturacion_CP").val();
    if (nombre.length < 1) {
        textoFaltante += "<li>Introducir valor en Nombre</li>";
        flag = 0;
    }

    if (aP.length < 1) {
        textoFaltante += "<li>Introducir valor en Apellido Paterno</li>";
        flag = 0;

    }
    if (aM.length < 1) {
        textoFaltante += "<li>Introducir valor en Apellido Materno</li>";
        flag = 0;
    }
    if (telefono.length < 1) {
        textoFaltante += "<li>Introducir valor en Teléfono</li>";
        flag = 0;
    } else {
        if (telefono.length != 10) {
            textoFaltante += "<li>El teléfono no tiene 10 digitos</li>";
            flag = 0;
        }
    }
    if (email.length < 1) {
        textoFaltante += "<li>Introducir valor en Correo electrónico</li>";
        flag = 0;
    } else {
        if (email.indexOf("@") == -1) {
            textoFaltante += "<li>El formato del correo es incorrecto</li>";
            flag = 0;
        }
    }
    if (sexo.length < 1) {
        textoFaltante += "<li>Introducir valor en Sexo</li>";
        flag = 0;
    }
    if (edad.length < 1) {
        textoFaltante += "<li>Introducir valor en Edad</li>";
        flag = 0;
    }else{
        if(edad < 1){
            textoFaltante += "<li>Valor en Edad incorrecto</li>";
            flag = 0;
        }
        if(edad > 110){
            textoFaltante += "<li>Valor en Edad incorrecto</li>";
            flag = 0;
        }
    }
    if (handicap.length < 1) {
    } else {
        if (handicap > 36) {
            textoFaltante += "<li>El Handicap no puede ser mayor a 36</li>";
            flag = 0;
        }
        if (handicap < 0) {
            textoFaltante += "<li>El Handicap no puede ser menor a 1</li>";
            flag = 0;
        }
    }
    if (metPago.length < 1) {
        textoFaltante += "<li>Introducir valor en Metodo de Pago</li>";
        flag = 0;
    }
    if (fac.length < 1) {
        textoFaltante += "<li>Introducir valor en Facturación</li>";
        flag = 0;
    }
    else {
        if (getFactura() == "Sí") {
            if (factRS.length < 1) {
                textoFaltante += "<li>Introducir valor en Razón Social</li>";
                flag = 0;
            }
            if (factRFC.length < 1) {
                textoFaltante += "<li>Introducir valor en RFC</li>";
                flag = 0;
            } else {
                if (factRFC.length > 13) {
                    textoFaltante += "<li>El RFC solo debe ser compuesto de 12 a 13 caracteres</li>";
                    flag = 0;
                }
                if (factRFC.length < 12) {
                    textoFaltante += "<li>El RFC solo debe ser compuesto de 12 a 13 caracteres</li>";
                    flag = 0;
                }
            }
            if (factCa.length < 1) {
                textoFaltante += "<li>Introducir valor en Calle</li>";
                flag = 0;
            }
            if (factExt.length < 1) {
                textoFaltante += "<li>Introducir valor en Numero Exterior</li>";
                flag = 0;
            }
            if (factCol.length < 1) {
                textoFaltante += "<li>Introducir valor en Colonia</li>";
                flag = 0;
            }
            if (factCd.length < 1) {
                textoFaltante += "<li>Introducir valor en Ciudad</li>";
                flag = 0;
            }
            if (factEs.length < 1) {
                textoFaltante += "<li>Introducir valor en Estado</li>";
                flag = 0;
            }
            if (factCP.length < 1) {
                textoFaltante += "<li>Introducir valor en Código Postal</li>";
                flag = 0;
            }
        }
    }
    if (autoFotos.length < 1) {
        textoFaltante += "<li>Introducir valor en Autorización de fotos del evento</li>";
        flag = 0;
    }


    if (flag == 0) {
        let mensaje = document.getElementById('registro');
        let botonConfirmar = document.getElementById('registrarInscripcion');
        mensaje.innerHTML = "Por favor corregir los siguientes campos:";
        document.getElementById('contenidoConfirmacion').innerHTML = textoFaltante;
        mensaje.style.display = "block";
        botonConfirmar.style.display = "none";

    } else {
        let botonConfirmar = document.getElementById('registrarInscripcion');
        let mensaje = document.getElementById('registro');
        mensaje.innerHTML = "¿Confirmar inscripción?";
        document.getElementById('contenidoConfirmacion').innerHTML = "Presione confirmar para realizar la inscripcion";
        botonConfirmar.style.display = "block";
    }
}

function resgistrarInscripcion() {
    let circulo=document.getElementById("circuloCarga_InscipcionTorneo");
    let forma = document.getElementById('forma');
    forma.style.display = "none";
    circulo.style.display="block";

    $.post("../Controladores/controlador_insertar_registro.php", {
        nombre: $("#nombre").val(),
        apellidoP: $("#apellidoPaterno").val(),
        apellidoM: $("#apellidoMaterno").val(),
        sexo: _sexo.val(),
        edad: $("#edad").val(),
        telefono: $("#telefono").val(),
        email: $("#email").val(),
        handicap: $("#handicap").val(),
        indicacionEspecial: $("#indicacion_especial").val(),
        metodoPago: _metodoPago.val(),
        facturacion:_facturacion.val(),
        autorizarFotos:_fotos.val(),
        factRazonSocial: $("#facturacion_razonSocial").val(),
        factRFC: $("#facturacion_RFC").val(),
        factDireccionFiscal: unirDireccionFiscal($("#facturacion_calle").val(), $("#facturacion_noExterior").val(), $("#facturacion_noInterior").val(), $("#facturacion_colonia").val(), $("#facturacion_ciudad").val(), $("#facturacion_estado").val(), $("#facturacion_CP").val()),
        factIndicacionEsp: $("#indicacion_especial_facturacion").val(),
        urlRecibo: $("#recibo_nombreArchivo").val(),
        indicacionEspecialPago: $("#indicacion_especial_pago").val()
    }).done(function (data) {
        if (data == 1) {
            let ajaxResponse = document.getElementById('mensaje');
            let titulo = document.getElementById('titulo');
            let subtitulo = document.getElementById('subtitulo');
            let confirmacion = document.getElementById('confirmacion');
            let confirmacion2 = document.getElementById('confirmacion2');
            circulo.style.display="none";
            ajaxResponse.style.display = "block";
            titulo.innerHTML = "¡Gracias por su apoyo!";
            subtitulo.innerHTML = "Con su donativo apoya a brindar educación y una vida digna a las niñas, jóvenes y adolescente con discapacidad intelectual que viven en Casa María Goretti IAP";
            confirmacion.innerHTML = "-Su inscripción ha sido realizada con exito-";
            confirmacion2.innerHTML = "Usted recibirá un correo de confirmación una vez que su pago haya sido validado";
            
        } else {
            let ajaxResponse = document.getElementById('mensaje');
            let titulo = document.getElementById('titulo');
            let subtitulo = document.getElementById('subtitulo');
            let confirmacion = document.getElementById('confirmacion');
            let confirmacion2 = document.getElementById('confirmacion2');
            circulo.style.display="none";
            ajaxResponse.style.display = "block";
            titulo.innerHTML = "Error al inscribir jugador";
            subtitulo.innerHTML = "Intentelo más tarde o verifique su información."
            confirmacion.innerHTML = "-Error interno al inscribir jugador-";
            confirmacion2.innerHTML = "";
        }
    });
}
function validarDatosPersonales(){
    let flag = 1;
    let nombre = $("#nombre").val();
    let aP = $("#apellidoPaterno").val();
    let aM = $("#apellidoMaterno").val();
    let sexo = _sexo;
    let edad = $("#edad").val();
    let telefono = $("#telefono").val();
    let email = $("#email").val();
    if (nombre.length < 1) {
        document.getElementById("textoNombre").innerHTML= "Llenar todos los campos";
        document.getElementById("textoNombre").style.display="block";
        flag = 0;
    }else{
        document.getElementById("textoNombre").style.display="none";
    }

    if (aP.length < 1) {
        document.getElementById("textoNombre").innerHTML= "Llenar todos los campos";
        document.getElementById("textoNombre").style.display="block";
        flag = 0;

    }else{
        document.getElementById("textoNombre").style.display="none";
    }
    if (aM.length < 1) {
        document.getElementById("textoNombre").innerHTML= "Llenar todos los campos";
        document.getElementById("textoNombre").style.display="block";
        flag = 0;
    }else{
        document.getElementById("textoNombre").style.display="none";
    }
    if (telefono.length < 1) {
        document.getElementById("textoTelefono").innerHTML= "Introducir valor en Teléfono";
        document.getElementById("textoTelefono").style.display="block";
        flag = 0;
    } else {
        if (telefono.length != 10) {
            document.getElementById("textoTelefono").innerHTML= "El teléfono no tiene 10 digitos";
            document.getElementById("textoTelefono").style.display="block";
            flag = 0;
        }else{
            document.getElementById("textoTelefono").style.display="none";
        }
    }
    if (email.length < 1) {
        document.getElementById("textoCorreo").innerHTML="Introducir valor en Correo electrónico";
        document.getElementById("textoCorreo").style.display="block";
        flag = 0;
        
    }else {
        if (email.indexOf("@") == -1) {
            document.getElementById("textoCorreo").innerHTML= "El formato del correo es incorrecto";
            document.getElementById("textoCorreo").style.display="block";
            flag = 0;
        }else{
            document.getElementById("textoCorreo").style.display="none";
        }
    }
    if (sexo.length < 1) {
        document.getElementById("textoSexo").innerHTML="Introducir valor en Sexo";
        document.getElementById("textoSexo").style.display="block";
        flag = 0;
    }else{
        document.getElementById("textoSexo").style.display="none";

    }
    if (edad.length < 1) {
        document.getElementById("textoEdad").innerHTML= "Introducir valor en Edad";
        document.getElementById("textoEdad").style.display="block";
        flag = 0;
    }else{
        if(edad < 1){
            document.getElementById("textoEdad").innerHTML= "Valor en Edad incorrecto";
            document.getElementById("textoEdad").style.display="block";
            flag = 0;
        }else{
            document.getElementById("textoEdad").style.display="none";
        }
        if(edad > 110){
            document.getElementById("textoEdad").innerHTML="Valor en Edad incorrecto";
            document.getElementById("textoEdad").style.display="block";
            flag = 0;
        }else{
            document.getElementById("textoEdad").style.display="none";
        }
    }
    if (flag == 0) {
        validacionDatosP=false;
    } else {
        validacionDatosP=true;
    }
}
function validarDatosAdministrativos(){
    let metPago = _metodoPago;
    let fac = _facturacion;
    let autoFotos = _fotos;
    let flag = 1;
    if (metPago.length < 1) {
        document.getElementById("textoMetodoPago").innerHTML= "Introducir valor en Metodo de Pago";
        document.getElementById("textoMetodoPago").style.display="block";
        flag = 0;
    }else{
        document.getElementById("textoMetodoPago").style.display="none";
    }
    if (fac.length < 1) {
        document.getElementById("textoRecibo").innerHTML= "Introducir valor en Facturación";
        document.getElementById("textoRecibo").style.display="block";
        flag = 0;
    }else{
        document.getElementById("textoRecibo").style.display="none";
    }
    if (autoFotos.length < 1) {
        document.getElementById("textoFotos").innerHTML= "Introducir valor en Autorización de fotografias";
        document.getElementById("textoFotos").style.display="block";
        flag = 0;
    }else{
        document.getElementById("textoFotos").style.display="none";
    }
    
    if (flag == 0) {
        validacionDatosAdministrativos=false;
    } else {
        validacionDatosAdministrativos=true;
    }
}
function validarDatosJuego(){
    let flag=1;
    let handicap = $("#handicap").val();
    if (handicap.length < 1) {
    } else {
        if (handicap > 36) {
            document.getElementById("textoHandicap").innerHTML= "El Handicap no puede ser mayor a 36";
            document.getElementById("textoHandicap").style.display="block";
            flag = 0;
        }else{
            document.getElementById("textoHandicap").style.display="none";
        }
        if (handicap < 0) {
            document.getElementById("textoHandicap").innerHTML= "El Handicap no puede ser menor a 0";
            document.getElementById("textoHandicap").style.display="block";
            flag = 0;
        }else{
            document.getElementById("textoHandicap").style.display="none";
        }
    }
    if (flag == 0) {
        validacionDatosJuego=false;
    } else {
        validacionDatosJuego=true;
    }

}
function validarDatosFacturacion(){
    let flag=1;
    let factRS = $("#facturacion_razonSocial").val();
    let factRFC = $("#facturacion_RFC").val();
    let factCa = $("#facturacion_calle").val();
    let factExt = $("#facturacion_noExterior").val();
    let factCol = $("#facturacion_colonia").val();
    let factCd = $("#facturacion_ciudad").val();
    let factEs = $("#facturacion_estado").val();
    let factCP = $("#facturacion_CP").val();

    if (factRS.length < 1) {
        document.getElementById("textoRazonSocial").innerHTML= "Introducir valor en Razón Social";
        document.getElementById("textoRazonSocial").style.display="block";
        flag = 0;
    }else{
        document.getElementById("textoRazonSocial").style.display="none";
    }
    if (factRFC.length < 1) {
        document.getElementById("textoRFC").innerHTML="Introducir valor en RFC";
        document.getElementById("textoRFC").style.display="block";
        flag = 0;
    } else {
        if (factRFC.length > 13) {
            document.getElementById("textoRFC").innerHTML= "El RFC solo debe ser compuesto de 12 a 13 caracteres";
            document.getElementById("textoRFC").style.display="block";
            flag = 0;
        }else{
            document.getElementById("textoRFC").style.display="none";
        }
        if (factRFC.length < 12) {
            document.getElementById("textoRFC").innerHTML= "El RFC solo debe ser compuesto de 12 a 13 caracteres";
            document.getElementById("textoRFC").style.display="block";
            flag = 0;
        }else{
            document.getElementById("textoRFC").style.display="none";

        }
    }
    if (factCa.length < 1) {
        document.getElementById("textoCalle").innerHTML= "Introducir valor en Calle";
        document.getElementById("textoCalle").style.display="block";
        flag = 0;
    }else{
        document.getElementById("textoCalle").style.display="none";
    }
    if (factExt.length < 1) {
        document.getElementById("textoNumExt").innerHTML="Introducir valor en Numero Exterior";
        document.getElementById("textoNumExt").style.display="block";
        flag = 0;
    }else{
        document.getElementById("textoNumExt").style.display="none";
    }
    if (factCol.length < 1) {
        document.getElementById("textoColonia").innerHTML= "Introducir valor en Colonia";
        document.getElementById("textoColonia").style.display="block";
        flag = 0;
    }else{
        document.getElementById("textoColonia").style.display="none";
    }
    if (factCd.length < 1) {
        document.getElementById("textoCiudad").innerHTML="Introducir valor en Ciudad";
        document.getElementById("textoCiudad").style.display="block";
        flag = 0;
    }else{
        document.getElementById("textoCiudad").style.display="none";
    }
    if (factEs.length < 1) {
        document.getElementById("textoEstado").innerHTML= "Introducir valor en Estado";
        document.getElementById("textoEstado").style.display="block";
        flag = 0;
    }else{
        document.getElementById("textoEstado").style.display="none";
    }
    if (factCP.length < 1) {
        document.getElementById("textoCP").innerHTML= "Introducir valor en Código Postal";
        document.getElementById("textoCP").style.display="block";
        flag = 0;
    }else{
        document.getElementById("textoCP").style.display="none"
    }
    if (flag == 0) {
        validacionDatosFacturacion=false;
    } else {
        validacionDatosFacturacion=true;
    }
}

document.getElementById("registrar").onclick = validarInscripcion;
document.getElementById("registrarInscripcion").onclick = resgistrarInscripcion;
