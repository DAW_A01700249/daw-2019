
let jugadoresTentativo = [];
let infoJugadoresTentativo = [];


function actualizarSeleccionadosTentativo() {
    for (let x in jugadoresTentativo) {
        let idBoton = "boton";
        idBoton += jugadoresTentativo[x];
        let idTabla = "participante";
        idTabla += jugadoresTentativo[x];
        let rowParticipante = document.getElementById(idTabla);
        let botonAgregar = document.getElementById(idBoton);
        if (botonAgregar != null) {
            botonAgregar.style.display = "none";
            rowParticipante.style.backgroundColor = "rgb(240, 238, 238)"
        }
    }
}
function validarEquipoTentativo(){
    let textoFaltante = "";
   let flag = 1;
   if (jugadoresTentativo.length == 0) {
      textoFaltante += "<li>Introducir participantes al equipo</li>";
      flag = 0;
   }
   if (flag == 0) {
      let mensaje = document.getElementById('tituloEquipoTentativoConfirmar');
      let botonConfirmar = document.getElementById('registrarEquipoTentativo');
      mensaje.innerHTML = "Por favor corregir los siguientes campos:";
      document.getElementById('contenidoConfirmacionEquipoTentativo').innerHTML = textoFaltante;
      mensaje.style.display = "block";
      botonConfirmar.style.display = "none";

   } else {
      let botonConfirmar = document.getElementById('registrarEquipoTentativo');
      let mensaje = document.getElementById('tituloEquipoTentativoConfirmar');
      mensaje.innerHTML = "¿Confirmar inscripción?";
      document.getElementById('contenidoConfirmacionEquipoTentativo').innerHTML = "Presione confirmar para realizar la inscripcion";
      botonConfirmar.style.display = "block";
   }
}

function actualizarEquipoTentativo() {
    let $result = "";
    let x = 0;
    while (x != jugadoresTentativo.length) {
        $result += infoJugadoresTentativo[x];
        x = x + 1;
    }
    document.getElementById("integrantesEquipoTextoTentativo").innerHTML = $result;
}

function añadirJugadorTentativo(idP, nombre) {
    if (jugadoresTentativo.indexOf(idP) == -1) {
        let $info = '<li class="collection-item" id="campoJugadorEquipo">';
        $info += idP;
        $info += '-';
        $info += nombre;
        $info += '<a id="campoJugadorEquipoBotonTentativo" class="btn btn-small waves-effect waves-light pink darken-1 right align"><i onclick="eliminarJugadorTentativo('
        $info += idP;
        $info += ')' + '"';
        $info += ' class="material-icons">remove_circle</i></a>';
        $info += "</li>";
        infoJugadoresTentativo.push($info);
        jugadoresTentativo.push(idP);
        let idTabla = "participanteTentativo";
        idTabla += idP;
        let idBoton = "boton";
        idBoton += idP;
        idBoton += "Tentativo";
        document.getElementById(idBoton).style.display = "none";
        document.getElementById(idTabla).style.backgroundColor = "rgb(240, 238, 238)";
        actualizarEquipoTentativo();
    }
}
function crearEquipoTentativo() {
    let jugadoresEquipoTentativo = JSON.stringify(jugadoresTentativo);
    let circulo = document.getElementById("circuloCarga_InscipcionEquipoTentativo");
    let forma = document.getElementById('formaEquipoTentativo');
    forma.style.display = "none";
    circulo.style.display = "block";
    $.post("../Controladores/controlador_crear_equipo_tentativo.php", {
        arrayJugadoresTentativo: jugadoresEquipoTentativo,
        tamañoEquipoTentativo: jugadoresTentativo.length,
    }).done(function (data) {
        console.log(data);
        if (data == 1) {
            circulo.style.display = "none";
            document.getElementById("wrap-crearEquipo").style.display = "none";
            document.getElementById("mensajeCrearEquipoTentativo").style.display = "block";
            document.getElementById("tituloCrearEquipoTentativo").innerHTML = "Se creo el equipo satisfactoriamente";
            document.getElementById("textoCrearEquipoTentativo").innerHTML = "El cambio se a guardado en la base de datos";
        } else {
            circulo.style.display = "none";

            document.getElementById("wrap-crearEquipo").style.display = "none";
            document.getElementById("mensajeCrearEquipoTentativo").style.display = "block";
            document.getElementById("tituloCrearEquipoTentativo").innerHTML = "No se pudo crear el equipo";
            document.getElementById("textoCrearEquipoTentativo").innerHTML = "Revisa tu conexion a internet o intentalo más tarde";
        }
    });

}

function eliminarJugadorTentativo(idP) {
    if (jugadoresTentativo.length == 1 || jugadoresTentativo.indexOf(idP) == 0) {
        jugadoresTentativo.shift();
        infoJugadoresTentativo.shift();
    } else {
        let index1 = jugadoresTentativo.indexOf(idP);
        infoJugadoresTentativo.splice(index1, 1);
        jugadoresTentativo.splice(index1, 1);
    }
    let idTabla = "participanteTentativo";
    idTabla += idP;
    let idBoton = "boton";
    idBoton += idP;
    idBoton += "Tentativo";
    document.getElementById(idBoton).style.display = "block";
    document.getElementById(idTabla).style.backgroundColor = "white";
    actualizarEquipoTentativo();
}

