
function editarTorneo() {
    //console.log($("#fechaT").val(),$("#cierreT").val());
    //$.post manda la petición asíncrona por el método post. También existe $.get
    if(!validarEditarTorneo()){
        return 0;
    }
    let circulo=document.getElementById("circuloCarga_EditarTorneo");
    circulo.style.display="block";
    let forma=document.getElementById('formaEditarTorneo');
    forma.style.display="none";
    $.post("../Controladores/controlador_editar_torneo.php", {
        nombreTE: $("#nombreTE").val(),
        preventaTE: $("#preventaTE").val(),
        fechaTE: $("#fechaTE").datepicker().val(),
        fechaCE: $("#fechaCE").datepicker().val(),
        fechaPE: $("#fechaCE").datepicker().val(),
        horaRE: $("#HoraRE").timepicker().val(),
        horaIE: $("#HoraRE").timepicker().val(),
        lugarE: $("#lugarE").val(),
        costoE: $("#costoE").val(),
        cierreTE: $("#cierreTE").datepicker().val(),
        cuentaTE: $("#cuentaTE").val(),
        clabeTE: $("#clabeTE").val(),
        bancoTE: $("#bancoTE").val(),
        beneTE: $("#beneficiarioT").val(),
        torneoActivo: $("#torneoActivo").val(),
        idTorneo: $("#idTorneo").val()
    }).done(function (data) {
        console.log(data);
        if(data==1){
            let ajaxResponse=document.getElementById('mensaje');
            let titulo2=document.getElementById('titulo2');
            let circulo=document.getElementById("circuloCarga_EditarTorneo");
            circulo.style.display="none";
            ajaxResponse.style.display="block";
            titulo2.innerHTML = "Los cambios al torneo se registraron exitosamente";
            let forma=document.getElementById('formaEditarTorneo');
            forma.style.display="none";
        }else{
            let ajaxResponse=document.getElementById('mensaje');
            let titulo2=document.getElementById('titulo2');
            ajaxResponse.style.display="block";
            titulo2.innerHTML="Los cambios no se pudieron registrar exitosamente. Favor de intentarlo nuevamente.";
            let circulo=document.getElementById("circuloCarga_EditarTorneo");
            circulo.style.display="none";
            let forma=document.getElementById('formaEditarTorneo');
            forma.style.display="none";
            let iconoRespuesta=document.getElementById('iconoRespuesta');
            iconoRespuesta.innerHTML="error";
            
        }
    });
}


function validarEditarTorneo(){

       let nombreTE = $("#nombreTE").val();
        let preventaTE = $("#preventaTE").val();
        let fechaPE = $("#fechaPE").datepicker().val();
        let fechaCE = $("#fechaCE").datepicker().val();
        let fechaTE = $("#fechaE").datepicker().val();
        let horaRE = $("#HoraRE").timepicker().val();
        let horaIE = $("#HoraIE").timepicker().val();
        let lugarE = $("#lugarE").val();
        let costoE = $("#costoE").val();
        let cierreTE = $("#cierreE").datepicker().val();
        let cuentaTE = $("#cuentaTE").val();
        let clabeTE = $("#clabeTE").val();
        let bancoTE = $("#bancoTE").val();
        let beneTE = $("#beneficiarioT").val();

    if(nombreTE.length < 1){
        let ajaxResponse=document.getElementById('MensajeEditorTorneo');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";;
        return 0;
    }
    if(preventaTE.length < 1){
        let ajaxResponse=document.getElementById('MensajeEditorTorneo');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";;
        return 0;
    }
    if($("#fechaPE").datepicker().val()== "0000-00-00"){
        let ajaxResponse=document.getElementById('MensajeEditorTorneo');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";;
        return 0;
    }
    if($("#fechaCE").datepicker().val() == "0000-00-00"){
        let ajaxResponse=document.getElementById('MensajeEditorTorneo');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";;
        return 0;
    }
    if($("#fechaTE").datepicker().val() == "0000-00-00"){
        let ajaxResponse=document.getElementById('MensajeEditorTorneo');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";;
        return 0;
    }
    if($("#HoraRE").timepicker().val() == ""){
        let ajaxResponse=document.getElementById('MensajeEditorTorneo');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if($("#HoraRE").timepicker().val() == ""){
        let ajaxResponse=document.getElementById('MensajeEditorTorneo');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(lugarE.length < 1){

        let ajaxResponse=document.getElementById('MensajeEditorTorneo');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(costoE.length < 1){
        let ajaxResponse=document.getElementById('MensajeEditorTorneo');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if($("#cierreTE").datepicker().val() == "0000-00-00"){
        let ajaxResponse=document.getElementById('MensajeEditorTorneo');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(cuentaTE.length < 1){
        let ajaxResponse=document.getElementById('MensajeEditorTorneo');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(clabeTE.length < 1){
        let ajaxResponse=document.getElementById('MensajeEditorTorneo');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(bancoTE.length < 1){
        let ajaxResponse=document.getElementById('MensajeEditorTorneo');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(beneTE.length < 1){
        let ajaxResponse=document.getElementById('MensajeEditorTorneo');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    
    return 1;
}



var habilitadaEditarT=document.getElementById("editarTor")
if(habilitadaEditarT!=null){
    habilitadaEditarT.onclick = editarTorneo;
}
