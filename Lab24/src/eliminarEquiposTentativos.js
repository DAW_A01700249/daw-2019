function eliminarEquipoTentativo($idEquipo) {
    console.log($idEquipo);
    let formaEliminar =document.getElementById("formaEliminarEquipoTentativo");
    let circuloCargaEliminar= document.getElementById("circuloCarga_EliminarEquipoTentativo");
    formaEliminar.style.display="none";
    circuloCargaEliminar.style.display="block";
    $.post("../Controladores/controlador_eliminar_equipo.php", {
       idEquipo: $idEquipo
   }).done(function (data) {
       if (data == 1) {
           circuloCargaEliminar.style.display="none";
           document.getElementById("wrap-container").style.display="none";
           document.getElementById("mensajeEliminarEquipoTentativo").style.display="block";
           document.getElementById("tituloMensajeEliminarEquipoTentativo").innerHTML="Se elimino el equipo satisfactoriamente";
           document.getElementById("textoMensajeEliminarEquipoTentativo").innerHTML="El cambio se a guardado en la base de datos";
           
       } else {
           circuloCargaEliminar.style.display="none";
           document.getElementById("wrap-container").style.display="none";
           document.getElementById("mensajeEliminarEquipoTentativo").style.display="block";
           document.getElementById("tituloMensajeEliminarEquipoTentativo").innerHTML="No se pudo eliminar el equipo";
           document.getElementById("textoMensajeEliminarEquipoTentativo").innerHTML="Revisa tu conexion a internet o intentalo más tarde";
 
       }
   });
 }