var _torneo="";
var _tipo="";

function sacarInfoTorneo() {
    //$.post manda la petición asíncrona por el método post. También existe $.get
    $.post("../Controladores/controlador_consulta_torneo.php", {
        torneo: $("#torneo").val()
    }).done(function (data) {
        
    });
}

var habilitadaIT=document.getElementById("InfoTorneo")
if(habilitadaIT!=null){
    habilitadaIT.onclick = sacarInfoTorneo;
}

function EditarPremio() {
    //$.post manda la petición asíncrona por el método post. También existe $.get
    if(!validarPremio()){
        return 0;
    }

    let circulo=document.getElementById("circuloCarga_EditarPremio");
    circulo.style.display="block";
    let forma=document.getElementById('formaEditarPremio');
    forma.style.display="none";
    $.post("../Controladores/controlador_edita_premio.php", {
        idTorneo: $("#torneo").val(),
        tipoEdit: $("#tipoEdit").val(),
        descripcionEdit: $("#descripcionEdit").val(),
        posicionEdit: $("#posicionEdit").val(),
        hoyoEdit: $("#hoyoEdit").val(),
        idPremio: $("#idPremio").val()
    }).done(function (data) {
    	console.log(data);
        if(data==0){
            let forma=document.getElementById('formaEditarPremio');
            forma.style.display="none";
            let ajaxResponse=document.getElementById('mensajeEditaPremio');
            let titulo=document.getElementById('titulo');
            let circulo=document.getElementById("circuloCarga_EditarPremio");
            circulo.style.display="none";
            ajaxResponse.style.display="block";
            titulo.innerHTML = "Se editó exitosamente el premio";
        }else{
            let ajaxResponse=document.getElementById('mensajeEditaPremio');
            let titulo=document.getElementById('tituloPremio');
            let forma=document.getElementById('formaEditarPremio');
            let circulo=document.getElementById("circuloCarga_EditarPremio");
            circulo.style.display="none";
            forma.style.display="none";
            ajaxResponse.style.display="block";
            titulo.innerHTML="No se pudieron registrar exitosamente los cambios. Favor de intentar más tarde.";
            let iconoRespuesta=document.getElementById('iconoRespuesta');
            iconoRespuesta.innerHTML="error";
            
        }
    });
}

function validarPremio(){
    let idTorneo = $("#torneo").val();
    let tipoEdit = $("#tipoEdit").val();
    let descripcionEdit = $("#descripcionEdit").val();
    let posicionEdit =  $("#posicionEdit").val();
    let hoyoEdit = $("#hoyoEdit").val();
    if($("#torneo").val()==0 || $("#torneo").val()==""){
        let ajaxResponse=document.getElementById('encabezadoMensajeEditarPremio');
        ajaxResponse.innerHTML="<h5>Por favor selecciona un torneo</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(descripcionEdit.length < 1){
        let ajaxResponse=document.getElementById('encabezadoMensajeEditarPremio');
        ajaxResponse.innerHTML="<h5>Por favor introduce una descripción para este premio</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if($("#hoyoEdit").val() == ""){
    } else if (hoyoEdit <1){
        let ajaxResponse=document.getElementById('encabezadoMensajeEditarPremio');
        ajaxResponse.innerHTML="<h5>Por favor introduce un número igual o mayor 1 en el campo 'Hoyo', o deja en blanco si este premio no corresponde a ningún hoyo</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if($("#tipoEdit").val()==0 ||$("#tipoEdit").val()==""){
        let ajaxResponse=document.getElementById('encabezadoMensajeEditarPremio');
        ajaxResponse.innerHTML="<h5>Por favor selecciona un tipo de premio</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    return 1;
}

var habilitadaEP=document.getElementById("EditarPremio")
if(habilitadaEP!=null){
    habilitadaEP.onclick = EditarPremio;
}



function eliminarPremio() {
    //$.post manda la petición asíncrona por el método post. También existe $.get
    /**if(!validar()){
        return 0;
    }**/
    let circulo=document.getElementById("circuloCarga_EliminarPremio");
    circulo.style.display="block";
    let forma=document.getElementById('formaEliminarPremio');
    forma.style.display="none";
    $.post("../Controladores/controlador_eliminar_premio.php", {
        idPremioElim: $("#idPremioElim").val()
    }).done(function (data) {
        console.log(data);
        if(data==0){
            let forma=document.getElementById('formaEliminarPremio');
            forma.style.display="none";
            let ajaxResponse=document.getElementById('mensajeEliminaPremio');
            let titulo=document.getElementById('tituloPremioE');
            let circulo=document.getElementById("circuloCarga_EliminarPremio");
            circulo.style.display="none";
            ajaxResponse.style.display="block";
            titulo.innerHTML = "Se eliminó exitosamente el premio";
        }else{
            let ajaxResponse=document.getElementById('mensajeEliminaPremio');
            let titulo=document.getElementById('tituloPremioE');
            let forma=document.getElementById('formaEliminarPremio');
            let circulo=document.getElementById("circuloCarga_EliminarPremio");
            circulo.style.display="none";
            forma.style.display="none";
            ajaxResponse.style.display="block";
            titulo.innerHTML="No se pudieron registrar exitosamente los cambios. Favor de intentar más tarde.";
            let iconoRespuesta=document.getElementById('iconoRespuesta');
            iconoRespuesta.innerHTML="error";
            
        }
    });
}


var habilitadaPE=document.getElementById("eliminarPremio")
if(habilitadaPE!=null){
    habilitadaPE.onclick = eliminarPremio;
}