<?php
    
//require_once("../Models/modelEditarRegistro.php"); 


 function conectar_based() {
      $conexion_bd = mysqli_connect("mysql1008.mochahost.com","dawbdorg_torneo","torneo","dawbdorg_torneogolf");
      if ($conexion_bd == NULL) {
          die("No se pudo conectar con la base de datos");
      }
      return $conexion_bd;
  }

  //función para desconectarse de una bd
  //@param $conexion: Conexión de la bd que se va a cerrar
  function desconectar_based($conexion_bd) {
      mysqli_close($conexion_bd);
  }

   function consultar_folio($username){
      $conexion_bd = conectar_based();
       $consulta = 'SELECT folio from participante_torneo PT, participante P WHERE PT.idParticipante = P.idParticipante AND nombreUsuario = "'.$username.'" ';
      $resultados = $conexion_bd->query($consulta);
      while($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)){
        $resultado =  $row['folio'];
      }
      desconectar_based($conexion_bd); 

      return $resultado;


    }

  function subir_recibo($nombre_imagen,$folio){
    $conexion_bd = conectar_based();

    $dml = 'UPDATE participante_torneo 
        SET 
        idFotoRecibo=(?)
        WHERE folio=(?)';

        if ( !($statement = $conexion_bd->prepare($dml)) ) {
            die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
            return 0;
        }
        if (!$statement->bind_param("si",$nombre_imagen,$folio)) {
            die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
            return 0;
           
        }
   //Executar la consulta
        if (!$statement->execute()) {
          die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
            return 0;
            
        }
    
        desconectar_based($conexion_bd);
          return 1;
      }
  

    function idT(){
      $conexion_bd = conectar_based();  
        
      $consulta = "SELECT iDTorneo FROM torneo WHERE activo=1";
      $resultados = $conexion_bd->query($consulta);
      while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
          desconectar_based($conexion_bd);
          return $row["iDTorneo"];
      }
          
      desconectar_based($conexion_bd);
      return 0;
  }


    function subir_poster($nombre_imagen){
    $conexion_bd = conectar_based();

    $dml = 'UPDATE torneo 
        SET 
        poster=(?)
        WHERE activo=1';

        if ( !($statement = $conexion_bd->prepare($dml)) ) {
            die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
            return 0;
        }
        if (!$statement->bind_param("s",$nombre_imagen)) {
            die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
            return 0;
        }
        //Executar la consulta
        if (!$statement->execute()) {
          die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
            return 0;
            
        }
        desconectar_based($conexion_bd);
          return 1;
      }

function consulta_posterTorneo(){
      $conexion_bd = conectar_based();
        
      $consulta = "SELECT poster FROM torneo WHERE activo=1";
      $resultados = $conexion_bd->query($consulta);
      while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
          desconectar_based($conexion_bd);
          return $row["poster"];
      }
          
      desconectar_based($conexion_bd);
      return 0;
  }

?>