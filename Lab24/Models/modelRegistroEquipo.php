<?php

function conectar_bd() {
    $conexion_bd = mysqli_connect("mysql1008.mochahost.com","dawbdorg_torneo","torneo","dawbdorg_torneogolf");
    if ($conexion_bd == NULL) {
        die("No se pudo conectar con la base de datos");
    }
    return $conexion_bd;
}
function desconectar_bd($conexion_bd) {
    mysqli_close($conexion_bd);
}
function registrarEquipo($arrayJugadores,$hoyoInicial,$tamañoEquipo){
    $conexion_bd = conectar_bd();
    
    $idTorneo=idTorneo();
    $dml="START TRANSACTION";
    $conexion_bd->query($dml);
    //
    // Se crea el equipo
    //
    $a=null;
    $oficial="Oficial";
    $dml = 'INSERT INTO equipo(idEquipo,numParticipantes,hoyoInicial,estatus) VALUES (?,?,?,?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
      $dml="rollback";
      $conexion_bd->query($dml);
      die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
      return 0;
    }
    if (!$statement->bind_param("iiis",$a,$tamañoEquipo,$hoyoInicial,$oficial)) {
      $dml="rollback";
      $conexion_bd->query($dml);
      die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
      return 0;
    }
    if (!$statement->execute()) {
      $dml="rollback";
      $conexion_bd->query($dml);
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
      return 0;
    }
    //
    //Se obtiene la id de equipo
    //
    $consulta = "SELECT idEquipo FROM equipo ORDER BY idEquipo DESC LIMIT 1";
    $resultados = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $idEquipo=$row["idEquipo"];
    }
    //
    // Se agregan los integrantes
    //
    for ($i=0; $i <$tamañoEquipo; $i++) { 
        $dml = 'INSERT INTO participante_equipo_torneo(idParticipante,idTorneo,idEquipo) VALUES (?,?,?)';
        if ( !($statement = $conexion_bd->prepare($dml)) ) {
            $dml="rollback";
            $conexion_bd->query($dml);
            die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
            return 0;
        }
        if (!$statement->bind_param("iii",$arrayJugadores[$i],$idTorneo,$idEquipo)) {
            $dml="rollback";
            $conexion_bd->query($dml);
            die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
            return 0;
        }
        if (!$statement->execute()) {
          $dml="rollback";
          $conexion_bd->query($dml);
          die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
            return 0;
        }  
    }
    
    $dml="commit";
    $conexion_bd->query($dml);
    desconectar_bd($conexion_bd);
    return 1;
  }

function idTorneo(){
    $conexion_bd = conectar_bd();
    $consulta = "Select idTorneo from torneo where activo=1";
    $resultados = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        desconectar_bd($conexion_bd);
        return $row["idTorneo"];
    }
    desconectar_bd($conexion_bd);
    return 0;
  }

  function registrarEquipoTentativo($arrayJugadoresTentativo,$hoyoInicialTentativo,$tamañoEquipoTentativo){
    $conexion_bd = conectar_bd();  
    $idTorneo=idTorneo();
    $dml="START TRANSACTION";
    $conexion_bd->query($dml);
    $estatus="Tentativo";
    $a=null;
    //
    //Se crea el equipo como tentativo
    //
      $dml = 'INSERT INTO equipo(idEquipo,numParticipantes,hoyoInicial,estatus) VALUES (?,?,?,?)';
      if ( !($statement = $conexion_bd->prepare($dml)) ) {
        $dml="rollback";
        $conexion_bd->query($dml);
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
      }
      if (!$statement->bind_param("iiis",$a,$tamañoEquipoTentativo,$hoyoInicialTentativo,$estatus)) {
        $dml="rollback";
        $conexion_bd->query($dml);
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
      }
      if (!$statement->execute()) {
        $dml="rollback";
        $conexion_bd->query($dml);
        die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
      }
    //
    //Se obtiene la id de equipo
    //
    $consulta = "SELECT idEquipo FROM equipo ORDER BY idEquipo DESC LIMIT 1";
    $resultados = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $idEquipo=$row["idEquipo"];
    }
    //
    //Se agregan los jugadores a los equipos
    //
    for ($i=0; $i <$tamañoEquipoTentativo; $i++) { 
      $dml = 'INSERT INTO participante_equipo_torneo(idParticipante,idTorneo,idEquipo) VALUES (?,?,?)';
      if ( !($statement = $conexion_bd->prepare($dml)) ) {
        $dml="rollback";
        $conexion_bd->query($dml);
          die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
          return 0;
      }
      if (!$statement->bind_param("iii",$arrayJugadoresTentativo[$i],$idTorneo,$idEquipo)) {
        $dml="rollback";
        $conexion_bd->query($dml);
          die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
          return 0;
      }
      if (!$statement->execute()) {
        $dml="rollback";
        $conexion_bd->query($dml);
        die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
          return 0;
      }  
    }
  $dml="commit";
  $conexion_bd->query($dml);
  desconectar_bd($conexion_bd);
  return 1;
  }
  
?>