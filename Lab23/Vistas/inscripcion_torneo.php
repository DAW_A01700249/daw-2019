<?php
	session_start();
	  if(isset($_SESSION['usuario'])){
	    if($_SESSION['usuario']['nombreRol']!="participante"){
	      header('Location: InterfazOrg.php');
	    }
	  }else{
	      header('Location: InterfazLogin.php');
    }

	require_once("../Models/model_DatosTorneo.php");
	$cantidad=getPrecioPreventa();
	include("header.html");
	include("inscripcion_torneo.html");
	include("footer.html");
?>