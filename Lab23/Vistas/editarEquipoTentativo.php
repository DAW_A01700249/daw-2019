<?php 
  session_start();
  require_once("../Models/modelEditarEquipo.php"); 
  $username= $_SESSION['usuario']['nombreUsuario'];
  //recuperar el caso que se va a editar
  $equipo_id = htmlspecialchars($_GET["equipo_id"]);
  $jugadoresEditarEquipoTentativo= recuperarEquipo($equipo_id);
  $idParticipantesTentativo=[];
  $nombresParticipantesTentativo=[];
  for ($i=0; $i < sizeof($jugadoresEditarEquipoTentativo[0]); $i++) { 
    array_push($idParticipantesTentativo,$jugadoresEditarEquipoTentativo[0][$i]);
  }
  for ($i=0; $i < sizeof($jugadoresEditarEquipoTentativo[0]); $i++) { 
    array_push($nombresParticipantesTentativo,$jugadoresEditarEquipoTentativo[1][$i]);
  }
  $json_idParticipantesTentativo=json_encode($idParticipantesTentativo);
  $json_nombresParticipantesTentativo=json_encode($nombresParticipantesTentativo);
  
  include("header.html");  
  include("editarEquipoTentativo.html");
  include("footer.html");
  echo "<script>
   recuperarInfoTentativo($json_idParticipantesTentativo,$json_nombresParticipantesTentativo); 
   </script>";
?>