<?php
	session_start();
	  if(isset($_SESSION['usuario'])){
	    if($_SESSION['usuario']['nombreRol']!="organizador"){
	      header('Location: pagina-principal.php');
	    }
	  }else{
	      header('Location: InterfazLogin.php');
    }
    require_once("../Models/modelEditarPremios.php");
	include("header.html");
	include("_formPremios.html");
	if (isset($_POST["torneo"])) {
          $torneo = htmlspecialchars($_POST["torneo"]);
      } else {
          $torneo = "";
      }

     if (isset($_POST["tipoPremio"])) {
          $premio = htmlspecialchars($_POST["tipoPremio"]);
      } else {
          $premio = "";
      }

      echo '<div class="col s7"id="resultados_consulta_premios">';
      echo mostrarPremios($torneo,$premio);
      echo '</div>';
	include("footer.html");
?>