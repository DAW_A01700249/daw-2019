<?php

     function conectar_bd() {
      $conexion_bd = mysqli_connect("mysql1008.mochahost.com","dawbdorg_torneo","torneo","dawbdorg_torneogolf");
      if ($conexion_bd == NULL) {
          die("No se pudo conectar con la base de datos");
      }
      return $conexion_bd;
  }
  //función para desconectarse de una bd
  //@param $conexion: Conexión de la bd que se va a cerrar
  function desconectar_bd($conexion_bd) {
      mysqli_close($conexion_bd);
  }

  //Consulta los casos
  
  function datosTorneoInicio() {
    $conexion_bd = conectar_bd();  
    
    $resultado =  "";
    
    $consulta = 'SELECT fechaTorneo, horaRegistro, horaInicio, lugar FROM torneo WHERE activo = 1';
      
    $resultados = $conexion_bd->query($consulta); 

    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= "<p>".$row['fechaTorneo']."</p>";
        $resultado .= "<p>".$row['lugar']."</p>";
        $resultado .= "<p>Registro: ".$row['horaRegistro']."</p>";
        $resultado .= "<p>Salida: ".$row['horaInicio']."</p>";
    }
    
    mysqli_free_result($resultados); //Liberar la memoria
  
    desconectar_bd($conexion_bd);   
      

    return $resultado;
  }


   function PremiosInicio() {
    $conexion_bd = conectar_bd();  
    
    $resultado =  "";

    $consulta = 'SELECT nombre FROM torneo WHERE activo = 1';
    $resultados = $conexion_bd->query($consulta); 
     while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= "<h3>".$row['nombre']."</h3>";
    }



    mysqli_free_result($resultados); //Liberar la memoria

      
     $resultado .= "<table class='tg'>
                      <thead>
                        <tr>
                          <th class='tg-baqh' colspan='4'>Premios</th>
                        </tr>
                      </thead>
                      <tbody>";

    echo $resultado;
    
    $consulta2 = 'SELECT descripcion, posicion, tipo, hoyo FROM premio P, torneo T WHERE P.idTorneo = T.idTorneo AND activo = 1 ORDER BY tipo ASC, hoyo ASC,  posicion ASC';
    $resultados = $conexion_bd->query($consulta2); 

    $tipo = array(); //emp
    $hoyo = array(); //sal
    $posicion = array();
    $descripcion = array();

    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        array_push($tipo, $row['tipo']);
        array_push($hoyo, $row['hoyo']);
        array_push($posicion, $row['posicion']);
        array_push($descripcion, $row['descripcion']);
    }

//Para calcular rowspan
    $arr = array();

    //loop para el array de tipo
    for ($i = 0; $i < sizeof($descripcion); $i++) {
        $tipoPr = $tipo[$i];
        # If there is no array for the employee
        # then create a elemnt.
        if (!isset($arr[$tipoPr])) {
            $arr[$tipoPr] = array();
            $arr[$tipoPr]['rowspan'] = 0;
        }
        $arr[$tipoPr]['printed'] = "no";
        # Increment the row span value.
        $arr[$tipoPr]['rowspan'] += 1;
    }

    echo "<table cellspacing='0' cellpadding='0'>
                <tr>
                    <th></th>
                    <th width=15% style='text-align:center'>Posicion</th>
                    <th width=15% style='text-align:center'>Hoyo</th>
                    <th>Premio</th>
                </tr>";

    for($i=0; $i < sizeof($descripcion); $i++) {
      $tipoPr = $tipo[$i];
      echo "<tr>";
      if($arr[$tipoPr]['printed'] == 'no'){
        echo "<td width=15% rowspan='".$arr[$tipoPr]['rowspan']."'>".$tipoPr."</td>";
            $arr[$tipoPr]['printed'] = 'yes';
      }
     
    echo "<td style='text-align:center'>".$posicion[$i]."</td>";
    echo "<td style='text-align:center'>".$hoyo[$i]."</td>";
    echo "<td>".$descripcion[$i]."</td>";
    }



    echo "</table>";

    desconectar_bd($conexion_bd);
  }


function getPrecioPreventa(){
    $conexion_bd = conectar_bd();  
    $consulta = 'SELECT preventa FROM torneo WHERE activo = 1';
    $resultado = $conexion_bd->query($consulta);
    $row = mysqli_fetch_array($resultado, MYSQLI_BOTH);
    $precio=$row['preventa'];
    mysqli_free_result($resultado); //Liberar la memoria
    return $precio;

}

function infoPaginaPrincipal(){



    $conexion_bd = conectar_bd();  
    
    $resultado =  "";
    
    $consulta = 'SELECT fechaTorneo, horaRegistro, horaInicio, lugar, preventa, fechaPreventa, costo, FechaCosto, nombre FROM torneo WHERE activo = 1';
      
    $resultados = $conexion_bd->query($consulta); 

    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= "<h3>".$row['nombre']."</h3>";
        $resultado .= "<div class='divider'></div>";
        $resultado .= "<h5>Fecha: </h5><h6>".$row['fechaTorneo']."</h6><br>";
        $resultado .= "<h5>Lugar: </h5><h6>".$row['lugar']."</h6><br>";
        $resultado .= "<h5>Hora de registro: </h5><h6>".$row['horaRegistro']."</h6><br>";
        $resultado .= "<h5>Hora de salida: </h5><h6>".$row['horaInicio']."</h6><br>";
        $resultado .= "<h5>Donativo hasta el día ".$row['fechaPreventa']." :</h5><h6>".$row['preventa']."</h6><br>";
        $resultado .= "<h5>Donativo en fecha posterior: ".$row['fechaPreventa']." :</h5><h6>".$row['costo']."</h6><br>";

    }
    
    mysqli_free_result($resultados); //Liberar la memoria
  
    desconectar_bd($conexion_bd);   
      

    return $resultado;

} 



function premiosVistaUsuario(){

    $resultado = "";
    $conexion_bd = conectar_bd();  

    $resultado .=  "<div class='row'><div class='col s4'><h5>Premios Por equipo</h5><div class='section'><table><th><td>Posición</td><td>Premio</th>";

    $consulta2 = 'SELECT descripcion, posicion, tipo, hoyo FROM premio P, torneo T WHERE P.idTorneo = T.idTorneo AND activo = 1 AND tipo = "Por equipo" ';
      
    $resultados = $conexion_bd->query($consulta2); 

    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
      $resultado .= "<tr><td></td>";
      $resultado .= "<td><h6>".$row['posicion']."</td><td>".$row['descripcion']."</h6></td></tr>";

    }

    $resultado .= " </table></div></div>";
    
    mysqli_free_result($resultados); //Liberar la memoria
    
    $resultado .=  "<div class='row'><div class='col s4'><h5>Premios Hole-in-one</h5><div class='section'><table><th><td>Hoyo</td><td>Premio</th>";
    
    $consulta = 'SELECT descripcion, posicion, tipo, hoyo FROM premio P, torneo T WHERE P.idTorneo = T.idTorneo AND activo = 1 AND tipo = "Hole-in-one" ';
      
    $resultados = $conexion_bd->query($consulta); 

    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
      $resultado .= "<tr><td></td>";
      $resultado .= "<td><h6>".$row['hoyo']."</td><td>".$row['descripcion']."</h6></td></tr>";

    }

    $resultado .= " </table></div></div>";
    
    mysqli_free_result($resultados); //Liberar la memoria

 
    $premio = "O'Yes";

    $resultado .=  "<div class='row'><div class='col s4'><h5>O'Yes</h5><div class='section'><table><th><td>Posición</td><td>Hoyo</td><td>Posición</th>";

    $consulta2 = 'SELECT descripcion, posicion, tipo, hoyo FROM premio P, torneo T WHERE P.idTorneo = T.idTorneo AND activo = 1 AND tipo = "'.$premio.'" ORDER BY hoyo ASC, posicion ASC';
      
    $resultados = $conexion_bd->query($consulta2); 

    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= "<tr><td></td>";
        $resultado .= "<td><h6>".$row['posicion']."</td><td>".$row['hoyo']."<td>".$row['descripcion']."</h6></td></tr>";

    }

    $resultado .= " </table></div></div></div>";
    
    mysqli_free_result($resultados); //Liberar la memoria
  
    desconectar_bd($conexion_bd);   
      

    return $resultado;

} 


function mostrarInfoAdministrativa(){
    $conexion_bd = conectar_bd();  
    
    $resultado =  "<div id='informacion-administrativa' class='col s4 offset-l1'>
          <br>
          <br>
          <br><div class='grey lighten-1 z-depth-2 center-align'><br><h5>Datos Depósito</h5>";
    
    $consulta = 'SELECT * FROM torneo WHERE activo = 1';
      
    $resultados = $conexion_bd->query($consulta); 

    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= "<p>Depósitos en ".$row['banco']. " <br>a nombre de: ".$row['beneficiario']."</p>";
        $resultado .= "<p>Cuenta ".$row['cuenta']. "</p>";
        $resultado .= "<p>CLABE ".$row['clabe']. "</p>";
        $resultado .= "<p>Referencia: Folio de registro </p><p>Favor de consultar la sección Consultar mis registros para verificar el número de folio que le fue asignado </p>";
        $resultado .= "<br><br>";
        $resultado .= "<h6>Donativo hasta el día ".$row['fechaPreventa'].":</h6><h6>$".$row['preventa']."</h6><br>";
        $resultado .= "<h6>Donativo fecha posterior ".$row['fechaPreventa'].":</h6><h6>$".$row['costo']."</h6><br>";
        $resultado .= "<br><br>";
    }
    
    mysqli_free_result($resultados); //Liberar la memoria
  
    desconectar_bd($conexion_bd);   
      
    $resultado .= "</div>
        </div>";
    return $resultado;

}
  ?>
