<?php
     
     function conectar_bd() {
      $conexion_bd = mysqli_connect("mysql1008.mochahost.com","dawbdorg_torneo","torneo","dawbdorg_torneogolf");
      if ($conexion_bd == NULL) {
          die("No se pudo conectar con la base de datos");
      }
      return $conexion_bd;
  }

  //función para desconectarse de una bd
  //@param $conexion: Conexión de la bd que se va a cerrar
  function desconectar_bd($conexion_bd) {
      mysqli_close($conexion_bd);
  }
  function recuperarEquipo($idEquipo){
    $conexion_bd = conectar_bd();
    $respuesta=[];
    $idParticipantes=[];
    $nombresParticipantes=[];
    $hoyoIncial;
    $consulta = "select PET.idParticipante, P.nombre, E.hoyoInicial from participante_equipo_torneo as PET, participante as P, equipo as E
    where PET.idParticipante=P.idParticipante AND PET.idEquipo=$idEquipo AND E.idEquipo= PET.idEquipo";
    $resultados = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) 
    {
      array_push($idParticipantes,$row['idParticipante']);
      array_push($nombresParticipantes,$row['nombre']);
      $hoyoIncial=$row['hoyoInicial'];
    }
    array_push($respuesta,$idParticipantes);
    array_push($respuesta,$nombresParticipantes);
    array_push($respuesta,$hoyoIncial);
    mysqli_free_result($resultados); //Liberar la memoria
    desconectar_bd($conexion_bd);   
    return $respuesta;
  }
  function consultar_casosEquipoEditar($dato,$idEquipoActual){
    $conexion_bd = conectar_bd();  
    
    $resultado = '<table class="centered"><thead><tr><th></th><th>Participante</th><th>Nombre Usuario</th><th>Nombre</th><th>Apellido Paterno</th><th>Apellido Materno</th><th>Handicap</th><th>Indicacion Especial</th></tr></thead>';
    
  
     $consulta = "SELECT P.idParticipante, P.nombreUsuario, P.nombre , P.apellidoPaterno, P.apellidoMaterno, P.handicap, PT.indicacionEspecial FROM participante_torneo as PT, participante as P, participante_equipo_torneo as PET where
     P.idParticipante=PT.idParticipante AND P.idParticipante NOT IN(select idParticipante from participante_equipo_torneo where idEquipo!=$idEquipoActual) AND (
    P.idParticipante LIKE '%".$dato."%' OR P.nombreUsuario LIKE '%".$dato."%' OR P.nombre LIKE '%".$dato."%' OR P.apellidoPaterno LIKE '%".$dato."%' OR P.apellidoMaterno LIKE '%".$dato."%' OR P.handicap LIKE '%".$dato."%' OR PT.indicacionEspecial LIKE '%".$dato."%')
     GROUP BY P.idParticipante";

    $resultados = $conexion_bd->query($consulta); 
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= '<tr id="participante'.$row['idParticipante'].'">';
        $resultado .= '<td> <a class="btn-floating btn-small waves-effect waves-light pink darken-1" id="boton'.$row['idParticipante'].'"><i onclick="añadirJugadorEditar('.$row['idParticipante'].'';
        $resultado .=',';
        $resultado.="'";
        $resultado.= $row['nombre'];
        $resultado.="'";
        $resultado.= ')"';
        $resultado .=' class="material-icons">add</i></a></td>';
        $resultado .= "<td>".$row['idParticipante']."</td>"; //o el nombre de la columna
        $resultado .= "<td>".$row['nombreUsuario']."</td>"; //o el nombre de la columna
        $resultado .= "<td>".$row['nombre']."</td>";
        $resultado .= "<td>".$row['apellidoPaterno']."</td>";
        $resultado .= "<td>".$row['apellidoMaterno']."</td>";
        $resultado .= "<td>".$row['handicap']."</td>";
        $resultado .= "<td>".$row['indicacionEspecial']."</td>";
        $resultado .= "</tr>";
    }
    mysqli_free_result($resultados); //Liberar la memoria
    desconectar_bd($conexion_bd);   
    $resultado .= "</tbody></table>";
    return $resultado;
  }
  function editarEquipoTentativo($agregarJugadores,$eliminarJugadores,$tamañoEquipoEditar,$idEquipoEditar){
    $conexion_bd = conectar_bd();
    $idTorneo=idTorneo();
    $dml="START TRANSACTION";
    $conexion_bd->query($dml);
    //
    //Agregar jugadores
    //
    for ($i=0; $i <count($agregarJugadores); $i++) {
      $dml = 'INSERT INTO participante_equipo_torneo(idParticipante,idTorneo,idEquipo) VALUES (?,?,?)';
      if ( !($statement = $conexion_bd->prepare($dml)) ) {
        $dml="rollback";
        $conexion_bd->query($dml);
          die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
          return 0;
      }
      if (!$statement->bind_param("iii",$agregarJugadores[$i],$idTorneo,$idEquipoEditar)) {
        $dml="rollback";
        $conexion_bd->query($dml);
          die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
          return 0;
      }
      if (!$statement->execute()) {
        $dml="rollback";
        $conexion_bd->query($dml);
        die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
          return 0;
      }
      
    }
      //
      //EliminarJugadores
      //
      for ($i=0; $i <count($eliminarJugadores); $i++) {
      $dml = 'DELETE FROM participante_equipo_torneo WHERE idParticipante = (?)';
      if ( !($statement = $conexion_bd->prepare($dml)) ) {
        $dml="rollback";
        $conexion_bd->query($dml);
          die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
          return 0;
      }
      if (!$statement->bind_param("i",$eliminarJugadores[$i])) {
        $dml="rollback";
        $conexion_bd->query($dml);
          die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
          return 0;
      }
      if (!$statement->execute()) {
        $dml="rollback";
        $conexion_bd->query($dml);
        die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
          return 0;
      }  
    }
    //
    //ActualizarEquipo
    //
    $dml = 'UPDATE equipo SET numParticipantes=(?) where idEquipo=(?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
      $dml="rollback";
        $conexion_bd->query($dml);
      die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
      return 0;
  }
  if (!$statement->bind_param("ii",$tamañoEquipoEditar,$idEquipoEditar)){
    $dml="rollback";
        $conexion_bd->query($dml);
      die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
      return 0;
  }
  if (!$statement->execute()) {
    $dml="rollback";
        $conexion_bd->query($dml);
    die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
      return 0;
  }
    $dml="commit";
    $conexion_bd->query($dml);
    return 1;

  }
  function editarEquipo($agregarJugadores,$eliminarJugadores,$hoyoInicialEditar,$tamañoEquipoEditar,$idEquipoEditar){
    $conexion_bd = conectar_bd();
    $idTorneo=idTorneo();
    if(! agregarJugador($agregarJugadores,$idEquipoEditar)){
      die("Error ");
          return 0;
    }
    if(! eliminarJugador($eliminarJugadores)){
      die("Error ");
          return 0;
    }
    if(! actualizarEquipo($hoyoInicialEditar,$tamañoEquipoEditar,$idEquipoEditar)){
      die("Error ");
          return 0;
    }
    desconectar_bd($conexion_bd);   
    return 1;
  }
  function agregarJugador($array,$idEquipo){
    $conexion_bd = conectar_bd();
    $idTorneo=idTorneo();
    for ($i=0; $i <count($array); $i++) {
        $dml = 'INSERT INTO participante_equipo_torneo(idParticipante,idTorneo,idEquipo) VALUES (?,?,?)';
        if ( !($statement = $conexion_bd->prepare($dml)) ) {
            die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
            return 0;
        }
        if (!$statement->bind_param("iii",$array[$i],$idTorneo,$idEquipo)) {
            die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
            return 0;
        }
        if (!$statement->execute()) {
          die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
            return 0;
        }  
    }
    desconectar_bd($conexion_bd);
    return 1;
  }
  function eliminarJugador($arrayEliminar){
    $conexion_bd = conectar_bd();
    for ($i=0; $i <count($arrayEliminar); $i++) {
      $dml = 'DELETE FROM participante_equipo_torneo WHERE idParticipante = (?)';
      if ( !($statement = $conexion_bd->prepare($dml)) ) {
          die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
          return 0;
      }
      if (!$statement->bind_param("i",$arrayEliminar[$i])) {
          die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
          return 0;
      }
      if (!$statement->execute()) {
        die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
          return 0;
      }  
  }
  desconectar_bd($conexion_bd);
  return 1;
  }
  function actualizarEquipo($hoyo,$num,$id){
    $conexion_bd = conectar_bd();
    $dml = 'UPDATE equipo SET hoyoInicial=(?), numParticipantes=(?) where idEquipo=(?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
      die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
      return 0;
  }
  if (!$statement->bind_param("iii",$hoyo,$num,$id)){
      die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
      return 0;
  }
  if (!$statement->execute()) {
    die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
      return 0;
  }
  desconectar_bd($conexion_bd);
    return 1;
  }
  function idTorneo(){
    $conexion_bd = conectar_bd();
    $consulta = "Select idTorneo from torneo where activo=1";
    $resultados = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        desconectar_bd($conexion_bd);
        return $row["idTorneo"];
    }
    desconectar_bd($conexion_bd);
    return 0;
  }
  function eliminarEquipo($idEquipoEliminar){
    $conexion_bd = conectar_bd();  
    $dml ="START TRANSACTION";
    $conexion_bd->query($dml);
    //Se elimina de la tabla de participante_equipo_Torneo
    $dml = "DELETE FROM participante_equipo_torneo where idEquipo=(?)";
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
      $dml="rollback";
      $conexion_bd->query($dml);
      die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
      return 0;
    }
    if (!$statement->bind_param("i",$idEquipoEliminar)){
      $dml="rollback";
      $conexion_bd->query($dml);
      die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
      return 0;
    }
    if (!$statement->execute()) {
      $dml="rollback";
      $conexion_bd->query($dml);
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
      return 0;
    }
    //Se elimina de la tabla de Equipo
    $dml = "DELETE FROM equipo where idEquipo=(?)";
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
      $dml="rollback";
      $conexion_bd->query($dml);
      die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
      return 0;
    }
    if (!$statement->bind_param("i",$idEquipoEliminar)){
      $dml="rollback";
      $conexion_bd->query($dml);
      die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
      return 0;
    }
    if (!$statement->execute()) {
      $dml="rollback";
      $conexion_bd->query($dml);
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
      return 0;
    }
      $dml="commit";
      $conexion_bd->query($dml);
      desconectar_bd($conexion_bd);
      return 1;
    
  }
  function consultar_casosEquipoTentativoEditar($equipo_id,$username){
    $conexion_bd = conectar_bd();  
    
    $resultado = '<table class="centered"><thead><tr><th></th><th>Participante</th><th>Nombre Usuario</th><th>Nombre</th><th>Apellido Paterno</th><th>Apellido Materno</th><th>Handicap</th><th>Indicacion Especial</th></tr></thead>';
    
  
     $consulta = "SELECT P.idParticipante, P.nombreUsuario, P.nombre , P.apellidoPaterno, P.apellidoMaterno, P.handicap, PT.indicacionEspecial FROM participante_torneo as PT, participante as P, participante_equipo_torneo as PET where
     P.idParticipante=PT.idParticipante AND P.idParticipante NOT IN(select idParticipante from participante_equipo_torneo where idEquipo!=$equipo_id) AND P.idParticipante in(SELECT idParticipante from participante where nombreUsuario='$username')
     GROUP BY P.idParticipante";

    $resultados = $conexion_bd->query($consulta); 
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= '<tr id="participante'.$row['idParticipante'].'">';
        $resultado .= '<td> <a class="btn-floating btn-small waves-effect waves-light pink darken-1" id="boton'.$row['idParticipante'].'"><i onclick="añadirJugadorEditarTentativo('.$row['idParticipante'].'';
        $resultado .=',';
        $resultado.="'";
        $resultado.= $row['nombre'];
        $resultado.="'";
        $resultado.= ')"';
        $resultado .=' class="material-icons">add</i></a></td>';
        $resultado .= "<td>".$row['idParticipante']."</td>"; //o el nombre de la columna
        $resultado .= "<td>".$row['nombreUsuario']."</td>"; //o el nombre de la columna
        $resultado .= "<td>".$row['nombre']."</td>";
        $resultado .= "<td>".$row['apellidoPaterno']."</td>";
        $resultado .= "<td>".$row['apellidoMaterno']."</td>";
        $resultado .= "<td>".$row['handicap']."</td>";
        $resultado .= "<td>".$row['indicacionEspecial']."</td>";
        $resultado .= "</tr>";
    }
    mysqli_free_result($resultados); //Liberar la memoria
    desconectar_bd($conexion_bd);   
    $resultado .= "</tbody></table>";
    return $resultado;

  }


?>