let jugadoresEditarEquipo = [];
let infojugadoresEditarEquipo = [];
let jugadoresEditarEquipoActual = [];
let jugadores_Eliminar=[];
let hoyoInicialActual;

function recuperarInfo($id_Participantes, $nombres_Participantes, $hoyoIncialActual) {
    let i = 0;
    hoyoInicialActual = $hoyoIncialActual;
    while (i < $id_Participantes.length) {
        numero = parseInt($id_Participantes[i], 10)
        jugadoresEditarEquipoActual.push(numero);
        añadirJugadorEditar(numero, $nombres_Participantes[i]);
        i = i + 1;
    }
}
function validarEquipoEditar() {
    let textoFaltante = "";
    let flag = 1;
    let hoyoInicial = $("#hoyoInicialEditar").val();
    if (jugadoresEditarEquipo.length == jugadoresEditarEquipoActual.length && jugadoresEditarEquipo.every(function (v, i) { return v === jugadoresEditarEquipoActual[i] }) && hoyoInicial == hoyoInicialActual) {
        document.getElementById('tituloEditar').innerHTML = "No se realizo ningun cambio";
        document.getElementById('contenidoConfirmacionEditar').innerHTML = "";
        document.getElementById("editarEquipo").style.display = "none";
    } else {
        if (hoyoInicial.length < 1) {
            textoFaltante += "<li>Introducir el hoyo inicial</li>";
            flag = 0;
        }
        if (jugadoresEditarEquipo.length == 0) {
            textoFaltante += "<li>Introducir participantes al equipo</li>";
            flag = 0;
        }
        if (flag == 0) {
            let mensaje = document.getElementById('tituloEditar');
            let botonConfirmar = document.getElementById('editarEquipo');
            mensaje.innerHTML = "Por favor corregir los siguientes campos:";
            document.getElementById('contenidoConfirmacionEditar').innerHTML = textoFaltante;
            mensaje.style.display = "block";
            botonConfirmar.style.display = "none";

        } else {
            let botonConfirmar = document.getElementById('editarEquipo');
            let mensaje = document.getElementById('tituloEditar');
            mensaje.innerHTML = "¿Confirmar inscripción?";
            document.getElementById('contenidoConfirmacionEditar').innerHTML = "Presione confirmar para realizar la inscripcion";
            botonConfirmar.style.display = "block";
        }
    }
}
function mostrarRegistrosEditar($idEquipo) {
    $.post("../Controladores/controlador_mostrar_equipoEditar.php", {
        consulta: $("#caja_busquedaEditar").val(),
        idEquipo: $idEquipo
    }).done(function (data) {
        let ajaxResponse = document.getElementById('tablaParticipantesEditar');
        ajaxResponse.innerHTML = data;
        actualizarSeleccionadosEditar();
    });
}

function leerConsultaEditar($idEquipo) {
    mostrarRegistrosEditar($idEquipo);
}

function actualizarEquipoEditar() {
    let $result = "";
    let x = 0;
    while (x != jugadoresEditarEquipo.length) {
        $result += infojugadoresEditarEquipo[x];
        x = x + 1;
    }
    document.getElementById("integrantesEquipoTextoEditar").innerHTML = $result;
}
function actualizarSeleccionadosEditar() {
    for (let x in jugadoresEditarEquipo) {
       let idBoton = "boton";
       idBoton += jugadoresEditarEquipo[x];
       let idTabla = "participante";
       idTabla += jugadoresEditarEquipo[x];
       let rowParticipante= document.getElementById(idTabla);
       let botonAgregar = document.getElementById(idBoton);
       if (botonAgregar != null) {
          botonAgregar.style.display = "none";
          rowParticipante.style.backgroundColor = "rgb(240, 238, 238)"
       }
    }
 }
function añadirJugadorEditar(idP, nombre) {
    if (jugadoresEditarEquipo.indexOf(idP) == -1) {
        let $info = '<li class="collection-item" id="campoJugadorEquipo">';
        $info += idP;
        $info += '-';
        $info += nombre;
        $info += '<a id="campoJugadorEquipoBoton" class="btn btn-medium waves-effect waves-light pink darken-1 right align"><i onclick="eliminarJugadorEditar(';
        $info += idP;
        $info += ')' + '"';
        $info += ' class="material-icons">remove_circle</i></a>';
        $info += "</li>";
        infojugadoresEditarEquipo.push($info);
        jugadoresEditarEquipo.push(idP);
        let idTabla = "participante";
        idTabla += idP;
        let idBoton = "boton";
        idBoton += idP;
        document.getElementById(idBoton).style.display = "none";
        document.getElementById(idTabla).style.backgroundColor = "rgb(240, 238, 238)";
        actualizarEquipoEditar();

    }
}
function eliminarJugadorEditar(idP) {
    if (jugadoresEditarEquipo.length == 1 || jugadoresEditarEquipo.indexOf(idP) == 0) {
        jugadoresEditarEquipo.shift();
        infojugadoresEditarEquipo.shift();
    } else {
        let index1 = jugadoresEditarEquipo.indexOf(idP);
        infojugadoresEditarEquipo.splice(index1, 1);
        jugadoresEditarEquipo.splice(index1, 1);
    }
    let idTabla = "participante";
    idTabla += idP;
    let idBoton = "boton";
    idBoton += idP;
    document.getElementById(idBoton).style.display = "block";
    document.getElementById(idTabla).style.backgroundColor = "white";
    actualizarEquipoEditar();
}
function editarEquipo( idEquipoEditar) {
    let circulo = document.getElementById("circuloCarga_InscipcionEquipo");
    let forma = document.getElementById('formaEquipoEditar');
    //Comparar los numeros de participantes que se agregaron
    let numJugadores= jugadoresEditarEquipo.length;
    conseguirParticipantes();
    forma.style.display = "none";
    circulo.style.display = "block";
    $.post("../Controladores/controlador_editar_equipo.php", {
        arrayJugadoresAgregar: JSON.stringify(jugadoresEditarEquipo),
        arrayJugadoresEliminar: JSON.stringify(jugadores_Eliminar),
        tamañoEquipo: numJugadores,
        hoyoInicialEditar: $("#hoyoInicialEditar").val(),
        idEquipoEditar
     }).done(function (data) {
        if (data == 1) {
            circulo.style.display = "none";
            document.getElementById("wrap-editarEquipo").style.display="none";
            document.getElementById("mensajeEditarEquipo").style.display="block";
            document.getElementById("tituloMensajeEditarEquipo").innerHTML="Se edito el equipo satisfactoriamente";
            document.getElementById("textoMensajeEditarEquipo").innerHTML="El cambio se a guardado en la base de datos";
        } else {
            circulo.style.display = "none";
            document.getElementById("wrap-editarEquipo").style.display="none";
            document.getElementById("mensajeEditarEquipo").style.display="block";
            document.getElementById("tituloMensajeEditarEquipo").innerHTML="No se pudo editar el equipo";
            document.getElementById("textoMensajeEditarEquipo").innerHTML="Revisa tu conexion a internet o intentalo más tarde";
        }
     });
}
function conseguirParticipantes() {
    let i = 0;
    let j=jugadoresEditarEquipoActual.length;
    while (i < j) {
        if (jugadoresEditarEquipo.includes(jugadoresEditarEquipoActual[i])==true) {
            if (jugadoresEditarEquipo.length == 1 || jugadoresEditarEquipo.indexOf(jugadoresEditarEquipoActual[i]) == 0) {
                jugadoresEditarEquipo.shift();
            } else {
            let index1 = jugadoresEditarEquipo.indexOf(jugadoresEditarEquipoActual[i]);
            jugadoresEditarEquipo.splice(index1, 1);            }
        }else{
            jugadores_Eliminar.push(jugadoresEditarEquipoActual[i]);
        }
        i = i + 1;
    }
    
}