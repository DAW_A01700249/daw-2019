function consultaPatrocinadores(){
	let opcion = document.getElementById("ordenVer");
    let opcionVal = opcion.options[opcion.selectedIndex].value;
    let opcion2 = document.getElementById("patrociniosVer");
    let opcionVal2 = opcion2.options[opcion2.selectedIndex].value;
    let opcion3 = document.getElementById("anio");
    let opcionVal3 = opcion3.options[opcion3.selectedIndex].value;
    let status="";
    let orden="";
    if(opcionVal!="NA" && opcionVal2!="NA"){
        if(opcionVal2==1){
        	status="Pendiente";
        }else if(opcionVal2==2){
        	status="Aceptado";
        }else{
        	status="Rechazado";
        }
        if(opcionVal==1){
        	orden="1";
        }else{
        	orden="2";
        }
    }else{
        let error=document.getElementById('encabezadoMensajeVP');
        error.innerHTML="<h5>Por favor selecciona el estatus y orden por el cual te gustaría buscar</h5>";
        error.style.display="block";
        return 0;
    }
    let circulo=document.getElementById("circuloCarga_VerPatro");
    circulo.style.display="block";
    let select=document.getElementById('SelectVer');
    select.style.display="none";
    $.post("../Controladores/controlador_ver_patrocinador.php", {
        estatus: status,
        orden: orden,
        fecha: opcionVal3
    }).done(function (data) {
        let consulta=document.getElementById('CasosVer');
        consulta.style.display="block";
        let select=document.getElementById('SelectVer');
        select.style.display="none";
        let consulta2=document.getElementById('ConsultaVer');
        consulta2.innerHTML=data;
        let botonRegresar=document.getElementById('RegresarBuscarVer');
        botonRegresar.style.display="block";
        let circulo=document.getElementById("circuloCarga_VerPatro");
        circulo.style.display="none";
       });
}

function regresar(){
    let consulta=document.getElementById('CasosVer');
    consulta.style.display="none";
    let select=document.getElementById('SelectVer');
    select.style.display="block";
    let botonRegresar=document.getElementById('RegresarBuscarVer');
    botonRegresar.style.display="none";
    let error=document.getElementById('encabezadoMensajeVP');
    error.style.display="none";
    let circulo=document.getElementById("circuloCarga_VerPatro");
    circulo.style.display="none";
}

var habilitadaSeleccionarM=document.getElementById("botonVer");
if(habilitadaSeleccionarM!=null){
    habilitadaSeleccionarM.onclick = consultaPatrocinadores;
    var botonR=document.getElementById("RegresarBuscarVer");
    botonR.onclick = regresar;
}