

function eliminarEquipo($idEquipo) {
   console.log($idEquipo);
   let formaEliminar =document.getElementById("formaEliminarEquipo");
   let circuloCargaEliminar= document.getElementById("circuloCarga_EliminarEquipo");
   formaEliminar.style.display="none";
   circuloCargaEliminar.style.display="block";
   $.post("../Controladores/controlador_eliminar_equipo.php", {
      idEquipo: $idEquipo
  }).done(function (data) {
      if (data == 1) {
          circuloCargaEliminar.style.display="none";
          document.getElementById("wrap-container").style.display="none";
          document.getElementById("mensajeEliminarEquipo").style.display="block";
          document.getElementById("tituloMensajeEliminarEquipo").innerHTML="Se elimino el equipo satisfactoriamente";
          document.getElementById("textoMensajeEliminarEquipo").innerHTML="El cambio se a guardado en la base de datos";
          
      } else {
          circuloCargaEliminar.style.display="none";
          document.getElementById("wrap-container").style.display="none";
          document.getElementById("mensajeEliminarEquipo").style.display="block";
          document.getElementById("tituloMensajeEliminarEquipo").innerHTML="No se pudo eliminar el equipo";
          document.getElementById("textoMensajeEliminarEquipo").innerHTML="Revisa tu conexion a internet o intentalo más tarde";

      }
  });
}



