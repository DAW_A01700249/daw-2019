function editarDatosOrganizador(){
    if(!validarEditarOrg()){
        return 0;
    }

    let circulo=document.getElementById("circuloCarga_EditarOrg");
    circulo.style.display="block";
    let info=document.getElementById('formaEditarOrg');
            info.style.display="none";
    $.post("../Controladores/controlador_editar_organizador.php", {
        nombre: $("#nombreEditarOrg").val(),
        ap: $("#apEditarOrg").val(),
        am: $("#amEditarOrg").val(),
        mail: $("#correoEditarOrg").val(),
        telefono: $("#telefonoEditarOrg").val(),
        username: $("#userEditarOrg").val()

    }).done(function (data) {
        if(data==1){
            let info=document.getElementById('formaEditarOrg');
            info.style.display="none";
            let secMensaje=document.getElementById('mensajeEditarOrg');
            secMensaje.style.display="block";
            let consulta2=document.getElementById('confirmacionEditarOrg');
            consulta2.innerHTML="Se actualizaron los datos exitosamente";
            let circulo=document.getElementById("circuloCarga_EditarOrg");
            circulo.style.display="none";
        }else{
            let info=document.getElementById('formaEditarOrg');
            info.style.display="none";
            let secMensaje=document.getElementById('mensajeEditarOrg');
            secMensaje.style.display="block";
            let consulta2=document.getElementById('confirmacionEditarOrg');
            consulta2.innerHTML="No se pudieron actualizar los datos exitosamente";
            let circulo=document.getElementById("circuloCarga_EditarOrg");
            circulo.style.display="none";
        }
       });
}

function validarEditarOrg(){
    let nombre = $("#nombreEditarOrg").val();
    let aP = $("#apEditarOrg").val();
    let aM = $("#amEditarOrg").val();
    let mail= $("#correoEditarOrg").val();
    let telefono = $("#telefonoEditarOrg").val();
    if(aP.length < 1){
        let ajaxResponse=document.getElementById('encabezadoMensajeEditarOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";;
        return 0;
    }
    if(aM.length < 1){
        let ajaxResponse=document.getElementById('encabezadoMensajeEditarOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(nombre.length < 1){
        let ajaxResponse=document.getElementById('encabezadoMensajeEditarOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(mail.length < 1){

        let ajaxResponse=document.getElementById('encabezadoMensajeEditarOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if (mail.indexOf("@") == -1) {
            let ajaxResponse=document.getElementById('encabezadoMensajeEditarOrganizador');
            ajaxResponse.innerHTML="<h5>El formato del correo es incorrecto</h5>";
            ajaxResponse.style.display="block";
            return 0;
    }
    if (mail.indexOf(".") == -1) {
            let ajaxResponse=document.getElementById('encabezadoMensajeEditarOrganizador');
            ajaxResponse.innerHTML="<h5>El formato del correo es incorrecto</h5>";
            ajaxResponse.style.display="block";
            return 0;
    }
    if(telefono.length < 10){
        let ajaxResponse=document.getElementById('encabezadoMensajeEditarOrganizador');
        ajaxResponse.innerHTML="<h5>El teléfono debe contener 10 dígitos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    return 1;
}

var habilitadaSeleccionarM=document.getElementById("botonEditarOrg");
if(habilitadaSeleccionarM!=null){
    habilitadaSeleccionarM.onclick = editarDatosOrganizador;
}