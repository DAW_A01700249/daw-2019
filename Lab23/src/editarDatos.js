function crearTabla(actual_nombre, actual_aP, actual_aM, actual_Sexo, actual_edad, actual_correo, actual_telefono, actual_handicap, actual_IndEspeciales, actual_metPago, actual_facturacion, actual_autorizarFotos, actual_factRFC, actual_factRazonSocial, actual_factDireccionFiscal, actual_factIndicacionesEsp, $20, actual_indicaionEspecialPago, actual_estatusInscripcion, actual_estatusPago, actual_asistenciaTorneo, $25) {

    if (validarEditarRegistro() == true) {
        let tabla = "<table><thead><tr><th>Campo</th><th>Anterior</th><th>Nuevo</th></tr></thead><tbody>";
        let flag = 0;
        let estatusInscripcion = $("#estatusInscripcion").val();
        let estatusPago = $("#estatusPago").val();
        let asistenciaTorneo = $("#asistenciaTorneo").val();
        let nombre = $("#nombre").val();
        let apellidoP = $("#apellidoPaterno").val();
        let apellidoM = $("#apellidoMaterno").val();
        let sexo = $("#sexo").val();
        let edad = $("#edad").val();
        let telefono = $("#telefono").val();
        let email = $("#email").val();
        let handicap = $("#handicap").val();
        let indicacionEspecial = $("#indicacion_especial").val();
        let metodoPago = $("#metodoPago").val();
        let facturacion = $("#facturacion").val();
        let autorizarFotos = $("#autorizarFotos").val();
        let factRazonSocial = $("#facturacion_razonSocial").val();
        let factRFC = $("#facturacion_RFC").val();
        let factDireccionFiscal = $("#factDireccionFiscal").val();
        let factIndicacionEsp = $("#indicacion_especial_facturacion").val();
        let indicacionEspecialPago = $("#indicacion_especial_pago").val();

        if (actual_nombre != nombre) {
            tabla += "<tr><td>Nombre</td><td>" + actual_nombre + "</td><td>" + nombre + "</td></tr>";
            flag = 1;
        }
        if (actual_aP != apellidoP) {
            tabla += "<tr><td>Apellido Paterno</td><td>" + actual_aP + "</td><td>" + apellidoP + "</td></tr>";
            flag = 1;
        }
        if (actual_aM != apellidoM) {
            tabla += "<tr><td>Apellido Materno</td><td>" + actual_aM + "</td><td>" + apellidoM + "</td></tr>";
            flag = 1;
        }
        if (actual_Sexo != sexo) {
            tabla += "<tr><td>Sexo</td><td>" + actual_Sexo + "</td><td>" + sexo + "</td></tr>";
            flag = 1;
        }
        if (actual_edad != edad) {
            tabla += "<tr><td>Edad</td><td>" + actual_edad + "</td><td>" + edad + "</td></tr>";
            flag = 1;
        }
        if (actual_correo != email) {
            tabla += "<tr><td>Correo electrónico</td><td>" + actual_correo + "</td><td>" + email + "</td></tr>";
            flag = 1;
        }
        if (actual_telefono != telefono) {
            tabla += "<tr><td>Teléfono</td><td>" + actual_telefono + "</td><td>" + telefono + "</td></tr>";
            flag = 1;
        }
        if (actual_handicap != handicap) {
            tabla += "<tr><td>Handicap</td><td>" + actual_handicap + "</td><td>" + handicap + "</td></tr>";
            flag = 1;
        }
        if (actual_IndEspeciales != indicacionEspecial) {
            tabla += "<tr><td>Indicaciones Especiales</td><td>" + actual_IndEspeciales + "</td><td>" + indicacionEspecial + "</td></tr>";
            flag = 1;
        }
        if (actual_metPago != metodoPago) {
            tabla += "<tr><td>Metodo de Pago</td><td>" + actual_metPago + "</td><td>" + metodoPago + "</td></tr>";
            flag = 1;
        }
        if (actual_facturacion != facturacion) {
            tabla += "<tr><td>Facturación</td><td>" + actual_facturacion + "</td><td>" + facturacion + "</td></tr>";
            flag = 1;
        }
        if (actual_autorizarFotos != autorizarFotos) {
            tabla += "<tr><td>Autorización de fotos</td><td>" + actual_autorizarFotos + "</td><td>" + autorizarFotos + "</td></tr>";
            flag = 1;
        }
        if (actual_factRFC != factRFC) {
            tabla += "<tr><td>RFC</td><td>" + actual_factRFC + "</td><td>" + factRFC + "</td></tr>";
            flag = 1;
        }
        if (actual_factRazonSocial != factRazonSocial) {
            tabla += "<tr><td>Razón Social</td><td>" + actual_factRazonSocial + "</td><td>" + factRazonSocial + "</td></tr>";
            flag = 1;
        }
        if (actual_factDireccionFiscal != factDireccionFiscal) {
            tabla += "<tr><td>Dirección fiscal</td><td>" + actual_factDireccionFiscal + "</td><td>" + factDireccionFiscal + "</td></tr>";
            flag = 1;
        }
        if (actual_factIndicacionesEsp != factIndicacionEsp) {
            tabla += "<tr><td>Indicaiones especiales de facturación</td><td>" + actual_factIndicacionesEsp + "</td><td>" + factIndicacionEsp + "</td></tr>";
            flag = 1;
        }
        if (actual_indicaionEspecialPago != indicacionEspecialPago) {
            tabla += "<tr><td>Indicaiones especiales de Pago</td><td>" + actual_indicaionEspecialPago + "</td><td>" + indicacionEspecialPago + "</td></tr>";
            flag = 1;
        }
        if (actual_estatusInscripcion != estatusInscripcion) {
            tabla += "<tr><td>Estatus de inscripción</td><td>" + actual_estatusInscripcion + "</td><td>" + estatusInscripcion + "</td></tr>";
            flag = 1;
        }
        if (actual_estatusPago != estatusPago) {
            tabla += "<tr><td>Estatus de pago</td><td>" + actual_estatusPago + "</td><td>" + estatusPago + "</td></tr>";
            flag = 1;
        }
        if (actual_asistenciaTorneo != asistenciaTorneo) {
            tabla += "<tr><td>Asistencia al torneo</td><td>" + actual_asistenciaTorneo + "</td><td>" + asistenciaTorneo + "</td></tr>";
            flag = 1;
        }
        if (flag == 1) {
            tabla += "</tbody></table>";
            document.getElementById("editar_confirmacion").innerHTML = tabla;
        } else {
            let titulo_confirmacion = document.getElementById("titulo_confirmacion");
            let boton = document.getElementById("editarInscripcion");
            boton.style.display = "none";
            titulo_confirmacion.innerHTML = "No se realizo ningun cambio";
        }
    } else {

    }
}
function validarEditarRegistro() {
    let validacion=1;
    let textoFaltante = "<br>";
    let estatusInscripcion = $("#estatusInscripcion").val();
    let estatusPago = $("#estatusPago").val();
    let nombre = $("#nombre").val();
    let apellidoP = $("#apellidoPaterno").val();
    let apellidoM = $("#apellidoMaterno").val();
    let edad = $("#edad").val();
    let telefono = $("#telefono").val();
    let email = $("#email").val();
    let handicap = $("#handicap").val();
    let facturacion = $("#facturacion").val();
    let factRazonSocial = $("#facturacion_razonSocial").val();
    let factRFC = $("#facturacion_RFC").val();
    let factDireccionFiscal = $("#factDireccionFiscal").val();
    let factIndicacionEsp = $("#indicacion_especial_facturacion").val();
    if (estatusInscripcion == "Inscrito" && estatusPago == "Pendiente") {
        textoFaltante += "<li>Un jugador no puede estar inscrito sin antes tener su pago confirmado</li>";
        validacion = 0;
    }
    if (nombre.length < 1) {
        textoFaltante += "<li>Introducir valor en Nombre</li>";
        validacion = 0;
    }
    if (apellidoP.length < 1) {
        textoFaltante += "<li>Introducir valor en Nombre</li>";
        validacion = 0;
    }
    if (apellidoM.length < 1) {
        textoFaltante += "<li>Introducir valor en Nombre</li>";
        validacion = 0;
    }
    if (telefono.length < 1) {
        textoFaltante += "<li>Introducir valor en Teléfono</li>";
        validacion = 0;
    } else {
        if (telefono.length != 10) {
            textoFaltante += "<li>El teléfono no tiene 10 digitos</li>";
            validacion = 0;
        }
    }
    if (email.length < 1) {
        textoFaltante += "<li>Introducir valor en Correo electrónico</li>";
        validacion = 0;
    } else {
        if (email.indexOf("@") == -1) {
            textoFaltante += "<li>El formato del correo es incorrecto</li>";
            validacion = 0;
        }
    }
    if (edad.length < 1) {
        textoFaltante += "<li>Introducir valor en Edad</li>";
        validacion = 0;
    } else {
        if (edad < 1) {
            textoFaltante += "<li>Valor en Edad incorrecto</li>";
            validacion = 0;
        }
        if (edad > 110) {
            textoFaltante += "<li>Valor en Edad incorrecto</li>";
            validacion = 0;
        }
    }
    if (handicap.length < 1) {
        textoFaltante += "<li>Introducir valor en Handicap</li>";
        validacion = 0;
    } else {
        if (handicap > 36) {
            textoFaltante += "<li>El Handicap no puede ser mayor a 36</li>";
            validacion = 0;
        }
        if (handicap < 0) {
            textoFaltante += "<li>El Handicap no puede ser menor a 1</li>";
            validacion = 0;
        }
    }
    if (facturacion == "Sí") {
        if (factRazonSocial.length < 1) {
            textoFaltante += "<li>Introducir valor en Razón Social</li>";
            validacion = 0;
        }
        if (factRFC.length < 1) {
            textoFaltante += "<li>Introducir valor en RFC</li>";
            validacion = 0;
        } else {
            if (factRFC.length > 13) {
                textoFaltante += "<li>El RFC solo debe ser compuesto de 12 a 13 caracteres</li>";
                validacion = 0;
            }
            if (factRFC.length < 12) {
                textoFaltante += "<li>El RFC solo debe ser compuesto de 12 a 13 caracteres</li>";
                validacion = 0;
            }
        }
        if (factDireccionFiscal.length < 1) {
            textoFaltante += "<li>Introducir valor en Dirección Fiscal</li>";
            validacion = 0;
        }
    }
    if (validacion == 0) {
        let titulo_confirmacion = document.getElementById("titulo_confirmacion");
        let mensaje=document.getElementById("editar_confirmacion");
        let boton = document.getElementById("editarInscripcion");
        boton.style.display = "none";
        titulo_confirmacion.innerHTML = "Favor de corregir los siguientes puntos";
        mensaje.innerHTML=textoFaltante;
        return false;
    } else {
        let boton = document.getElementById("editarInscripcion");
        boton.style.display = "block";
        return true;
    }
}

function editarInscripcion() {
    let circulo=document.getElementById("circuloCarga_EditarOrganizador");
    let forma = document.getElementById('forma');
    forma.style.display = "none";
    circulo.style.display="block";
    $.post("../Controladores/controlador_editar_registro.php", {
        estatusInscripcion: $("#estatusInscripcion").val(),
        estatusPago: $("#estatusPago").val(),
        asistenciaTorneo: $("#asistenciaTorneo").val(),
        folio: $("#folio").val(),
        nombre: $("#nombre").val(),
        apellidoP: $("#apellidoPaterno").val(),
        apellidoM: $("#apellidoMaterno").val(),
        sexo: $("#sexo").val(),
        edad: $("#edad").val(),
        telefono: $("#telefono").val(),
        email: $("#email").val(),
        handicap: $("#handicap").val(),
        indicacionEspecial: $("#indicacion_especial").val(),
        metodoPago: $("#metodoPago").val(),
        facturacion: $("#facturacion").val(),
        autorizarFotos: $("#autorizarFotos").val(),
        factRazonSocial: $("#facturacion_razonSocial").val(),
        factRFC: $("#facturacion_RFC").val(),
        factDireccionFiscal: $("#factDireccionFiscal").val(),
        factIndicacionEsp: $("#indicacion_especial_facturacion").val(),
        indicacionEspecialPago: $("#indicacion_especial_pago").val(),
        idParticipante: $("#idParticipante").val()
    }).done(function (data) {
        if (data == 1) {
            let ajaxResponse = document.getElementById('mensaje');
            let titulo = document.getElementById('titulo');
            let subtitulo = document.getElementById('subtitulo');
            let confirmacion = document.getElementById('confirmacion');
            let confirmacion2 = document.getElementById('confirmacion2');
            circulo.style.display="none";
            ajaxResponse.style.display = "block";
            titulo.innerHTML = "Se edito el registro satisfactoriamente";
            subtitulo.innerHTML = "";
            confirmacion.innerHTML = "-Recuerde notificar al usuario sobre cualquie modificaion de informacion personal-";
            confirmacion2.innerHTML = "";
        } else {
            let ajaxResponse = document.getElementById('mensaje');
            let titulo = document.getElementById('titulo');
            let subtitulo = document.getElementById('subtitulo');
            let confirmacion = document.getElementById('confirmacion');
            let confirmacion2 = document.getElementById('confirmacion2');
            circulo.style.display="none";
            ajaxResponse.style.display = "block";
            titulo.innerHTML = "No se pudo editar el registro";
            subtitulo.innerHTML = "Revise la informacion del original y la registrada"
            confirmacion.innerHTML = "";
            confirmacion2.innerHTML = "";
        }
    });

}

document.getElementById("editarInscripcion").onclick = editarInscripcion;
