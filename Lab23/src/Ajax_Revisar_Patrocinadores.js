function consultaPatrocinadores(){
	let opcion = document.getElementById("orden");
    let opcionVal = opcion.options[opcion.selectedIndex].value;
    let opcion2 = document.getElementById("patrocinios");
    let opcionVal2 = opcion2.options[opcion2.selectedIndex].value;
    let status="";
    let orden="";
    if(opcionVal!="NA" && opcionVal2!="NA"){
        if(opcionVal2==1){
        	status="Pendiente";
        }else if(opcionVal2==2){
        	status="Aceptado";
        }else{
        	status="Rechazado";
        }
        if(opcionVal==1){
        	orden="1";
        }else{
        	orden="2";
        }
    }else{
        let error=document.getElementById('MRP');
        error.innerHTML="<h5>Por favor selecciona el estatus y el orden en el cual buscar</h5>";
        error.style.display="block";
        return 0;
    }
    $.post("../Controladores/controlador_consulta_patrocinador.php", {
        estatus: status,
        ordenB: orden
    }).done(function (data) {
        let consulta=document.getElementById('Casos');
        consulta.style.display="block";
        let select=document.getElementById('SelectRP');
        select.style.display="none";
        let consulta2=document.getElementById('Consulta');
        consulta2.innerHTML=data;
        let botonRegresar=document.getElementById('RegresarBuscar');
        botonRegresar.style.display="block";
       });
}

function regresar(){
    let consulta=document.getElementById('Casos');
    consulta.style.display="none";
    let select=document.getElementById('SelectRP');
    select.style.display="block";
    let botonRegresar=document.getElementById('RegresarBuscar');
    botonRegresar.style.display="none";
    let error=document.getElementById('MRP');
    error.style.display="none";
}

var habilitadaSeleccionarM=document.getElementById("botonRP");
if(habilitadaSeleccionarM!=null){
    habilitadaSeleccionarM.onclick = consultaPatrocinadores;
    var botonR=document.getElementById("RegresarBuscar");
    botonR.onclick = regresar;
}