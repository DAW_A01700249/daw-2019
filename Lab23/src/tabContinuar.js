let tabSiguiente = "test6";
let facturacionActivo = 0;
let cargoActivo = 0;
//Informacion Personal
$(document).ready(function () {
  $('#tabs').tabs();
  $("#test1Continue").click(function () {
    if(validacionDatosP==true){
      $('#tabs').tabs('select', 'test2');
    }
  });
});
//Datos de juego
$(document).ready(function () {
  $('#tabs').tabs();
  $("#test2Continue").click(function () {
    if(validacionDatosJuego==true){
      $('#tabs').tabs('select', 'test3');
    }
  });
});
$(document).ready(function () {
  $('#tabs').tabs();
  $("#test2Back").click(function () {
    $('#tabs').tabs('select', 'test1');
  });
});
//Datos Administrativos
$(document).ready(function () {
  $('#tabs').tabs();
  $("#test3Continue").click(function () {
    if(validacionDatosAdministrativos==true){
      $('#tabs').tabs('select', tabSiguiente);
    }
  });
});
$(document).ready(function () {
  $('#tabs').tabs();
  $("#test3Back").click(function () {
    $('#tabs').tabs('select', 'test2');
  });
});
//Datos cargo
$(document).ready(function () {
  $('#tabs').tabs();
  $("#test4Continue").click(function () {
    $('#tabs').tabs('select', tabSiguiente);
  });
});
$(document).ready(function () {
  $('#tabs').tabs();
  $("#test4Back").click(function () {
    $('#tabs').tabs('select', tabPasada);
  });
});
//Datos Factura
$(document).ready(function () {
  $('#tabs').tabs();
  $("#test5Continue").click(function () {
    if(validacionDatosFacturacion==true){
      $('#tabs').tabs('select', tabSiguiente);
    }
  });
});
$(document).ready(function () {
  $('#tabs').tabs();
  $("#test5Back").click(function () {
    $('#tabs').tabs('select', tabPasada);
  });
});
//Fin
$(document).ready(function () {
  $('#tabs').tabs();
  $("#test6Back").click(function () {
    $('#tabs').tabs('select', tabPasada);
  });
});
function actualizarTab(x) {
  if (x == 3) {
    if (cargoActivo == 1) {
      tabSiguiente = 'test4';
    } else if (facturacionActivo == 1) {
      tabSiguiente = 'test5';
    } else {
      tabSiguiente = 'test6';
    }
  }
  if (x == 4) {
    if (facturacionActivo == 1) {
      tabSiguiente = 'test5';
      tabPasada = 'test4';
    } else {
      tabSiguiente = 'test6';
      tabPasada = 'test3';
    }
  } else if(x==-4){
    tabPasada = 'test3';
  }
  if (x == 5) {
    if (cargoActivo == 1) {
      tabSiguiente = 'test6';
      tabPasada = 'test5';
    } else {
      tabSiguiente = 'test6';
      tabPasada = 'test3';
    }
  } else if(x==-5){
    if (cargoActivo == 1) {
      tabPasada = 'test4';
    } else {
      tabPasada = 'test3';
    }
  }
  if(x==6){
    tabPasada='test3';
    if(facturacionActivo==1){
      tabPasada='test5';
    }
    if(cargoActivo==1 && facturacionActivo==0){
      tabPasada='test4';
    }
  }
}
