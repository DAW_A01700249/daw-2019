
function registrarOrganizador() {
    //$.post manda la petición asíncrona por el método post. También existe $.get
    if(!validar()){
        return 0;
    }
    let circulo=document.getElementById("circuloCarga_AltaOrg");
    circulo.style.display="block";
    let forma=document.getElementById('forma');
    forma.style.display="none";
    $.post("../Controladores/controlador_registro_organizador.php", {
        nombre: $("#nombre").val(),
        apellidoP: $("#apellido1").val(),
        apellidoM: $("#apellido2").val(),
        telefono: $("#telefono").val(),
        email: $("#email").val(),
        username: $("#username").val(),
        password: $("#pass").val()
    }).done(function (data) {
        if(data==1){
            let forma=document.getElementById('forma');
            forma.style.display="none";
            let ajaxResponse=document.getElementById('mensaje');
            let titulo=document.getElementById('titulo');
            let circulo=document.getElementById("circuloCarga_AltaOrg");
            circulo.style.display="none";
            ajaxResponse.style.display="block";
            titulo.innerHTML = "Se registró exitosamente al organizador";
        }else{
            let ajaxResponse=document.getElementById('mensaje');
            let titulo=document.getElementById('titulo');
            let forma=document.getElementById('forma');
            let circulo=document.getElementById("circuloCarga_AltaOrg");
            circulo.style.display="none";
            forma.style.display="none";
            ajaxResponse.style.display="block";
            titulo.innerHTML="No se pudo registrar exitosamente al organizador. Intenta asignarle otro nombre de usuario";
            let iconoRespuesta=document.getElementById('iconoRespuesta');
            iconoRespuesta.innerHTML="error";
            
        }
    });
}

function validar(){
    let telefono = $("#telefono").val();
    let aP = $("#apellido1").val();
    let aM = $("#apellido2").val();
    let nombre= $("#nombre").val();
    let mail = $("#email").val();
    let user = $("#username").val();
    let pass = $("#pass").val();
    if(aP.length < 1){
        let ajaxResponse=document.getElementById('encabezadoMensajeOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";;
        return 0;
    }
    if(aM.length < 1){
        let ajaxResponse=document.getElementById('encabezadoMensajeOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(nombre.length < 1){
        let ajaxResponse=document.getElementById('encabezadoMensajeOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(mail.length < 1){

        let ajaxResponse=document.getElementById('encabezadoMensajeOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if (mail.indexOf("@") == -1) {
            let ajaxResponse=document.getElementById('encabezadoMensajeOrganizador');
            ajaxResponse.innerHTML="<h5>El formato del correo es incorrecto</h5>";
            ajaxResponse.style.display="block";
            return 0;
    }
    if (mail.indexOf(".") == -1) {
            let ajaxResponse=document.getElementById('encabezadoMensajeOrganizador');
            ajaxResponse.innerHTML="<h5>El formato del correo es incorrecto</h5>";
            ajaxResponse.style.display="block";
            return 0;
    }
    if(user.length < 1){
        let ajaxResponse=document.getElementById('encabezadoMensajeOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(telefono.length < 10){
        let ajaxResponse=document.getElementById('encabezadoMensajeOrganizador');
        ajaxResponse.innerHTML="<h5>El teléfono debe contener 10 dígitos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(pass.length < 6){
        let ajaxResponse=document.getElementById('encabezadoMensajeOrganizador');
        ajaxResponse.innerHTML="<h5>La contraseña es demasiado corta, debe tener al menos 6 caracteres</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    return 1;
}

function selecMetodo(){
    let opcion = document.getElementById("seleccionarBusqueda");
    let opcionVal = opcion.options[opcion.selectedIndex].value;
    if(opcionVal==1){
        let bloque=document.getElementById('metodosBusquedaUser');
        bloque.style.display="block";
        let seleccion=document.getElementById('primerSelect');
        seleccion.style.display="none";
    }else if(opcionVal==2){
        let bloque=document.getElementById('metodosBusquedaNombre');
        bloque.style.display="block";
        let seleccion=document.getElementById('primerSelect');
        seleccion.style.display="none";
    }
    let boton = document.getElementById("botonBorrar");
    boton.style.display="block";
}


function eliminarOrganizador(){
    let opcion = document.getElementById("selectN");
    let opcionVal = opcion.options[opcion.selectedIndex].value;
    let opcion2 = document.getElementById("organizador");
    let opcionVal2 = opcion2.options[opcion2.selectedIndex].value;
    let valor="";
    let circulo=document.getElementById("circuloCarga_BajaOrg");
    circulo.style.display="block";
    let metBusqueda=document.getElementById('metodosBusquedaUser');
    metBusqueda.style.display="none";
    let metBusqueda2=document.getElementById('metodosBusquedaNombre');
    metBusqueda2.style.display="none";
    let bBorrar=document.getElementById('botonBorrar');
    bBorrar.style.display="none";
    if(opcionVal=="NA" && opcionVal2!="NA"){
        valor = opcionVal2;
    }else if(opcionVal!="NA" && opcionVal2=="NA"){
        valor = opcionVal;
    }else{
        return 0;
    }  
    $.post("../Controladores/controlador_baja_organizador.php", {
        user: valor
    }).done(function (data) {
        if(data==1){
            let ajaxResponse=document.getElementById('menasjeBajaOrg');
            ajaxResponse.style.display="block";
            let metBusqueda=document.getElementById('metodosBusquedaUser');
            metBusqueda.style.display="none";
            let metBusqueda2=document.getElementById('metodosBusquedaNombre');
            metBusqueda2.style.display="none";
            let bBorrar=document.getElementById('botonBorrar');
            bBorrar.style.display="none";
            let circulo=document.getElementById("circuloCarga_BajaOrg");
            circulo.style.display="none";
        }else{
            let ajaxResponse=document.getElementById('menasjeBajaOrg');
            ajaxResponse.style.display="block";
            let metBusqueda=document.getElementById('metodosBusquedaUser');
            metBusqueda.style.display="none";
            let metBusqueda2=document.getElementById('metodosBusquedaNombre');
            metBusqueda2.style.display="none";
            let titulo=document.getElementById('tituloBajaOrganizador');
            titulo.innerHTML = "No se eliminó exitosamente al organizador";
            let bBorrar=document.getElementById('botonBorrar');
            bBorrar.style.display="none";
            let circulo=document.getElementById("circuloCarga_BajaOrg");
            circulo.style.display="none";
        }
    });
}


var habilitadaSeleccionarM=document.getElementById("seleccionarMetodo");
if(habilitadaSeleccionarM!=null){
    habilitadaSeleccionarM.onclick = selecMetodo;
    let borrar = document.getElementById("borrarOrg");
    borrar.onclick = eliminarOrganizador;
}
var habilitadaRegistrarO=document.getElementById("registrarOrg")
if(habilitadaRegistrarO!=null){
    habilitadaRegistrarO.onclick = registrarOrganizador;
}

function registrarTorneo() {
    //console.log($("#fechaT").val(),$("#cierreT").val());
    //$.post manda la petición asíncrona por el método post. También existe $.get
    if(!validarELTorneo()){
        return 0;
    }
    console.log("lo que quiera");
    $.post("../Controladores/controlador_registro_torneo.php", {
        nombreT: $("#nombreT").val(),
        preventaT: $("#preventaT").val(),
        fechaP: $("#fechaP").val(),
        fechaC: $("#fechaC").val(),
        fecha: $("#fechaT").val(),
        horaR: $("#HoraR").val(),
        horaI: $("#HoraI").val(),
        lugar: $("#lugar").val(),
        costo: $("#costo").val(),
        cierre: $("#cierreT").val(),
        cuentaT: $("#cuentaT").val(),
        clabeT: $("#clabeT").val(),
        bancoT: $("#bancoT").val(),
        beneT: $("#beneT").val()
        
    }).done(function (data) {
        console.log(data);
        if(data==1){
            let ajaxResponse=document.getElementById('mensaje');
            let titulo2=document.getElementById('titulo2');
            ajaxResponse.style.display="block";
            titulo2.innerHTML = "Se registró exitosamente al torneo";
            let forma=document.getElementById('forma');
            forma.style.display="none";
        }else{
            let ajaxResponse=document.getElementById('mensaje');
            let titulo2=document.getElementById('titulo2');
            ajaxResponse.style.display="block";
            titulo2.innerHTML="No se pudo registrar exitosamente al torneo";
            let forma=document.getElementById('forma');
            forma.style.display="none";
            let iconoRespuesta=document.getElementById('iconoRespuesta');
            iconoRespuesta.innerHTML="error";
            
        }
    });
}

function validarELTorneo(){
    let nombreT = $("#nombreT").val();
    let preventaT = $("#preventaT").val();
    let fechaP = $("#fechaP").val();
    let fechaC = $("#fechaC").val();
    let fechaT = $("#fechaT").val();
    let horaR = $("#HoraR").val();
    let horaI = $("#HoraI").val();
    let lugar= $("#lugar").val();
    let costo = $("#costo").val();
    let cierre = $("#cierreT").val();
    let cuentaT = $("#cuentaT").val();
    let clabeT = $("#clabeT").val();
    let bancoT = $("#bancoT").val();
    let beneT = $("#beneT").val();

    if(nombreT.length < 1){
        let ajaxResponse=document.getElementById('MensajeOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";;
        return 0;
    }
    if(preventaT.length < 1){
        let ajaxResponse=document.getElementById('MensajeOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";;
        return 0;
    }
    if(fechaP.length < 1){
        let ajaxResponse=document.getElementById('MensajeOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";;
        return 0;
    }
    if(fechaC.length < 1){
        let ajaxResponse=document.getElementById('MensajeOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";;
        return 0;
    }
    if(fechaT.length < 1){
        let ajaxResponse=document.getElementById('MensajeOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";;
        return 0;
    }
    if(horaR.length < 1){
        let ajaxResponse=document.getElementById('MensajeOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(horaI.length < 1){
        let ajaxResponse=document.getElementById('MensajeOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(lugar.length < 1){

        let ajaxResponse=document.getElementById('MensajeOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(costo.length < 1){
        let ajaxResponse=document.getElementById('MensajeOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(cierre.length < 1){
        let ajaxResponse=document.getElementById('MensajeOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(cuentaT.length < 1){
        let ajaxResponse=document.getElementById('MensajeOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(clabeT.length < 1){
        let ajaxResponse=document.getElementById('MensajeOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(bancoT.length < 1){
        let ajaxResponse=document.getElementById('MensajeOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(beneT.length < 1){
        let ajaxResponse=document.getElementById('MensajeOrganizador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    
    return 1;
}

var habilitadaRegistrarT=document.getElementById("registrarTor")
if(habilitadaRegistrarT!=null){
    habilitadaRegistrarT.onclick = registrarTorneo;
}


//funciones para registrar una cuenta
function registrarCuenta() {
    //$.post manda la petición asíncrona por el método post. También existe $.get
    if(!validarCuenta()){
        return 0;
    }
    $.post("../Controladores/controlador_registro_cuenta.php", {
        email: $("#email").val(),
        username: $("#username").val(),
        password: $("#pass").val()
    }).done(function (data) {
        if(data==1){
            let forma=document.getElementById('forma');
            forma.style.display="none";
            let ajaxResponse=document.getElementById('mensaje');
            let titulo=document.getElementById('titulo');
            ajaxResponse.style.display="block";
            titulo.innerHTML = "Se registró exitosamente al sistema";
        }else{
            let ajaxResponse=document.getElementById('mensaje');
            let titulo=document.getElementById('titulo');
            let forma=document.getElementById('forma');
            forma.style.display="none";
            ajaxResponse.style.display="block";
            titulo.innerHTML="No se pudo registrar exitosamente al sistema. Intenta asignarle otro nombre de usuario";
            let iconoRespuesta=document.getElementById('iconoRespuesta');
            iconoRespuesta.innerHTML="error";
            
        }
    });
}

function validarCuenta(){
    let mail = $("#email").val();
    let user = $("#username").val();
    let pass = $("#pass").val();
    if(mail.length < 1){

        let ajaxResponse=document.getElementById('encabezadoMensajeCuenta');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(user.length < 1){
        let ajaxResponse=document.getElementById('encabezadoMensajeCuenta');
        ajaxResponse.innerHTML="<h5>Por favor introduce información en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(pass.length < 6){
        let ajaxResponse=document.getElementById('encabezadoMensajeCuenta');
        ajaxResponse.innerHTML="<h5>La contraseña es demasiado corta, debe tener al menos 6 caracteres</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    return 1;
}

var habilitadaRegistrarO=document.getElementById("registrarCuenta")
if(habilitadaRegistrarO!=null){
    habilitadaRegistrarO.onclick = registrarCuenta;
}