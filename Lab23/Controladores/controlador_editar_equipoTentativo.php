<?php
    session_start();
    require_once("../Models/modelEditarEquipo.php");
    $arrayAgregar = htmlspecialchars($_POST["arrayJugadoresAgregarT"]);
    $arrayEliminar = htmlspecialchars($_POST["arrayJugadoresEliminarT"]);
    $nuevoTamañoEquipo=htmlspecialchars($_POST["tamañoEquipoT"]);
    $idEquipo=htmlspecialchars($_POST["idEquipoEditar"]);

    echo editarEquipoTentativo(json_decode($arrayAgregar, true),json_decode($arrayEliminar, true),$nuevoTamañoEquipo,$idEquipo);
    
?>