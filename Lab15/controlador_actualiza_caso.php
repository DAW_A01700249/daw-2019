<?php
  session_start();
  require_once("model.php");  

  $_POST["numero_caso"] = htmlspecialchars($_POST["numero_caso"]);
  $_POST["estado"] = htmlspecialchars($_POST["estado"]);

  if(isset($_POST["numero_caso"])) {
      if (actualizar_registro($_POST["numero_caso"],$_POST["estado"])) {
          $_SESSION["mensaje"] = "Se actualizo el caso";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al actualizar el caso";
      }
  }

  header("location:index.php");
?>