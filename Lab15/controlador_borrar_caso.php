<?php
  session_start();
  require_once("model.php");  

  $_POST["caso"] = htmlspecialchars($_POST["caso"]);

  if(isset($_POST["caso"])) {
      if (borrar_registro($_POST["caso"])) {
          $_SESSION["mensaje"] = "Se borró el caso";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al borrar el caso";
      }
  }

  header("location:index.php");
?>