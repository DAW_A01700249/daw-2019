<?php 
  session_start();
  require_once("../Models/modelEditarEquipo.php"); 

  //recuperar el caso que se va a editar
  $equipo_id = htmlspecialchars($_GET["equipo_id"]);
  $jugadoresEditarEquipo= recuperarEquipo($equipo_id);
  $idParticipantes=[];
  $nombresParticipantes=[];
  $hoyoInicial=$jugadoresEditarEquipo[2];
  for ($i=0; $i < sizeof($jugadoresEditarEquipo[0]); $i++) { 
    array_push($idParticipantes,$jugadoresEditarEquipo[0][$i]);
  }
  for ($i=0; $i < sizeof($jugadoresEditarEquipo[0]); $i++) { 
    array_push($nombresParticipantes,$jugadoresEditarEquipo[1][$i]);
  }
  $json_idParticipantes=json_encode($idParticipantes);
  $json_nombresParticipantes=json_encode($nombresParticipantes);
  
  include("header.html");  
  include("editarEquipo.html");
  include("footer.html");
  echo "<script>
   recuperarInfo($json_idParticipantes,$json_nombresParticipantes,$hoyoInicial); 
   </script>";
?>