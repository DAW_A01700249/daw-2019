<?php
     
   function conectar_bd() {
      $conexion_bd = mysqli_connect("localhost","root","","torneogolf");
      if ($conexion_bd == NULL) {
          die("No se pudo conectar con la base de datos");
      }
      return $conexion_bd;
  }

  //función para desconectarse de una bd
  //@param $conexion: Conexión de la bd que se va a cerrar
  function desconectar_bd($conexion_bd) {
      mysqli_close($conexion_bd);
  }

  function mostrarTorneos(){
  $conexion_bd = conectar_bd();
  $resultado = '    <div class="col s7">
<table class="highlight"><thead>
  <tr>
  <th>Nombre del torneo</th>
  <th>Activo</th>
  <th>Editar</th>

  </tr>
  </thead>';

  $consulta = 'SELECT nombre, activo, idTorneo FROM torneo';
  $resultados = $conexion_bd->query($consulta);
  while($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)){
    $resultado .= "<div><tr>";
    $resultado .= "<td>".$row['nombre']."</td>";
    if($row['activo'] == 1){
      $resultado .= "<td>Sí</td>";
    } else {
      $resultado .= "<td>No</td>";
    }
        $resultado .= '<td><a href="editarTorneo.php?caso_id='.$row['idTorneo'].' "class="btn " id="botonesA"><i class="material-icons">create</i></a></td>'; 
        $resultado .= "</tr>";
      }

    mysqli_free_result($resultados); //Liberar la memoria

    desconectar_bd($conexion_bd); 

    $resultado .= "</tbody></table></div>";
  
    return $resultado;
  }


  function recuperar_info($idTorneo) {
        $conexion_bd = conectar_bd();  
        
        $consulta = 'SELECT * FROM torneo WHERE iDTorneo="'.$idTorneo.'"';
        $resultados = $conexion_bd->query($consulta);
        while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
            $array =
            [
              $row["iDTorneo"],
              $row["fechaTorneo"],
              $row["horaRegistro"],
              $row["horaInicio"],
              $row["lugar"],
              $row["costo"],
              $row["cierreTorneo"],
              $row["nombre"],
              $row["preventa"],
              $row["fechaPreventa"],
              $row["fechaCosto"],
              $row["activo"],
              $row["cuenta"],
              $row["clabe"],
             $row["beneficiario"],
             $row["banco"],
            ];
            if($row["activo"]==1){
              $array[16] = "Activo";
            } else {
              $array[16] = "Inactivo";
            }
            $array[17] = $idTorneo;
            desconectar_bd($conexion_bd);
            return $array;
        }
            
        desconectar_bd($conexion_bd);
        return 0;      
  }

function crear_select($id, $columna_descripcion, $tabla, $seleccion=0) {
    $conexion_bd = conectar_bd();  
      
    $resultado = '<div class="input-field"><select name="'.$tabla.'" id="'.$tabla.'"><option value="" disabled selected>Selecciona una opción</option>';
            
    $consulta = "SELECT $id, $columna_descripcion FROM $tabla";
    $resultados = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= '<option value="'.$row["$id"].'" ';
        if($seleccion == $row["$id"]) {
            $resultado .= 'selected';
        }
        $resultado .= '>'.$row["$columna_descripcion"].'</option>';
    }
        
    desconectar_bd($conexion_bd);
    $resultado .=  '</select><label>'.$tabla.'...</label></div>';
    return $resultado;
  }


function getOpciones($id, $campo, $tabla,$seleccion=0) {
    $conexion_bd = conectar_bd();  
      
    $resultado = '';

    $consulta ="SELECT count(*) as cuenta, idTorneo FROM torneo WHERE activo = 1";
    $result = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
        $array = [ 
          $row["cuenta"],
          $row["idTorneo"]
        ];
    }
    

     if($array[0] >= 1 ){
      if($id == $array[1]){
          $resultado .= '<option value=0>Desactivado</option>';
          $resultado .= '<option selected value=1>Activo</option>';
          $resultado .=  '</select>';
          $resultado .=  '<label for="torneoActivo">Activar o desactivar torneo</label>';
      } else {
          $resultado .= '<option selected value=0>Desactivado</option></select>';
          $resultado .=  '<label for="torneoActivo">Activar o desactivar torneo</label>';
          $resultado .= '<p>Este torneo no se puede activar debido a que ya existe uno activo.</p>';
      }  
    } else {
      $resultado .= '<option value=0>Desactivado</option>';
      $resultado .=  '<label for="torneoActivo">Activar o desactivar torneo</label>';
      $resultado .= '<option value=1>Activo</option>';
      $resultado .=  '</select>';
    }
        
    desconectar_bd($conexion_bd);
    return $resultado;
}

function editarTorneo($caso_id, $fecha,$horaR,$horaI,$lugar,$costo,$cierre,$nombreT,$preventaT,$fechaP,$fechaC,$cuentaT,$clabeT,$bancoT,$beneT,$torneoActivo){
        $conexion_bd = conectar_bd();
        $dml = 'UPDATE torneo
        SET fechaTorneo=(?),horaRegistro=(?),horaInicio=(?),lugar=(?),costo=(?),cierreTorneo=(?),nombre=(?),preventa=(?),fechaPreventa=(?),fechaCosto=(?),activo=(?),cuenta=(?),clabe=(?),banco=(?),beneficiario=(?)
        WHERE idTorneo=(?)';

        if ( !($statement = $conexion_bd->prepare($dml)) ) {
            die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
            return 0;
        }
          
        //Unir los parámetros de la función con los parámetros de la consulta   
        //El primer argumento de bind_param es el formato de cada parámetro
        if (!$statement->bind_param("ssssissississssi",$fecha,$horaR, $horaI, $lugar, $costo, $cierre,$nombreT,$preventaT,$fechaP,$fechaC,$torneoActivo,$cuentaT,$clabeT,$bancoT,$beneT,$caso_id)) {
            die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
            return 0;
           
        }
        //Executar la consulta
        if (!$statement->execute()) {
          die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
            return 0;
            
        }
    
        desconectar_bd($conexion_bd);
          return 1;
      }

?>