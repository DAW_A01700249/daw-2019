<?php
    function conectar_bd() {
      $conexion_bd = mysqli_connect("localhost","root","","torneogolf");
      if ($conexion_bd == NULL) {
          die("No se pudo conectar con la base de datos");
      }
      return $conexion_bd;
  }
  
    //función para desconectarse de una bd
    //@param $conexion: Conexión de la bd que se va a cerrar
    function desconectar_bd($conexion_bd) {
        mysqli_close($conexion_bd);
    }
    //funcion para recuperar la informacion de las inscripciones en la interfaz del organizador
    //funcion para recuperar la informacion de las inscripciones en la interfaz del organizador
    function recuperar_info($folio) {
      $conexion_bd = conectar_bd();  
      
      $consulta = "SELECT * FROM participante as P,participante_torneo as PT WHERE folio=$folio AND PT.idParticipante=P.idParticipante";
      $resultados = $conexion_bd->query($consulta);
      while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
          $array =
          [
              $row["folio"],
              $row["nombreUsuario"],
              $row["idTorneo"],
              $row["nombre"],
              $row["apellidoPaterno"],
              $row["apellidoMaterno"],
              $row["sexo"],
              $row["edad"],
              $row["correo"],
              $row["telefono"],
              $row["handicap"],
              $row["indicacionEspecial"],
              $row["metodoPago"],
              $row["facturacion"],
              $row["autorizarFotos"],
              $row["RFC"],
              $row["razonSocial"],
              $row["domicilioFiscal"],
              $row["comentariosFacturacion"],
              $row["idFotoRecibo"],
              $row["indicacionPagoEspecial"],
              $row["estatusInscripcion"],
              $row["estatusPago"],
              $row["asistenciaAlTorneo"],
              $row["idParticipante"]
          ];
          desconectar_bd($conexion_bd);
          return $array;
      }
          
      desconectar_bd($conexion_bd);
      return 0;
    }

      function crear_SelectEstatusRegistro($seleccion){
      $resultado = '<select id="estatusInscripcion"><option value="" disabled selected>Selecciona una opción</option>';
      if($seleccion=='Pendiente'){
        $resultado .= '<option value="Pendiente" selected>Pendiente</option>';
        $resultado .= '<option value="Inscrito">Inscrito</option>';
        }
        if($seleccion=='Inscrito'){
          $resultado .= '<option value="Pendiente">Pendiente</option>';
          $resultado .= '<option value="Inscrito" selected>Inscrito</option>';
          }
        $resultado .=  '</select>';
        return $resultado;
      }
      function crear_SelectEstatusPago($seleccion){
        $resultado = '<select id="estatusPago"><option value="" disabled selected>Selecciona una opción</option>';
      if($seleccion=='Pendiente'){
        $resultado .= '<option value="Pendiente" selected>Pendiente</option>';
        $resultado .= '<option value="Pagado">Pagado</option>';
        }
        if($seleccion=='Pagado'){
          $resultado .= '<option value="Pendiente">Pendiente</option>';
          $resultado .= '<option value="Pagado" selected>Pagado</option>';
          }
        $resultado .=  '</select>';
        return $resultado;
      }
      function crear_SelectSexo($seleccion){
        $resultado = '<select id="sexo"><option value="" disabled selected>Selecciona una opción</option>';
        if($seleccion=='Hombre'){
        $resultado .= '<option value="Hombre" selected>Hombre</option>';
        $resultado .= '<option value="Mujer">Mujer</option>';
        }
        if($seleccion=='Mujer'){
        $resultado .= '<option value="Hombre">Hombre</option>';
        $resultado .= '<option value="Mujer" selected>Mujer</option>';
        }
        $resultado .=  '</select>';
        return $resultado;
      }
      function crear_SelectSiNo($tabla,$seleccion){
        $resultado = '<select id="'.$tabla.'"><option value="" disabled selected>Selecciona una opción</option>';
        if($seleccion=='Sí'){
        $resultado .= '<option value="Sí" selected>Sí</option>';
        $resultado .= '<option value="No">No</option>';
        }
        if($seleccion=='No'){
        $resultado .= '<option value="Sí">Sí</option>';
        $resultado .= '<option value="No" selected>No</option>';
        }
        $resultado .=  '</select>';
        return $resultado;
      }
      function crear_selectMetodoPago($seleccion) {
        $resultado = '<select id="metodoPago"><option value="" disabled selected>Selecciona una opción</option>';
        if($seleccion=='Transferencia electrónica'){
        $resultado .= '<option value="Transferencia electrónica" selected>Transferencia electrónica</option>';
        $resultado .= '<option value="Depósito en efectivo">Depósito en efectivo</option>';
        $resultado .= '<option value="Cargo a tarjeta de crédito">Cargo a tarjeta de crédito</option>';
        $resultado .= '<option value="Pago en efectivo previo al evento">Pago en efectivo previo al evento</option>';
        $resultado .= '<option value="Pago en efectivo el día del evento">Pago en efectivo el día del evento </option>';
        }
        if($seleccion=='Depósito en efectivo'){
        $resultado .= '<option value="Transferencia electrónica">Transferencia electrónica</option>';
        $resultado .= '<option value="Depósito en efectivo" selected>Depósito en efectivo</option>';
        $resultado .= '<option value="Cargo a tarjeta de crédito">Cargo a tarjeta de crédito</option>';
        $resultado .= '<option value="Pago en efectivo previo al evento">Pago en efectivo previo al evento</option>';
        $resultado .= '<option value="Pago en efectivo el día del evento">Pago en efectivo el día del evento </option>';
        }
        if($seleccion=='Cargo a tarjeta de crédito'){
        $resultado .= '<option value="Transferencia electrónica">Transferencia electrónica</option>';
        $resultado .= '<option value="Depósito en efectivo">Depósito en efectivo</option>';
        $resultado .= '<option value="Cargo a tarjeta de crédito" selected>Cargo a tarjeta de crédito</option>';
        $resultado .= '<option value="Pago en efectivo previo al evento">Pago en efectivo previo al evento</option>';
        $resultado .= '<option value="Pago en efectivo el día del evento">Pago en efectivo el día del evento </option>';
        }
        if($seleccion=='Pago en efectivo previo al evento'){
        $resultado .= '<option value="Transferencia electrónica">Transferencia electrónica</option>';
        $resultado .= '<option value="Depósito en efectivo">Depósito en efectivo</option>';
        $resultado .= '<option value="Cargo a tarjeta de crédito">Cargo a tarjeta de crédito</option>';
        $resultado .= '<option value="Pago en efectivo previo al evento" selected>Pago en efectivo previo al evento</option>';
        $resultado .= '<option value="Pago en efectivo el día del evento">Pago en efectivo el día del evento </option>';
        }
        if($seleccion=='Pago en efectivo el día del evento'){
        $resultado .= '<option value="Transferencia electrónica">Transferencia electrónica</option>';
        $resultado .= '<option value="Depósito en efectivo">Depósito en efectivo</option>';
        $resultado .= '<option value="Cargo a tarjeta de crédito">Cargo a tarjeta de crédito</option>';
        $resultado .= '<option value="Pago en efectivo previo al evento">Pago en efectivo previo al evento</option>';
        $resultado .= '<option value="Pago en efectivo el día del evento" selected>Pago en efectivo el día del evento </option>';
        }
        
        
                    
        $resultado .=  '</select>';
        return $resultado;
      }
      function editar_participante($nombre,$apellidoP,$apellidoM,$handicap,$sexo,$edad,$telefono,$email,$idParticipante){
        $conexion_bd = conectar_bd();
        //Prepara la consulta
        $dml = 'UPDATE participante 
        SET 
        nombre=(?), apellidoPaterno=(?), apellidoMaterno=(?),
        handicap=(?), sexo=(?), edad=(?), telefono=(?), correo=(?)
        WHERE idParticipante=(?)';

        if ( !($statement = $conexion_bd->prepare($dml)) ) {
            die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
            return 0;
        }
          
        //Unir los parámetros de la función con los parámetros de la consulta   
        //El primer argumento de bind_param es el formato de cada parámetro
        if (!$statement->bind_param("sssdssssi",$nombre,$apellidoP,$apellidoM,$handicap,$sexo,$edad,$telefono,$email,$idParticipante)) {
            die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
            return 0;
           
        }
        //Executar la consulta
        if (!$statement->execute()) {
          die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
            return 0;
            
        }
    
        desconectar_bd($conexion_bd);
          return 1;
      }

      function editar_registro($estatusInscripcion,$estatusPago,$asistenciaTorneo, $folio,$nombre,$apellidoP,$apellidoM,$sexo,$edad,$telefono,$email,$handicap,$indicacionEspecial,
      $metodopago,$facturacion,$autorizarFotos,$factRazonSocial,
      $factRFC,$factDireccionFiscal,$factIndicacionEsp,$indicacionEspecialPago,$idParticipante) {
        $conexion_bd = conectar_bd();
        //Prepara la consulta
        if(! editar_participante($nombre,$apellidoP,$apellidoM,$handicap,$sexo,$edad,$telefono,$email,$idParticipante)){
          die("Error al editar participante");
            return 0;
        }
        $dml = 'UPDATE participante_torneo 
        SET 
        indicacionEspecial=(?),metodoPago=(?),
        facturacion=(?),autorizarFotos=(?),RFC=(?),razonSocial=(?),domicilioFiscal=(?),
        comentariosFacturacion=(?),indicacionPagoEspecial=(?),estatusInscripcion=(?),
        estatusPago=(?),asistenciaAlTorneo=(?)
        WHERE folio=(?)';

        if ( !($statement = $conexion_bd->prepare($dml)) ) {
            die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
            return 0;
        }
          
        //Unir los parámetros de la función con los parámetros de la consulta   
        //El primer argumento de bind_param es el formato de cada parámetro
        if (!$statement->bind_param("ssssssssssssi",$indicacionEspecial,$metodopago,$facturacion,$autorizarFotos,$factRFC,$factRazonSocial,$factDireccionFiscal,$factIndicacionEsp,$indicacionEspecialPago,$estatusInscripcion,$estatusPago,$asistenciaTorneo,$folio)) {
            die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
            return 0;
           
        }
        //Executar la consulta
        if (!$statement->execute()) {
          die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
            return 0;
            
        }
    
        desconectar_bd($conexion_bd);
          return 1;
      }


      function editar_registro_Usuario($folio,$nombre,$apellidoP,$apellidoM,$sexo,$edad,$telefono,$email,$handicap,$indicacionEspecial,
  $metodopago,$facturacion,$autorizarFotos,$datosTarjetaCredito,$factRazonSocial,
  $factRFC,$factDireccionFiscal,$factIndicacionEsp,
  $urlRecibo,$indicacionEspecialPago){

    $conexion_bd = conectar_bd();
    $dml = 'UPDATE participante_torneo SET nombre=(?), apellidoPaterno=(?), apellidoMaterno=(?),sexo=(?), edad=(?), telefono=(?),correo=(?), handicap=(?), indicacionEspecial=(?), metodopago=(?),facturacion=(?),autorizarFotos=(?), datosTarjetaCredito=(?), razonSocial=(?), RFC = (?),domicilioFiscal=(?), comentariosFacturacion=(?), idFotoRecibo=(?), indicacionPagoEspecial=(?), WHERE folio=(?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }

      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro

    if (!$statement->bind_param("ssssdssisssssssssssi",$nombre,$apellidoP,$apellidoM,$sexo,$edad,$telefono,$email,$handicap,$indicacionEspecial,$metodopago,$facturacion,$autorizarFotos,$datosTarjetaCredito,$factRazonSocial,$factRFC,$factDireccionFiscal,$factIndicacionEsp,$urlRecibo,$indicacionEspecialPago,$folio)){
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    desconectar_bd($conexion_bd);
      return 1;
  }
?>