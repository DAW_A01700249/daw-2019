<?php
     
   function conectar_bd() {
      $conexion_bd = mysqli_connect("localhost","root","","torneogolf");
      if ($conexion_bd == NULL) {
          die("No se pudo conectar con la base de datos");
      }
      return $conexion_bd;
  }

  //función para desconectarse de una bd
  //@param $conexion: Conexión de la bd que se va a cerrar
  function desconectar_bd($conexion_bd) {
      mysqli_close($conexion_bd);
  }


//Función que conecta a la bd, realiza un query y vuelve a cerrar la bd. Recibe el SQL del query y regresa un objeto mysqli result
function sqlqry($qry) {
    $con = conectar_bd();
    if(!$con){
        return false;
    }

    $result = mysqli_query($con, $qry);
    desconectar_bd($con);
    return $result;
}

/*
    Función para simplificar la inserción correcta a la bd. Recibe el código SQL donde los valores q insertar se representan con '?'
    E.g. INSERT INTO frutas (nombre, familia, precio) VALUES (?,?,?)
    Los valores se pasan como argumentos, deben ser correspondientes al numero de '?'. Se puede usar un arreglo como parámetro precedido de '...'
    E.g. insertIntoDb($sql, $nom, $fam, $precio)   insertIntDb($sql, ...$arrayWithValues)
    Regresa en indice del elemento insertado
*/


function agregarPremio($tipoPremio, $descripcion, $hoyo, $posicion, $torneo) {
    $conexion_bd = conectar_bd();

    $id=idPrem();
    $id=$id+1;

    $dml = 'INSERT INTO premio (tipo,descripcion,posicion,hoyo,idTorneo,idPremio) VALUES (?,?,?,?,?,?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
    if (!$statement->bind_param("sssiii", $tipoPremio,$descripcion,$posicion,$hoyo,$torneo,$id)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    desconectar_bd($conexion_bd);
      return 1;
}



function idPrem(){
    $conexion_bd = conectar_bd();  
      
    $consulta = "SELECT idPremio FROM premio ORDER BY idPremio DESC LIMIT 1";
    $resultados = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        desconectar_bd($conexion_bd);
        return $row["idPremio"];
    }
        
    desconectar_bd($conexion_bd);
    return 0;
  }


function mostrarPremios($torneo="",$premio=""){
  $conexion_bd = conectar_bd();
  $resultado = '    <div class="col s7">
<table class="highlight"><thead>
  <tr>
  <th>Tipo de premio</th>
  <th>Decripción</th>
  <th>Posición</th>
  <th>Hoyo</th>
  <th>Torneo</th>
  <th>Editar</th>
  <th>Eliminar</th>

  </tr>
  </thead>';

  $consulta = 'SELECT * FROM torneo T, premio P WHERE P.idTorneo = T.idTorneo ';
  if ($torneo != "") {
        $consulta .= ' AND T.idTorneo='.$torneo;
    }
    if ($premio != "") {
        $consulta .= ' AND tipo="'.$premio.'" ';
  }
  $resultados = $conexion_bd->query($consulta);
  while($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)){
    $resultado .= "<tr>";
    $resultado .= "<td>".$row['tipo']."</td>";
    $resultado .= "<td>".$row['descripcion']."</td>";
    $resultado .= "<td>".$row['posicion']."</td> ";
    $resultado .= "<td>".$row['hoyo']."</td>";
    $resultado .= "<td>".$row['nombre']."</td>";
        $resultado .= '<td><a href="editarPremio.php?caso_id='.$row['idPremio'].' "class="btn " id="botonesA"><i class="material-icons">create</i></a></td>'; //Se puede usar el índice de la consulta
        $resultado .= '<td><a href="eliminar_premio.php?caso_id='.$row['idPremio'].'" class="btn " id="botones"><i class="large material-icons">clear</i></a></td>';
        $resultado .= "</tr>";
      }

    mysqli_free_result($resultados); //Liberar la memoria

    desconectar_bd($conexion_bd); 

    $resultado .= "</tbody></table></div>";
  
    return $resultado;
  }

function recuperar_info($idPremio) {
        $conexion_bd = conectar_bd();  
        
        $consulta = 'SELECT * FROM premio P, torneo T WHERE P.idTorneo = T.idTorneo AND idPremio="'.$idPremio.'"';
        $resultados = $conexion_bd->query($consulta);
        while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
            $array =
            [
                $row["idPremio"],
                $row["tipo"],
                $row["descripcion"],
                $row["posicion"],
                $row["hoyo"],
                $row["nombre"],
                $row["idTorneo"]
            ];
            desconectar_bd($conexion_bd);
            return $array;
        }
            
        desconectar_bd($conexion_bd);
        return 0;
}


function crear_select($id, $columna_descripcion, $tabla, $seleccion=0) {
    $conexion_bd = conectar_bd();  
      
    $resultado = '<select name="'.$tabla.'" id="'.$tabla.'"><option value="" disabled selected>Selecciona una opción</option><option value="">Ver todos</option>';
            
    $consulta = "SELECT $id, $columna_descripcion FROM $tabla";
    $resultados = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= '<option value="'.$row["$id"].'" ';
        if($seleccion == $row["$id"]) {
            $resultado .= 'selected';
        }
        $resultado .= '>'.$row["$columna_descripcion"].'</option>';
    }
        
    desconectar_bd($conexion_bd);
    $resultado .=  '</select>';
    return $resultado;
  }

function getOpciones($id, $campo, $tabla,$seleccion=0) {
    $conexion_bd = conectar_bd();  
      
    $resultado = '<div class="input-field"><select name="'.$tabla.'" id="'.$tabla.'"><option value="" disabled selected>Selecciona una opción</option>';
            
    $consulta = "SELECT $id, $campo FROM $tabla";
    $resultados = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= '<option value="'.$row["$id"].'" ';
        if($seleccion == $row["$id"]) {
            $resultado .= 'selected';
        }
        $resultado .= '>'.$row["$campo"].'</option>';
    }
        
    desconectar_bd($conexion_bd);
    $resultado .=  '</select><label>'.ucfirst($tabla).'</label></div>';
    return $resultado;
}

function getOpcionesPremio($id, $campo, $tabla) {
    $sql = "SELECT $id, $campo FROM $tabla GROUP BY $campo";
    $result = sqlqry($sql);
    $option = "";

    while($row = mysqli_fetch_array($result)){
        $option = $option."<option value=".$row[0].">".ucfirst($row[1])."</option>";
    }

    return $option;
}


function editarPremio($idPremio, $tipo, $descripcion, $posicion, $hoyo, $idTorneo){
        $hoyoVal = $hoyo;
        if($hoyo == 0 || $hoyo = ""){
          $hoyoVal = null;
        }
        $conexion_bd = conectar_bd();
        $dml = 'UPDATE premio
        SET 
        tipo=(?),descripcion=(?),posicion=(?),hoyo=(?),idTorneo=(?)
        WHERE idPremio=(?)';

        if ( !($statement = $conexion_bd->prepare($dml)) ) {
            die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
            return 0;
        }
          
        //Unir los parámetros de la función con los parámetros de la consulta   
        //El primer argumento de bind_param es el formato de cada parámetro
        if (!$statement->bind_param("sssiii", $tipo, $descripcion, $posicion, $hoyoVal, $idTorneo,$idPremio)) {
            die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
            return 0;
           
        }
        //Executar la consulta
        if (!$statement->execute()) {
          die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
            return 0;
            
        }
    
        desconectar_bd($conexion_bd);
          return 1;
      }
function eliminarPremio($idPremio){
   $conexion_bd = conectar_bd();  
        
        $consulta = 'DELETE FROM premio WHERE idPremio="'.$idPremio.'"';
        $resultado=$conexion_bd->query($consulta);
    desconectar_bd($conexion_bd);  
    return $resultado;
}

?>