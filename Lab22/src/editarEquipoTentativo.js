let jugadoresEditarEquipoTentativo = [];
let infojugadoresEditarEquipoTentativo = [];
let jugadoresEditarEquipoActualTentativo = [];
let jugadores_EliminarTentativo=[];

function recuperarInfoTentativo($id_ParticipantesTentativo, $nombres_ParticipantesTentativo) {
    let i = 0;
    while (i < $id_ParticipantesTentativo.length) {
        numero = parseInt($id_ParticipantesTentativo[i], 10)
        jugadoresEditarEquipoActualTentativo.push(numero);
        añadirJugadorEditarTentativo(numero, $nombres_ParticipantesTentativo[i]);
        i = i + 1;
    }
}
function validarEquipoEditarTentativo() {
    let textoFaltante = "";
    let flag = 1;
    if (jugadoresEditarEquipoTentativo.length == jugadoresEditarEquipoActualTentativo.length && jugadoresEditarEquipoTentativo.every(function (v, i) { return v === jugadoresEditarEquipoActualTentativo[i] })) {
        document.getElementById('tituloEditarEquipoTentativo').innerHTML = "No se realizo ningun cambio";
        document.getElementById('contenidoConfirmacionEditarTentativo').innerHTML = "";
        document.getElementById("editarEquipoTentativo").style.display = "none";
    } else {
        
        if (jugadoresEditarEquipoTentativo.length == 0) {
            textoFaltante += "<li>Introducir participantes al equipo</li>";
            flag = 0;
        }
        if (flag == 0) {
            let mensaje = document.getElementById('tituloEditarTentativo');
            let botonConfirmar = document.getElementById('editarEquipoTentativo');
            mensaje.innerHTML = "Por favor corregir los siguientes campos:";
            document.getElementById('contenidoConfirmacionEditarTentativo').innerHTML = textoFaltante;
            mensaje.style.display = "block";
            botonConfirmar.style.display = "none";

        } else {
            let botonConfirmar = document.getElementById('editarEquipoTentativo');
            let mensaje = document.getElementById('tituloEditarEquipoTentativo');
            mensaje.innerHTML = "¿Confirmar inscripción?";
            document.getElementById('contenidoConfirmacionEditarTentativo').innerHTML = "Presione confirmar para realizar la inscripcion";
            botonConfirmar.style.display = "block";
        }
    }
}
function actualizarEquipoEditarTentativo() {
    let $result = "";
    let x = 0;
    while (x != jugadoresEditarEquipoTentativo.length) {
        $result += infojugadoresEditarEquipoTentativo[x];
        x = x + 1;
    }
    document.getElementById("integrantesEquipoTextoEditarTentativo").innerHTML = $result;
}
function actualizarSeleccionadosEditarTentativo() {
    for (let x in jugadoresEditarEquipoTentativo) {
       let idBoton = "boton";
       idBoton += jugadoresEditarEquipoTentativo[x];
       let idTabla = "participante";
       idTabla += jugadoresEditarEquipoTentativo[x];
       let rowParticipante= document.getElementById(idTabla);
       let botonAgregar = document.getElementById(idBoton);
       if (botonAgregar != null) {
          botonAgregar.style.display = "none";
          rowParticipante.style.backgroundColor = "rgb(240, 238, 238)"
       }
    }
 }
function añadirJugadorEditarTentativo(idP, nombre) {
    if (jugadoresEditarEquipoTentativo.indexOf(idP) == -1) {
        let $info = '<li class="collection-item" id="campoJugadorEquipo">';
        $info += idP;
        $info += '-';
        $info += nombre;
        $info += '<a id="campoJugadorEquipoBoton" class="btn btn-medium waves-effect waves-light pink darken-1 right align"><i onclick="eliminarJugadorEditarTentativo(';
        $info += idP;
        $info += ')' + '"';
        $info += ' class="material-icons">remove_circle</i></a>';
        $info += "</li>";
        infojugadoresEditarEquipoTentativo.push($info);
        jugadoresEditarEquipoTentativo.push(idP);
        let idTabla = "participante";
        idTabla += idP;
        let idBoton = "boton";
        idBoton += idP;
        document.getElementById(idBoton).style.display = "none";
        document.getElementById(idTabla).style.backgroundColor = "rgb(240, 238, 238)";
        actualizarEquipoEditarTentativo();

    }
}
function eliminarJugadorEditarTentativo(idP) {
    if (jugadoresEditarEquipoTentativo.length == 1 || jugadoresEditarEquipoTentativo.indexOf(idP) == 0) {
        jugadoresEditarEquipoTentativo.shift();
        infojugadoresEditarEquipoTentativo.shift();
    } else {
        let index1 = jugadoresEditarEquipoTentativo.indexOf(idP);
        infojugadoresEditarEquipoTentativo.splice(index1, 1);
        jugadoresEditarEquipoTentativo.splice(index1, 1);
    }
    let idTabla = "participante";
    idTabla += idP;
    let idBoton = "boton";
    idBoton += idP;
    document.getElementById(idBoton).style.display = "block";
    document.getElementById(idTabla).style.backgroundColor = "white";
    actualizarEquipoEditarTentativo();
}
function editarEquipoTentativo( idEquipoEditar) {
    let circulo = document.getElementById("circuloCarga_EditarEquipoTentativo");
    let forma = document.getElementById('formaEquipoEditarTentativo');
    //Comparar los numeros de participantes que se agregaron
    let numJugadores= jugadoresEditarEquipoTentativo.length;
    conseguirParticipantesTentativo();
    forma.style.display = "none";
    circulo.style.display = "block";
    $.post("../Controladores/controlador_editar_equipoTentativo.php", {
        arrayJugadoresAgregarT: JSON.stringify(jugadoresEditarEquipoTentativo),
        arrayJugadoresEliminarT: JSON.stringify(jugadores_EliminarTentativo),
        tamañoEquipoT: numJugadores,
        idEquipoEditar
     }).done(function (data) {
        if (data == 1) {
            circulo.style.display = "none";
            document.getElementById("wrap-editarEquipoTentativo").style.display="none";
            document.getElementById("mensajeEditarEquipoTentativo").style.display="block";
            document.getElementById("tituloMensajeEditarEquipoTentativo").innerHTML="Se edito el equipo satisfactoriamente";
            document.getElementById("textoMensajeEditarEquipoTentativo").innerHTML="El cambio se a guardado en la base de datos";
        } else {
            circulo.style.display = "none";
            document.getElementById("wrap-editarEquipoTentativo").style.display="none";
            document.getElementById("mensajeEditarEquipoTentativo").style.display="block";
            document.getElementById("tituloMensajeEditarEquipoTentativo").innerHTML="No se pudo editar el equipo";
            document.getElementById("textoMensajeEditarEquipoTentativo").innerHTML="Revisa tu conexion a internet o intentalo más tarde";
        }
     });
}
function conseguirParticipantesTentativo() {
    let i = 0;
    let j=jugadoresEditarEquipoActualTentativo.length;
    while (i < j) {
        if (jugadoresEditarEquipoTentativo.includes(jugadoresEditarEquipoActualTentativo[i])==true) {
            if (jugadoresEditarEquipoTentativo.length == 1 || jugadoresEditarEquipoTentativo.indexOf(jugadoresEditarEquipoActualTentativo[i]) == 0) {
                jugadoresEditarEquipoTentativo.shift();
            } else {
            let index1 = jugadoresEditarEquipoTentativo.indexOf(jugadoresEditarEquipoActualTentativo[i]);
            jugadoresEditarEquipoTentativo.splice(index1, 1);            }
        }else{
            jugadores_EliminarTentativo.push(jugadoresEditarEquipoActualTentativo[i]);
        }
        i = i + 1;
    }
    
}