<?php 
  session_start();
  if(isset($_SESSION['usuario'])){
      if($_SESSION['usuario']['nombreRol']!="participante"){
        header('Location: InterfazOrg.php');
      }
    }else{
        header('Location: InterfazLogin.php');
    }
  require("../Models/model_multimedia.php");

    //require_once("../Models/modelEditarRegistro.php"); 
    
  include("../Vistas/header.html");
    include("../Vistas/mensajeRecibo.html");

    $username= $_SESSION['usuario']['nombreUsuario'];

    //$caso_id = consultar_folio($username);
    $caso_id = htmlspecialchars($_GET['caso_id']);
    //echo "$caso_id";
    $folio = $caso_id;
    //$idParticipante= htmlspecialchars($_POST["idPart"]);

    $nombre_imagen=$_FILES['imagen']['name'];
    $tipo_imagen=$_FILES['imagen']['type'];
    $size_imagen=$_FILES['imagen']['size'];

    if($size_imagen<=5000000){
        if($tipo_imagen=="image/jpeg"||$tipo_imagen=="image/jpg"||$tipo_imagen=="image/png"||$tipo_imagen=="image/gif"||$tipo_imagen=="application/pdf"){
            //$carpeta=$_SERVER['DOCUMENT_ROOT'].'/Controladores/uploads/';
            $carpeta = "../src/uploads/$username/";
            if (!file_exists($carpeta)) {
                mkdir($carpeta, 0777, true);
            }
            

            move_uploaded_file($_FILES['imagen']['tmp_name'], $carpeta.$nombre_imagen);
            echo "<h1 class=center>Se subió el archivo</h1>";
            echo "<a class=btn href=../Vistas/informacion-usuario.php>Salir</a>";
            echo " ";
            echo "<a class=btn href=../Vistas/editarRegistroUsuario.php?caso_id=folio>Ver información</a>";
            
             subir_recibo($nombre_imagen,$folio);
             
        }else{

            echo "<h1>Solo se aceptan archivos jpg, jpeg, png, gif o pdf</h1>";
            echo "<a class=btn href=../Vistas/informacion-usuario.php>Salir</a>";
            echo " ";
             echo "<a class=btn href=../Vistas/subirRecibo.php?caso_id=folio>Regresar</a>";
            
        }
    }else{
        echo "<h1>Tamaño muy grande</h1>";
        echo "<a class=btn href=../Vistas/informacion-usuario.php>Salir</a>";
        echo " ";
        echo "<a class=btn href=../Vistas/subirRecibo.php?caso_id=folio>Regresar</a>";
    }
    echo "</div><br>";
    include("../Vistas/footer.html");
    
?> 