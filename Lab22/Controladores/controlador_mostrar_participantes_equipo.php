<?php
  session_start();
  require_once("../Models/modelConsulta.php");
  $idEquipo = htmlspecialchars($_POST["idEquipo"]);
  $array= consultar_ParticipantesEquipo($idEquipo);
  json_encode($array);
  $separado_por_comas = implode(",", $array);
  echo $separado_por_comas;
?>