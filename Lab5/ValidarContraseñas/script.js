function validar_contraseña()
{

    let contraseña1=document.f1.contraseña_1.value;
    let contraseña2=document.f1.contraseña_2.value;
    let espacios = false;
    let mayus=false;
    let minuscula = false;
	let numero = false;
    let cont = 0;
    if(contraseña1.length==0||contraseña2.length==0){
        alert("Favor introducir la contraseña en ambos campos")
    }else{
        for(let i=0;i<contraseña1.length;i++) {
            if (contraseña1.charAt(cont) == " "){
                espacios = true;
                break;
            }
            if (contraseña1.charCodeAt(i)>= 65 && contraseña1.charCodeAt(i) <= 90){
                mayus = true;
            }
            if (contraseña1.charCodeAt(i)>= 97 && contraseña1.charCodeAt(i) <= 122){
                minuscula = true;
            }
            if (contraseña1.charCodeAt(i)>= 48 && contraseña1.charCodeAt(i) <= 57){
                numero = true;
            }
        }
        if (espacios) {
            alert ("La contraseña no puede contener espacios en blanco");
            }else if(!mayus){
                alert ("La contraseña debe tener almenos una mayuscula");
                } else if(!minuscula){
                    alert ("La contraseña debe tener almenos una minuscula");
                    } else if(!numero){
                        alert ("La contraseña debe tener almenos un numero");
                        } else if(contraseña1==contraseña2){
                            setTimeout(function(){ alert("Contraseña validada"); }, 3000);
                            }else{
                                alert("Las contraseñas no son las mismas.")
                            }
        }
}
function on_logo(){
    document.getElementById("_logo").style.opacity="1";
  }
  function off_logo(){
    document.getElementById("_logo").style.opacity="0.5";
  }