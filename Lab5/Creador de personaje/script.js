//Logica de procedimientos
var personaje= new Object();
var atributo_puntaje=[];
function validar_personaje()
{
    personaje.nombre= document.f1.nombrePersonaje.value;
    personaje.genero=document.f1.genero.value;
    personaje.clase=document.f1.clase.value;
    personaje.regalo=document.f1.regalo.value;
    let respuesta_nombre= document.createElement("h2");
    let respuesta_regalo=document.createElement("p");
    let respuesta_genero=document.createElement("p");
    let espacios = false;
    let cont = 0;
    if(personaje.nombre.length==0||personaje.genero.length==0||personaje.clase.length==0||personaje.regalo.length==0){
        alert("Favor de llenar todos los campos para la creación del personaje");
    }else{
        while (!espacios && (cont < personaje.nombre.length)) {
            if (personaje.nombre.charAt(cont) == " ")
            espacios = true;
            cont++;
        }
        if (espacios) {
            alert ("El nombre del personaje no puede tener espacion en blanco");
        }
    }
    respuesta_nombre.appendChild(document.createTextNode(personaje.nombre+" : "+personaje.clase));
    respuesta_regalo.appendChild(document.createTextNode("Regalo: "+personaje.regalo));
    respuesta_genero.appendChild(document.createTextNode("Genero: "+personaje.genero));
    document.getElementById("nombre").appendChild(respuesta_nombre);
    document.getElementById("genero").appendChild(respuesta_genero);
    document.getElementById("regalo").appendChild(respuesta_regalo);
    generar_atributos();
    genera_tabla();
}
function genera_tabla() {
    let atributo=[
        "Vida","Resistencia a frustración","Velocidad codeo","Trabajo en equipo","Conocimiento Git","Conocimiento JS","Conocimiento CSS","Aprendizaje","Creatividad","Organización"
    ];
    let div_tabla = document.getElementById("tabla");
    let tabla   = document.createElement("table");
    let tblBody = document.createElement("tbody");
   
    for (let i = 0; i < atributo.length; i++) {
        let hilera = document.createElement("tr");
      for (let j = 0; j < 2; j++) {
        let celda = document.createElement("td");
        let textoCelda;
        if(j%2==0)
        {
            textoCelda = document.createTextNode(atributo[i]);
        }
        else
        {
            textoCelda = document.createTextNode(atributo_puntaje[i]);    
        }
        celda.appendChild(textoCelda);
        hilera.appendChild(celda);
      }
      tblBody.appendChild(hilera);
    }
    tabla.appendChild(tblBody);
    div_tabla.appendChild(tabla);
    tabla.setAttribute("border", "2");
  }
  function generar_atributos()
  {
    if(personaje.clase=="Back-end Developer"){
        atributo_puntaje=[
            10,12,15,9,10,15,7,13,9,12
        ]
    }
    if(personaje.clase=="Front-end Developer"){
        atributo_puntaje=[
            10,12,7,11,7,12,15,10,15,10
        ]
    }
    if(personaje.clase=="Full-stack Developer"){
        atributo_puntaje=[
            15,13,12,4,13,10,10,15,8,10
        ]
    }
    if(personaje.clase=="Project Manager"){
        atributo_puntaje=[
            13,10,8,15,14,9,9,6,6,15
        ]
    }
    if(personaje.clase=="Arquitecto de SFW"){
        atributo_puntaje=[
            15,15,5,13,10,10,10,13,8,13
        ]
    }
    if(personaje.clase=="Aguero"){
        atributo_puntaje=[
            8,4,2,2,2,3,1,1,1,2
        ]
    }
  }
  function alerta_nombre(){
    document.getElementById("aviso_nombre").style.visibility = "visible";
    document.getElementById("aviso_nombre").style.fontStyle = "italic";
  }
  //funciones del logo
  function on_logo(){
    document.getElementById("_logo").style.opacity="1";
  }
  function off_logo(){
    document.getElementById("_logo").style.opacity="0.5";
  }
  //Funciones para las facciones
  function allowDrop(ev) {
    ev.preventDefault();
  }
  
  function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
  }
  
  function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
  }