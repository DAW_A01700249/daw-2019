-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: mysql1008.mochahost.com    Database: dawbdorg_torneogolf
-- ------------------------------------------------------
-- Server version	5.6.45

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cuenta`
--

DROP TABLE IF EXISTS `cuenta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cuenta` (
  `nombreUsuario` varchar(20) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `correoElectronico` varchar(50) NOT NULL,
  `IdRol` int(5) NOT NULL,
  PRIMARY KEY (`nombreUsuario`),
  KEY `IdRol` (`IdRol`),
  CONSTRAINT `cuenta_fk_1` FOREIGN KEY (`IdRol`) REFERENCES `rol` (`IdRol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuenta`
--

LOCK TABLES `cuenta` WRITE;
/*!40000 ALTER TABLE `cuenta` DISABLE KEYS */;
INSERT INTO `cuenta` VALUES (' NuevoUser','$2y$10$XeEgxeMGwRS3qiB5soC1UudBOkz196o5npIp6C1PMADUCuHgWBdHO','micorreo@gmail.com',1),('A01351355DAW','$2y$10$p56HdUyoS2P8mj/nAiGqqe3/g8GVQEd5IbLY8r8FCEeYxlSnQEVEy','A01351355@itesm.mx',1),('asadsa','$2y$10$vj5KWPr/Su.Bg/Rq0VNXrOzTiDWPmJ/AVB7AnCkiY29FNRjyW01TG','daniel.alex17hotmail.com',1),('ccinatacion','$2y$10$UWRhsFuahgHTHCXhK0sKweXRHfO/F10og08VLTKcJnPCax/AL.P8K','A01351355@itesm.mx',1),('elbueno','$2y$10$LXTug.FhmGE9yx8ml1XnB.7gMP5L5YlZPCVsC06cG1yjrm4cGGVuq','emsaguilera@gmail.com',1),('elemilio','$2y$10$3yu48e6NvgGP8MtoHw1LruRh9XHcSqsQHTMQkhFc4G1N2Y.5r0kku','A01351355@itesm.mx',1),('ElHulkNuevo','$2y$10$XzhId84xCbhK1OeoXF6wOe6.KjWGyoM73ZVrNBTYxqyIIaFgZUtL2','eftr249@outlook.com',1),('ElMazorcas','$2y$10$vA1LHqxNj3AQnPSoDjaxNukB8BM7G.tKW0mXtM4lu6x/geXnfm6ze','eftr249@outlook.com',1),('emi123','$2y$10$gBLORO1Ee/C9guxhpltsIuS5XmKxxHia.w2Tu4ZHh2kayNjKx2e1W','A01351355@itesm.mx',1),('emilo','$2y$10$hft9QjbGKU1L1ElrvgxPjO5ihPPBEqjTGWRmZWPG6M9NkxiBdOsBm','qwer@wdud',1),('emms','$2y$10$ZzXbgSKJ15KGZHJvo376TuQLmehtYIn5jf9Z9bfT5TcefVAqdaEZq','defr@defr',2),('ems123','emilioelchido','',2),('EMSAGC','$2y$10$aE7DEJbCvNYXEu4dbmA0aO5rnF/FZCwiCj4enVLKg8/Au9kMml97e','A01351355@itesm.mx',1),('emsagui','$2y$10$H7YPlDT5/2E7NgwVphQ0Xepll2sV0Ojp4Cx7G8tGhAlvXgm6SVX1G','emsaguilera99@gmail.com',1),('emsaguil','$2y$10$zdfG/SAbg4xPi87gxfHcmOaNZDEeYsOQuQBdCoPwqcWhs3qIY7/tW','emsaguilera99@gmail.com',1),('emsaguilera','$2y$10$./A7IF97o41M3n380PVvaOroifKooLgU3/IUq3qjqK/clcjx2zlF2','ems@mail.com',1),('emss','$2y$10$SmaydQMm0PnAFRxOeUuDvuDBllerALhbWUxqCeVveqDMu3Q27jCra','1234@12345',1),('erct17','erictorres27','erictores_45@hotmail.com',2),('eric0249','$2y$10$4hkTT/tJ0i0beQ38.ZbfR.vZDXtT7aSo1eSxU1nwE197RBWP.tYiW','erictores_45@hotmail.com',1),('ericOrg','$2y$10$CM4ipJVfhUIVDXEwCcxbN.dfujefJK3LbVzqWTKrm/xAERBkF2Y4e','elcorreodeeric@hotmail.com',2),('ericParticipante','$2y$10$Mnn4zkRZmm0FkxbmTNrZ3.0cZWhbpqvrxvMMlQzYCUglIU6KwxQZ.','erictores_45@hotmail.com',1),('erict24','erictorres27','daniel.alex17@hotmail.com',2),('erict27','erictorres27','',2),('ericTorres27','$2y$10$z4OM55einMe79wibk23RW.hdZ3Hm6JVyYLfW1kCGXp6l7W8pqai6q','erictorres_45@hotmail.com',1),('ericTR','$2y$10$nkUw2F0G/rMGIoamTEDqP.gdkaTSf2ahOmSiHamHhtqadXkBGDLLy','rftr249@outlook.com',1),('EsteUserEsMuyNuevo','$2y$10$hVgPYkofI.YmkEmkiet6meFXZWeFJmljqoHwY11eFJ5dBlIAJYCVm','peraltaalejandro22@gmail.com',2),('hulk123','elhulk','elmasVerde@hotmail.com',2),('hulkCHIKITO','CHIKITO','',2),('IvanDiaz','12356','',2),('IvanOrg','$2y$10$EC3P/dbcdgdOENeRutp/hOy7/B3VHdLLq.8JqHe8EmU3Yop7VViU2','erictores_45@hotmail.com',2),('IVAN_DAYZ','123456','',2),('IVAN_DAYZ2','$2y$10$p4SY8r8RZZ4Cfw2oFg4.uuvfYNX1rjjtUUmOxa2UkCNCG/5D5d9ym','A01700486@itesm.com',2),('jctj','$2y$10$W6vC4ZPDWnWRgIY7uNfTheOs.l3Wd3jL3czyNrJN3YND0MEGBsDjq','1234@12345',1),('JimJim','Jim123','',2),('JimTorres','$2y$10$IDEaS1uUQqhA6z4cuKEsI.JBa19N3czmzrSorC9FMwTfSka2Vs3Lq','nahim.torres@hotmail.com',1),('jules','$2y$10$32NTdHMKRyLNDX/OMzCuLejvGUJGtYf4g4qhRFqhZQWoGarNWrYUS','julio@julio.com',1),('lamole1','$2y$10$P8pUcPZg.VERhRv4ZCaPBugFx2H5NcfeFq9vcuUJLrrPq2sFkoF5u','1234@12345',1),('lolol','$2y$10$i5Ar2clR3T4EoDUaBhKklOE9EBBfX7qSm4GPP9DqZZLvYmpH6wxtO','A01351355@itesm.mx',1),('Lulu','$2y$10$5pqzdJddlMjUXgIz.OIUguWAUSPLV9o3R1wKEo9ttEIDscVcEO1Zi','lulu@gmail.com',2),('nahimmedto','$2y$10$SMbCjpyAYlYl7qKFmniSueoIXM50zAElaufO2r3ni1uLIzByfccKm','nahim.torres@hotmail.com',1),('Najim1','$2y$10$vD4eXWzXvcGUktpHbCj2eu2OoGqYC4r/4fU4RJzYVMpew2bQqyv1.','nahim@yahoo.com',2),('NuevoOg','15975468','peraltaalejandro22@gmail.com',2),('NuevoUser','$2y$10$hMXyMxiipV85OZsfIs2QTuYzAnSQfi5pwwEOOX1rBZTN4xh8GaBNG','micorreo@gmail.com',1),('organizador1','contra1','',2),('organizador5','contraseña5','',2),('OsiOsi','$2y$10$/dxHIUdz69FmZzk6iHjLSurv6GQINRyycT/Pfq5ca6BGsrLbIjqC2','eftr249@outlook.com',2),('prueba1','contra1','prueba@gmail.com',1),('prueba12','erictorres27','daniel.alex17@hotmail.com',2),('prueba2','contraseña1','prueba@gmail.com',1),('prueba3','contraseña1','prueba@gmail.com',1),('prueba4','contraseña1','prueba@gmail.com',1),('prueba5','contraseña1','prueba@gmail.com',1),('pruebachida1','prueba1','erictores_45@hotmail.com',2),('pruebafuncional','prueba1','daniel.alex17@hotmail.com',2),('pruebafuncional1','prueba1','daniel.alex17@hotmail.com',2),('pruebaPresent','$2y$10$aNTcpSFrrDTIW9OTARA4IufvmNBC.iu4g0G4mnZGJgYD5gPq0bQsu','A01700190@itesm.mx',1),('PruebaUltimaaaa','$2y$10$OBlIAAnEhCrTYVOAYDXMiOaaYjgA7bCo7nxLytoqi8kOim4QSTYeC','peraltaalejandro22@gmail.com',2),('PurebaU','$2y$10$UdE9agxeWwvXwi1E53jcnOaRVaj1lwop6pv/6tzy4Ui105Q93QajW','eftr249@outlook.com',2),('thor1','$2y$10$f2U3J54sIjB5DtVAOJsdnOfFB3HoxuXS5t/ltXrsfer1dn9tYeXo.','thor@avenger.com',1),('us123','us123','',2),('user123','user123','',2),('usuario1','contraseña1','',1),('usuario10','contraseña10','',1),('usuario11','contraseña11','',1),('usuario12','contraseña12','',1),('usuario13','contraseña13','',1),('usuario14','contraseña14','',1),('usuario15','contraseña15','',1),('usuario2','contraseña2','',1),('usuario3','contraseña3','',1),('usuario4','contraseña4','',1),('usuario5','contraseña5','',1),('usuario6','contraseña6','',1),('usuario7','contraseña7','',1),('usuario8','contraseña8','',1),('usuario9','contraseña9','',1),('usuarioPrueba','contraseña1','prueba@gmail.com',1),('uuuuuu','$2y$10$RTzv7/UxcoLtDj.vgRKhluCTKR7kQzDULMkSvgAO.JyVbUxl.2ywm','A01700486@itesm.mx',2),('uuuuuuuuuuu','$2y$10$cWiqrOJSFN.Tyf/ezA8acuQjTqz22C/EQUuL7AaNem0H0XwYfZ9oK','A01700486@itesm.mx',2),('victorprueba','$2y$10$0nArTNJztterlPpifNj5peA.INkQYmjZ/QbIVZxUBN70rBBxvgwK.','victor.rey@gmail.com',1),('Yaelultimo','$2y$10$BAuCyuOYBHmGJsWEVEAv.OCjBtuw1doVSQSxC3feOyVwKyfOYMOeG','peraltaalejandro22@gmail.com',2),('yoshi','$2y$10$qRFltqE8Asw5NzFfGfE5bOZjUNM9PjeUk1z4Tb12viFmZqIE.ZEMC','emsaguilera99@gmail.com',1);
/*!40000 ALTER TABLE `cuenta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipo`
--

DROP TABLE IF EXISTS `equipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `equipo` (
  `idEquipo` int(5) NOT NULL AUTO_INCREMENT,
  `numParticipantes` int(5) NOT NULL,
  `hoyoInicial` int(10) NOT NULL,
  `estatus` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idEquipo`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipo`
--

LOCK TABLES `equipo` WRITE;
/*!40000 ALTER TABLE `equipo` DISABLE KEYS */;
INSERT INTO `equipo` VALUES (49,1,1,'Oficial'),(50,4,12,'Oficial'),(51,3,2,'Oficial'),(52,2,4,'Oficial'),(56,3,0,'Tentativo');
/*!40000 ALTER TABLE `equipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organizador`
--

DROP TABLE IF EXISTS `organizador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `organizador` (
  `nombreUsuario` varchar(20) NOT NULL,
  `idOrganizador` int(5) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `apellidoPaterno` varchar(20) NOT NULL,
  `apellidoMaterno` varchar(20) NOT NULL,
  `desactivado` int(1) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`nombreUsuario`),
  KEY `nombreUsuario` (`nombreUsuario`),
  CONSTRAINT `organizador_fk_1` FOREIGN KEY (`nombreUsuario`) REFERENCES `cuenta` (`nombreUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organizador`
--

LOCK TABLES `organizador` WRITE;
/*!40000 ALTER TABLE `organizador` DISABLE KEYS */;
INSERT INTO `organizador` VALUES ('emms',25,'dsvfbdgnf','bfdxgnf','dfbxg',0,'4424424424'),('ems123',5,'','','',0,'2147483647'),('erct17',17,'sdfds','sdfsdf','sdfsdf',0,'2147483647'),('ericOrg',23,'Eric','Torres','Rodríguez',0,'2147483647'),('erict24',18,'Daniel Alejandro','dasda','asdda',0,'2147483647'),('hulk123',12,'Bruce','Banner','QuienSabexd',0,'2147483649'),('hulkCHIKITO',13,'','','',1,'2147483647'),('IvanDiaz',10,'','','',0,'2147483647'),('IvanOrg',27,'Ivan','Díaz','Peralta',0,'2147483647'),('IVAN_DAYZ',7,'','','',0,'2147483647'),('IVAN_DAYZ2',26,'Juan','García','Perez',0,'2147483647'),('JimJim',6,'','','',0,'2147483647'),('Lulu',28,'Lourdes','A','A',0,'2147483647'),('NuevoOg',22,'Juan','García','Perez',0,'2147483647'),('organizador1',1,'','','',0,'2147483647'),('OsiOsi',32,'Juan','García','Perez',0,'4425877319'),('prueba12',20,'prueba funcional 3','as','asd',0,'2147483647'),('pruebafuncional',21,'prueba funcional 5','a','a',0,'2147483647'),('pruebafuncional1',19,'Prueba funcionalidad','preueba','prueba',0,'2147483647'),('PruebaUltimaaaa',29,'Juan','García','Perez',0,'4425877319'),('us123',9,'','','',0,'2147483647'),('user123',11,'','','',0,'2147483647'),('uuuuuu',31,'Juan Luciano ','García','Perez',0,'4425877319'),('Yaelultimo',30,'Juan','García','Perez',0,'4425877319');
/*!40000 ALTER TABLE `organizador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organizador_torneo`
--

DROP TABLE IF EXISTS `organizador_torneo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `organizador_torneo` (
  `fechaLarga` date NOT NULL,
  `nombreUsuario` varchar(20) NOT NULL,
  `idTorneo` int(5) NOT NULL,
  PRIMARY KEY (`nombreUsuario`,`idTorneo`,`fechaLarga`),
  KEY `nombreUsuario` (`nombreUsuario`),
  KEY `idTorneo` (`idTorneo`),
  CONSTRAINT `organizador_torneo_fk_1` FOREIGN KEY (`nombreUsuario`) REFERENCES `organizador` (`nombreUsuario`),
  CONSTRAINT `organizador_torneo_fk_2` FOREIGN KEY (`idTorneo`) REFERENCES `torneo` (`iDTorneo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organizador_torneo`
--

LOCK TABLES `organizador_torneo` WRITE;
/*!40000 ALTER TABLE `organizador_torneo` DISABLE KEYS */;
INSERT INTO `organizador_torneo` VALUES ('2020-04-15','organizador1',1);
/*!40000 ALTER TABLE `organizador_torneo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participante`
--

DROP TABLE IF EXISTS `participante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `participante` (
  `idParticipante` int(5) NOT NULL AUTO_INCREMENT,
  `nombreUsuario` varchar(20) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `apellidoPaterno` varchar(20) NOT NULL,
  `apellidoMaterno` varchar(20) NOT NULL,
  `handicap` float NOT NULL,
  `sexo` varchar(10) NOT NULL,
  `edad` int(3) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `correo` varchar(50) NOT NULL,
  PRIMARY KEY (`idParticipante`),
  KEY `nombreUsuario` (`nombreUsuario`),
  CONSTRAINT `participante_fk_1` FOREIGN KEY (`nombreUsuario`) REFERENCES `cuenta` (`nombreUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participante`
--

LOCK TABLES `participante` WRITE;
/*!40000 ALTER TABLE `participante` DISABLE KEYS */;
INSERT INTO `participante` VALUES (8,'emsaguilera','juan','fbdgfhjkj','szrgdhfjgkhd',21.9,'Hombre',23,'1234567890','cdfdvbgnhfbgd@ddcdbusd'),(9,'emsaguilera','dsvfb','sdfb','fsgd',2,'Hombre',35,'2345676543','scdvfb@guhd'),(10,'emsaguilera','Hola','dvfbc','vfbg',31,'Hombre',42,'3456786544','dfc@ncudc'),(11,'emsaguilera','srgdf','dsgfhg','sgzdxf',23.3,'Hombre',37,'2345678765','sdgfv@hbvivsdh'),(18,'prueba1','Eric Fernando ','Torres a','Rodríguez',10.9,'Mujer',20,'4422198979','erictorres_45@hotmail.com'),(19,'emsaguilera','Emilio','Aguiler','Carlin',12,'Hombre',22,'2345678876','ascdvs@vsdbd'),(20,'pruebaPresent','Juan','Perez','Perez',20,'Hombre',26,'1234567891','A01700190@itesm.mx'),(21,'emsaguilera','steve','rogers','cap',31,'Hombre',100,'1236575886','capamerica@avenger.com'),(24,'victorprueba','Victor','Rey','y',6,'Hombre',39,'4443456767','victor.rey@gmail.com'),(25,'victorprueba','mario','jimenez','prueba',8,'Hombre',38,'4427897890','marioj@hotmail.com'),(26,'victorprueba','Maria de Lourdes','Enriquez','Aguilar',0.5,'Hombre',28,'3456789087','lourdesaer@gmail.com'),(27,'victorprueba','5','5','5',5,'Hombre',5,'4564567890','hola@gmail.com'),(33,'jules','Julio','Di','bella',27,'Hombre',20,'4754646846','1234@12345'),(34,'jules','sdfb','df','fd',0.5,'Hombre',43,'5345788888','1234@12345'),(35,'emsaguilera','ad','sg','sdf',22,'Hombre',33,'4516541651','1234@12345'),(36,'prueba1','Eric','Test','Test',10,'Hombre',23,'4422198979','erictorres_45@hotmail.com'),(37,'nahimmedto','Nahim','Medellin','Torres',25,'Mujer',20,'4422742333','nahim.torres@yahoo.com'),(38,'nahimmedto','Nahim','Medellin','Torres',15,'Mujer',20,'4443176342','nahim.torres@yahoo.com'),(39,'nahimmedto','Nahim','Medellin','Torres',4,'Mujer',20,'4422742333','nahim.torres@yahoo.com'),(40,'nahimmedto','Nahim','Medellin','Torres',5,'Mujer',20,'4422742333','nahim.torres@yahoo.com'),(41,'prueba1','Fernanda','Arauna','Camarena',0,'Mujer',21,'4422198977','eftr149@outlook.com'),(42,'prueba1','a','a','a',0,'Hombre',23,'4422198979','daniel.alex17@hotmail.com');
/*!40000 ALTER TABLE `participante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participante_equipo_torneo`
--

DROP TABLE IF EXISTS `participante_equipo_torneo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `participante_equipo_torneo` (
  `idParticipante` int(5) NOT NULL,
  `idTorneo` int(5) NOT NULL,
  `idEquipo` int(5) NOT NULL,
  PRIMARY KEY (`idParticipante`,`idTorneo`,`idEquipo`),
  KEY `idParticipante` (`idParticipante`),
  KEY `idTorneo` (`idTorneo`),
  KEY `idEquipo` (`idEquipo`),
  CONSTRAINT `participante_equipo_torneo_fk_1` FOREIGN KEY (`idParticipante`) REFERENCES `participante` (`idParticipante`),
  CONSTRAINT `participante_equipo_torneo_fk_2` FOREIGN KEY (`idTorneo`) REFERENCES `torneo` (`iDTorneo`),
  CONSTRAINT `participante_equipo_torneo_fk_3` FOREIGN KEY (`idEquipo`) REFERENCES `equipo` (`idEquipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participante_equipo_torneo`
--

LOCK TABLES `participante_equipo_torneo` WRITE;
/*!40000 ALTER TABLE `participante_equipo_torneo` DISABLE KEYS */;
INSERT INTO `participante_equipo_torneo` VALUES (10,1,49),(11,1,50),(18,1,56),(19,1,50),(20,1,50),(21,1,50),(24,1,51),(25,1,51),(26,1,51),(27,1,52),(33,1,52),(41,1,56),(42,1,56);
/*!40000 ALTER TABLE `participante_equipo_torneo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participante_torneo`
--

DROP TABLE IF EXISTS `participante_torneo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `participante_torneo` (
  `folio` int(10) NOT NULL AUTO_INCREMENT,
  `idParticipante` int(5) NOT NULL,
  `idTorneo` int(5) NOT NULL,
  `indicacionEspecial` text NOT NULL,
  `metodoPago` varchar(100) NOT NULL,
  `facturacion` varchar(10) NOT NULL,
  `autorizarFotos` varchar(5) NOT NULL,
  `RFC` varchar(15) NOT NULL,
  `razonSocial` varchar(50) NOT NULL,
  `domicilioFiscal` varchar(180) NOT NULL,
  `comentariosFacturacion` text NOT NULL,
  `idFotoRecibo` varchar(50) NOT NULL,
  `indicacionPagoEspecial` text NOT NULL,
  `estatusInscripcion` varchar(20) NOT NULL,
  `estatusPago` varchar(10) NOT NULL,
  `asistenciaAlTorneo` varchar(10) NOT NULL,
  `fechaLarga` datetime DEFAULT CURRENT_TIMESTAMP,
  `inactivo` int(1) DEFAULT NULL,
  PRIMARY KEY (`folio`,`idParticipante`,`idTorneo`),
  KEY `idParticipante` (`idParticipante`),
  KEY `idTorneo` (`idTorneo`),
  CONSTRAINT `participante_torneo_fk_1` FOREIGN KEY (`idParticipante`) REFERENCES `participante` (`idParticipante`),
  CONSTRAINT `participante_torneo_fk_2` FOREIGN KEY (`idTorneo`) REFERENCES `torneo` (`iDTorneo`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participante_torneo`
--

LOCK TABLES `participante_torneo` WRITE;
/*!40000 ALTER TABLE `participante_torneo` DISABLE KEYS */;
INSERT INTO `participante_torneo` VALUES (5,8,1,'','Transferencia electrónica','No','Sí','','','','','de_paseo.png','','Inscrito','Pagado','Sí',NULL,0),(6,9,1,'','Pago en efectivo el día del evento','No','No','','','','','','','Pendiente','Pendiente','No',NULL,0),(7,10,1,'','Pago en efectivo previo al evento','No','Sí','','','','','','','Pendiente','Pendiente','No',NULL,0),(8,11,1,'','Pago en efectivo el día del evento','Sí','No','qwerftgyhjggd','12345654323456787654321123456y78876543211234567890','wdefrgdfnghcdsfv, ds, 1234567654321eb, gvndxfgh, afdfgvhbj,sfdvfbzdfbddf, 23456','','poster_oficial.jpg','','Inscrito','Pagado','No',NULL,0),(15,18,1,'','Depósito en efectivo','No','No','','','','','','','Pendiente','Pendiente','No',NULL,0),(16,19,1,'','Transferencia electrónica','No','No','','','','','_102816316_tiburon3.jpg','','Inscrito','Pagado','Sí','2020-05-19 02:40:39',0),(17,20,1,'','Pago en efectivo previo al evento','No','Sí','','','','','','Subiré después mi archivo','Inscrito','Pagado','Sí','2020-05-19 08:58:43',0),(18,21,1,'','Depósito en efectivo','No','Sí','','','','','DesertedJadedEasternglasslizard-small.gif','','Pendiente','Pendiente','No','2020-05-23 21:08:42',0),(21,24,1,'salir del hoyo 1','Transferencia electrónica','Sí','Sí','RELV801404123','ddfgdf435','sds5, , casa, Querétaro, Querétaro, 76140','','','','Pendiente','Pendiente','No','2020-05-27 11:10:58',NULL),(22,25,1,'equipo de victor rey','Cargo a tarjeta de crédito','No','Sí','','','','','','','Pendiente','Pendiente','No','2020-05-27 11:12:18',NULL),(23,26,1,'','Cargo a tarjeta de crédito','Sí','Sí','dflg456756ty7','flako jimenes','Blvd Bernardo Quintana','','','','Pendiente','Pendiente','No','2020-05-27 11:16:25',NULL),(24,27,1,'','Depósito en efectivo','No','No','','','','','','','Pendiente','Pendiente','No','2020-05-27 11:32:16',NULL),(27,33,1,'','Transferencia electrónica','No','No','','','','','','','Pendiente','Pendiente','No','2020-05-29 23:52:46',NULL),(28,34,1,'','Pago en efectivo previo al evento','No','No','','','','','','','Pendiente','Pendiente','No','2020-05-29 23:53:41',NULL),(29,35,1,'','Pago en efectivo el día del evento','No','No','','','','','1578796096742.png','','Pendiente','Pendiente','No','2020-05-29 23:55:41',NULL),(34,40,1,'','Depósito en efectivo','Sí','No','VECJ880326l85','LALA','Epigmenio513, 14, Real, San Luis Potosí, San Luis Potosi, 78216','Grax amix','WhatsApp Image 2020-05-21 at 4.01.07 PM.jpeg','','Pendiente','Pendiente','No','2020-06-05 19:08:42',NULL),(35,41,1,'','Transferencia electrónica','No','Sí','','','','','','','Pendiente','Pendiente','No','2020-06-07 02:11:03',NULL),(36,42,1,'','Transferencia electrónica','No','Sí','','','','','','','Pendiente','Pendiente','No','2020-06-07 08:18:42',NULL);
/*!40000 ALTER TABLE `participante_torneo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patrocinador`
--

DROP TABLE IF EXISTS `patrocinador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `patrocinador` (
  `nombreEmpresa` varchar(80) NOT NULL,
  `nombreUsuario` varchar(40) DEFAULT NULL,
  `nombreContacto` varchar(80) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `descripcionEmpresa` text NOT NULL,
  `descripcionInteres` text NOT NULL,
  `estatusPatrocinio` varchar(10) NOT NULL,
  `patrocinioObtenido` text,
  `fechaLarga` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`nombreEmpresa`,`fechaLarga`),
  KEY `nombreUsuario` (`nombreUsuario`),
  CONSTRAINT `patrocinador_fk_1` FOREIGN KEY (`nombreUsuario`) REFERENCES `organizador` (`nombreUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patrocinador`
--

LOCK TABLES `patrocinador` WRITE;
/*!40000 ALTER TABLE `patrocinador` DISABLE KEYS */;
INSERT INTO `patrocinador` VALUES ('AAAAA',NULL,'Eric','4422198977','erictorres1727@gmail.com','DASDSAD','sdasasasdfas','Pendiente',NULL,'2020-06-06 16:38:33','Logotipo_Vertical_Negro_Sin_Fondo.png'),('asdasdasd',NULL,'asfasdad','4422198977','erictorres_45@hotmail.com','asdasdasd','adasdasd','Pendiente',NULL,'2020-06-06 16:39:30','Logotipo_Vertical_Negro_Sin_Fondo.png'),('EINEC',NULL,'Pepe','4422222222','A01700486@itesm.mx','la + chida','Unos cheetos','Pendiente',NULL,'2020-06-06 17:12:08','Logotipo_Vertical_Negro_Sin_Fondo.png'),('EINEC',NULL,'Ptrocinador','4422222222','A01700486@itesm.mx','Descripcion','Unos cheetos','Pendiente',NULL,'2020-06-06 19:22:26','Logotipo_Vertical_Negro_Sin_Fondo.png'),('EINEC',NULL,'Ptrocinador','4422222222','erictorres_45@hotmail.com','Descripcion','Unos cheetos','Pendiente',NULL,'2020-06-06 19:24:19','Logotipo_Vertical_Negro_Sin_Fondo.png'),('EINEC',NULL,'Ptrocinador','4422222222','emichiva_99@hotmail.com','Descripcion','Unos cheetos','Pendiente',NULL,'2020-06-06 19:53:39','Logotipo_Vertical_Negro_Sin_Fondo.png'),('EINEC',NULL,'Ptrocinador','4422222222','torneo_cmg@outlook.com','Descripcion','Unos cheetos','Pendiente',NULL,'2020-06-06 19:56:39','Logotipo_Vertical_Negro_Sin_Fondo.png'),('EINEC',NULL,'Ptrocinador','4422222222','torneo_cmg@outlook.com','Descripcion','Unos cheetos','Pendiente',NULL,'2020-06-06 19:59:22','Logotipo_Vertical_Negro_Sin_Fondo.png'),('EINEC',NULL,'Ptrocinador','4422222222','Alinitao@hotmail.com','Descripcion','Unos cheetos','Pendiente',NULL,'2020-06-06 20:08:42','Logotipo_Vertical_Negro_Sin_Fondo.png'),('EINEC',NULL,'Ptrocinador','4422222222','torneo_cmg@outlook.com','Descripcion','Unos cheetos','Pendiente',NULL,'2020-06-06 20:21:20','Logotipo_Vertical_Negro_Sin_Fondo.png'),('Empresa',NULL,'Oscar','4421547779','micorreo@gmail.com','Mi empresa','Me interesa','Aceptado','10 chicles','2020-05-09 15:30:56','ETu-734X0AIJtuW.jpg'),('Empresa S.A.',NULL,'Juanito Díaz','4421547779','micorreo@gmail.com','Somos una empresa','Buena razón','Pendiente',NULL,'2020-05-16 22:00:03','Logotipo_Vertical_Negro_Sin_Fondo.png'),('Empresa S.A.',NULL,'Juanito Díaz','4422198977','micorre','Somos una empresa','Buena razon','Pendiente',NULL,'2020-05-16 22:06:23','Logotipo_Vertical_Negro_Sin_Fondo.png'),('Empresa S.A.',NULL,'Juanito Díaz','4422198977','micorre','Somos una empresa','Buena razon','Pendiente','','2020-05-16 22:06:28','Logotipo_Vertical_Negro_Sin_Fondo.png'),('EMS Corp.',NULL,'Emilio','1246654616','emsaguilera99@gmail.com','Empresa bien chida','1 Eric y 1 Ivan ','Pendiente',NULL,'2020-06-04 15:04:29','EN TU CANCHA 123.png'),('EMS Corp.',NULL,'Ems','2352787645','emsaguilera99@gmail.com','Empresa de empresas','5 erics y 2 ivanes','Pendiente',NULL,'2020-06-04 15:18:11','EN TU CANCHA 123.png'),('EMS Corp.','hulk123','ems','6544468388','emichiva_99@hotmail.com','Empresa de tecnología','5 erics y 2 ivanes','Aceptado','','2020-06-06 19:49:52','EN TU CANCHA 123.png'),('Eric Inc.','hulk123','Eric Torres','4422198977','ericinc@hotmail.com','Empresa dedicada al serivcio','Quisiera brindar camaras para el evento.','Rechazado','','2020-05-18 12:54:08','Logotipo_Vertical_Negro_Sin_Fondo.png'),('Eric Torres Inc.','hulk123','Eric Torres','4422198977','ericgorras@gmail.com','Empresa dedicada a fabricar gorras','Quisiera donar 300 gorras','Aceptado','','2020-05-19 12:12:32','_102816316_tiburon3.jpg'),('EricINC',NULL,'Eric','4422198977','eftr249@outlook.com','Empresa chida','bla bla bla','Pendiente',NULL,'2020-06-06 16:25:34','Logotipo_Vertical_Negro_Sin_Fondo.png'),('IVANCO',NULL,'Ivan El Grande','6514894684','A01700486@itesm.mx','Manufacturera de ivanes','22 erics','Pendiente',NULL,'2020-06-06 19:46:27','Logotipo_Vertical_Negro_Sin_Fondo.png'),('LALA',NULL,'Nahim','4443176342','nahim.torres@yahoo.com','Empresa shida','Ay pues muxas cosas wuuu','Pendiente',NULL,'2020-06-05 19:15:08','Logotipo_Vertical_Negro_Sin_Fondo.png'),('Lozano',NULL,'Lalo','4422222222','pruebaDAW@outlook.com','Descripcion','Unos cheetos','Pendiente',NULL,'2020-06-07 01:16:30','Logotipo_Vertical_Negro_Sin_Fondo.png'),('Lozano',NULL,'Lalo','4422222222','pruebadaw@outlook.com','Descripcion','Unos cheetos','Pendiente',NULL,'2020-06-07 01:17:57','Logotipo_Vertical_Negro_Sin_Fondo.png'),('Patrocinios S.A',NULL,'Ptrocinador','4422222222','micorreo@gmail.com','Descripcion','Me interesa','Aceptado','180 gorras','2020-05-09 17:31:46','Logotipo_Horizontal_Azul_Sin_Fondo.png'),('Patrocinios S.A',NULL,'Ptrocinador','4422222222','micorreo@gmail.com','Descripcion','Muy interesado','Pendiente',' ','2020-05-10 14:22:04','Logotipo_Horizontal_Azul_Sin_Fondo.png'),('Patrocinios S.A',NULL,'Patrocinador','4422222222','micorreo@gmail.com','Descripcion','Okay','Aceptado','20 botellas de agua','2020-05-13 12:49:49','Logotipo_Horizontal_Azul_Sin_Fondo.png'),('Patrocinios S.A',NULL,'Ptrocinador','4422222222','micorreo@gmail.com','Descripcion','Me interesa','Aceptado','8 chicles','2020-05-13 19:43:38','Logotipo_Horizontal_Azul_Sin_Fondo.png'),('Patrocinios S.A',NULL,'Ptrocinador','4422222222','micorreo@gmail.com','Descripcion','Me gusta su torneo','Aceptado','12 chicles','2020-05-14 15:13:56','Logotipo_Horizontal_Azul_Sin_Fondo.png'),('Patrocinios S.A',NULL,'Ptrocinador','4422222222','micorreo@gmail.com','Descripcion','Una buena Razón','Pendiente',NULL,'2020-05-16 22:54:41','Logotipo_Horizontal_Azul_Sin_Fondo.png'),('Patrocinios S.A',NULL,'Juan','4422222222','micorreo@gmail.com','Descripcion','Muy interesado','Pendiente','','2020-05-17 18:25:41','Logotipo_Horizontal_Azul_Sin_Fondo.png'),('Patrocinios S.A',NULL,'Ptrocinador','4422222222','micorreo@gmail.com','Descripcion','Me interesa','Aceptado','20 Chicles','2020-05-17 18:29:31','Logotipo_Horizontal_Azul_Sin_Fondo.png'),('Patrocinios S.A',NULL,'Ptrocinador','4422222222','micorreo@gmail.com','Descripcion','Me interesa','Pendiente',NULL,'2020-05-17 19:14:53','Logotipo_Horizontal_Azul_Sin_Fondo.png'),('Patrocinios S.A',NULL,'Iván','4422222222','peraltaalejandro22@gmail.com','Descripcion','200 chicles','Pendiente',NULL,'2020-05-23 20:10:17','Logotipo_Horizontal_Azul_Sin_Fondo.png'),('Patrocinios S.A',NULL,'Ptrocinador','4422222222','eftr249@outlook.com','Descripcion','Unos cheetos','Pendiente',NULL,'2020-06-06 18:57:54','Logotipo_Horizontal_Azul_Sin_Fondo.png'),('Patrocinios S.A',NULL,'Ptrocinador','4422222222','eftr249@outlook.com','Descripcio','Unos cheetos','Pendiente',NULL,'2020-06-06 19:20:15','Logotipo_Horizontal_Azul_Sin_Fondo.png'),('Patrocinios S.A',NULL,'Ptrocinador','4422222222','peraltaalejandro2299@gmail.com','Descripcio','Unos cheetos','Pendiente',NULL,'2020-06-06 19:21:08','Logotipo_Horizontal_Azul_Sin_Fondo.png'),('Patrocinios S.A',NULL,'Ptrocinador','4422222222','torneo_cmg@outlook.com','Descripcion','Unos cheetos','Pendiente',NULL,'2020-06-06 20:49:06','Logotipo_Horizontal_Azul_Sin_Fondo.png'),('Patrocinios S.A',NULL,'Ptrocinador','4422222222','pruebadaw@outlook.com','Descripcion','Unos cheetos','Pendiente',NULL,'2020-06-07 01:24:23','Logotipo_Horizontal_Azul_Sin_Fondo.png'),('Patrocinios S.A',NULL,'Ptrocinador','4422222222','pruebaDAW@outlook.com','Descripcion','Unos cheetos','Pendiente',NULL,'2020-06-07 01:25:17','Logotipo_Horizontal_Azul_Sin_Fondo.png'),('Patrocinios S.A',NULL,'Ptrocinador','4422222222','peraltaalejandro2299@gmail.com','Descripcion','Unos cheetos','Pendiente',NULL,'2020-06-07 01:25:31','Logotipo_Horizontal_Azul_Sin_Fondo.png'),('Stark Industries',NULL,'Tony Stark','2345677765','stark@avenger.com','Empresa de tecnología','5 trajes IronMan','Pendiente',NULL,'2020-05-23 21:06:58','Logotipo_Vertical_Negro_Sin_Fondo.png'),('Stark Industries',NULL,'Tony Stark','2352787645','emsaguilera99@gmail.com','Empresa de tecnología','5 trajes IronMan','Pendiente',NULL,'2020-06-02 14:55:35','Logotipo_Vertical_Negro_Sin_Fondo.png'),('Telmex','hulk123','Nahim','1234567891','nahimtorres@yahoo.com','servicios','120 gorras','Aceptado','120 gorras','2020-05-19 09:03:45','PERFIL CENMUN CON TRANSPARENCIA.png');
/*!40000 ALTER TABLE `patrocinador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `premio`
--

DROP TABLE IF EXISTS `premio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `premio` (
  `idPremio` int(5) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(50) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `hoyo` smallint(5) DEFAULT NULL,
  `posicion` varchar(50) DEFAULT NULL,
  `idTorneo` smallint(5) DEFAULT NULL,
  PRIMARY KEY (`idPremio`),
  KEY `premio_fk1` (`idTorneo`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `premio`
--

LOCK TABLES `premio` WRITE;
/*!40000 ALTER TABLE `premio` DISABLE KEYS */;
INSERT INTO `premio` VALUES (1,'Hole-in-one','Televisión Smart LG',NULL,'1ero',1),(2,'O\'Yes','Coche 2020',12,'2do',1),(3,'O\'Yes','Carrito de golf',12,'3ero',1),(4,'O\'Yes','Nintendo',5,'2do',1),(5,'O\'Yes','Tenis Yeezys',12,'1ero',1),(6,'Por equipo','Televisión LG',NULL,'1ero',1),(7,'Por equipo','Coche 2020',0,'2do',2),(8,'Por equipo','Carrito de golf',NULL,'3ero',1),(9,'Por equipo','Nintendo',NULL,'2do',1),(10,'Por equipo','Tenis Yeezys',NULL,'1ero',1),(13,'Hole-in-one','Carrito de golf',7,'',1),(14,'Hole-in-one','Nintendo',10,'',1),(16,'1','Hole-in-one',0,'',0),(17,'1','Hole-in-one',0,'',0),(18,'1','O\'Yes',0,'',0),(19,'1','O\'Yes',0,'',0),(21,'1','Hole-in-one',0,'',0),(22,'1','Hole-in-one',0,'',0),(24,'O\'Yes','Samsung 37+',15,'1ero',0),(25,'Hole-in-one','Samsung 37+',6,'1ero',0),(26,'Por equipo','Samsung 37+',3,'1ero',0),(27,'O\'Yes','Emilio es 1 dios',7,'2do',0),(29,'Hole-in-one','Samsung 37+',17,'8vo',1),(30,'Hole-in-one','pruebaaaa',12,'52',1),(32,'Hole-in-one','Samsung 37+',4,'52',1),(34,'O\'Yes','Samsung 37+',5,'5to',1),(35,'O\'Yes','Huawei P30',1,'8vo',1);
/*!40000 ALTER TABLE `premio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `premio_torneo`
--

DROP TABLE IF EXISTS `premio_torneo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `premio_torneo` (
  `idPremioTorneo` int(5) NOT NULL AUTO_INCREMENT,
  `idPremio` smallint(5) DEFAULT NULL,
  `iDTorneo` smallint(5) DEFAULT NULL,
  PRIMARY KEY (`idPremioTorneo`),
  KEY `premio_torneo_pk1` (`idPremio`),
  KEY `premio_torneo_pk2` (`iDTorneo`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `premio_torneo`
--

LOCK TABLES `premio_torneo` WRITE;
/*!40000 ALTER TABLE `premio_torneo` DISABLE KEYS */;
INSERT INTO `premio_torneo` VALUES (1,1,1);
/*!40000 ALTER TABLE `premio_torneo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `privilegio`
--

DROP TABLE IF EXISTS `privilegio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `privilegio` (
  `IdPrivilegio` int(5) NOT NULL AUTO_INCREMENT,
  `nombrePrivilegio` varchar(40) NOT NULL,
  `Descripcion` varchar(80) NOT NULL,
  PRIMARY KEY (`IdPrivilegio`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `privilegio`
--

LOCK TABLES `privilegio` WRITE;
/*!40000 ALTER TABLE `privilegio` DISABLE KEYS */;
INSERT INTO `privilegio` VALUES (1,'puedeVerInformacion','El privilegio permite ver informacion en la pagina\r\n'),(2,'puedeConsultarRegistros','El privilegio permite consultar registros de la pagina\r\n'),(3,'puedeEditarRegistros','El privilegio permite editar registros de la paginna\r\n'),(4,'puedeEditarTorneo','El privilegio permite editar informacion del torneo, ubicación premios etc.\r\n'),(5,'puedeAceptarPatrocinadores','El privilegio permite consultar patrocinadores y confrimarlos\r\n'),(6,'puedeBorrarTorneo','El privilegio permite borrar un torneo\r\n'),(7,'puedeSubirFotos','El privilegio permite subir arhivos multimedia\r\n'),(8,'BorrarOrganizador','Borra a otro organizador');
/*!40000 ALTER TABLE `privilegio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recursomultimedia`
--

DROP TABLE IF EXISTS `recursomultimedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `recursomultimedia` (
  `idRecurso` int(5) NOT NULL AUTO_INCREMENT,
  `idTorneo` int(5) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `descripcion` text NOT NULL,
  `URL` varchar(100) NOT NULL,
  PRIMARY KEY (`idRecurso`),
  KEY `idTorneo` (`idTorneo`),
  CONSTRAINT `recursomultimedia_fk_1` FOREIGN KEY (`idTorneo`) REFERENCES `torneo` (`iDTorneo`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recursomultimedia`
--

LOCK TABLES `recursomultimedia` WRITE;
/*!40000 ALTER TABLE `recursomultimedia` DISABLE KEYS */;
INSERT INTO `recursomultimedia` VALUES (6,1,'3er torneo','foto de juanito','25-252040_the-hunger-games-png-transparent-images-hunger-games.png'),(7,1,'foto del torneo 2020','foto de emilio feliz','56.JPG'),(8,1,'','','44.JPG'),(9,1,'4to torneo','Panchito saliendo de hoyo 4','51.JPG');
/*!40000 ALTER TABLE `recursomultimedia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rol` (
  `IdRol` int(5) NOT NULL AUTO_INCREMENT,
  `nombreRol` varchar(40) DEFAULT NULL,
  `Descripcion` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`IdRol`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (1,'participante','participante del torneo'),(2,'organizador','organizador del torneo'),(3,'Admin','AdministraOrganizadoes');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol_privilegio`
--

DROP TABLE IF EXISTS `rol_privilegio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rol_privilegio` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `IdRol` int(5) NOT NULL,
  `IdPrivilegio` int(5) NOT NULL,
  PRIMARY KEY (`id`,`IdRol`,`IdPrivilegio`),
  KEY `IdRol` (`IdRol`),
  KEY `IdPrivilegio` (`IdPrivilegio`),
  CONSTRAINT `rol_privilegio_fk_1` FOREIGN KEY (`IdRol`) REFERENCES `rol` (`IdRol`),
  CONSTRAINT `rol_privilegio_fk_2` FOREIGN KEY (`IdPrivilegio`) REFERENCES `privilegio` (`IdPrivilegio`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol_privilegio`
--

LOCK TABLES `rol_privilegio` WRITE;
/*!40000 ALTER TABLE `rol_privilegio` DISABLE KEYS */;
INSERT INTO `rol_privilegio` VALUES (1,1,1),(9,1,7),(2,2,1),(3,2,2),(4,2,3),(5,2,4),(6,2,5),(7,2,6),(8,2,7);
/*!40000 ALTER TABLE `rol_privilegio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `torneo`
--

DROP TABLE IF EXISTS `torneo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `torneo` (
  `iDTorneo` int(5) NOT NULL AUTO_INCREMENT,
  `fechaTorneo` date NOT NULL,
  `horaRegistro` varchar(10) NOT NULL,
  `horaInicio` varchar(10) NOT NULL,
  `lugar` varchar(100) NOT NULL,
  `costo` int(20) NOT NULL,
  `cierreTorneo` date NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `preventa` int(20) DEFAULT NULL,
  `fechaPreventa` date DEFAULT NULL,
  `fechaCosto` date DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  `cuenta` int(20) DEFAULT NULL,
  `clabe` int(20) DEFAULT NULL,
  `banco` varchar(50) DEFAULT NULL,
  `beneficiario` varchar(50) DEFAULT NULL,
  `poster` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`iDTorneo`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `torneo`
--

LOCK TABLES `torneo` WRITE;
/*!40000 ALTER TABLE `torneo` DISABLE KEYS */;
INSERT INTO `torneo` VALUES (1,'2020-06-23','09:42 AM','09:42 AM','Campo de Golf Campestre',1200,'2020-06-23','10° Torneo de Golf',2800,'2020-06-10','2020-06-10',1,2147483647,2147483647,'Bancomer','Ana Luisa P.','poster_oficial.jpg'),(2,'2020-04-18','10:00','11:00','Campo ...',1500,'2020-05-18',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL),(3,'0000-00-00','10:00 AM','09:01 AM','Querétaro',300,'0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'0000-00-00','10:00 AM','09:00 AM','Querétaro',1000,'0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'2020-05-10','12:00 PM','12:15 PM','campes',1200,'2020-05-30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'2020-05-27','04:20 PM','07:20 PM','qrooo',100,'2020-05-31',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'2020-05-27','04:20 PM','07:20 PM','qrooo',100,'2020-05-31',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,'2020-05-27','04:20 PM','07:20 PM','qrooo',100,'2020-05-31',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,'2020-04-29','09:00 AM','12:00 PM','Campo Golf',1000,'2020-04-30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,'2020-04-29','09:00 AM','12:00 PM','Campo Golf',1000,'2020-04-30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,'0000-00-00','09:00 AM','12:00 PM','Campo Golf',1000,'2020-04-30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,'0000-00-00','09:00 AM','12:00 PM','Campo Golf',1000,'2020-04-30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,'0000-00-00','','','',0,'0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,'0000-00-00','09:00 AM','12:00 PM','Campo Golf',1000,'2020-04-30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,'2020-04-29','09:00 AM','12:00 PM','Campo Golf',0,'2020-05-30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,'2020-04-29','09:00 AM','12:00 PM','',1000,'2020-04-30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,'0000-00-00','','','',0,'0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,'2020-05-19','05:31 PM','04:29 PM','Club Campestre',1300,'2020-05-20',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,'2020-05-28','06:01 AM','08:01 PM','Campo Golf',1000,'2020-05-31',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,'2020-05-20','04:23 PM','03:30 PM','Club Campestre de Querétaro',1300,'2020-05-28',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,'2020-05-21','07:30 AM','06:20 AM','Club Campestre de Querétaro',1300,'2020-05-31',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,'2020-05-29','06:30 PM','06:30 PM','qqq',1234,'2020-05-31',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,'2020-05-19','06:37 AM','06:40 AM','qroo',666,'2020-05-26','',0,'0000-00-00','0000-00-00',0,0,0,'','',NULL),(24,'2020-05-28','08:45 AM','12:59 AM','ira',300,'2020-05-31','',0,'0000-00-00','0000-00-00',0,0,0,'','',NULL),(25,'2020-05-11','04:47 PM','01:47 PM','121',1223,'2020-05-20',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(26,'2020-05-19','05:49 PM','01:49 PM','123',123,'2020-05-29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(27,'2020-05-19','05:49 PM','01:49 PM','123',123,'2020-05-29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(28,'2020-05-19','05:49 PM','01:49 PM','123',123,'2020-05-29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(29,'2020-06-09','04:10 PM','11:11 PM','deathstar',500,'2020-06-30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(30,'2020-06-08','12:45 PM','08:45 PM','ssd',242,'2020-06-30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(31,'2020-06-02','05:50 PM','07:50 PM','qrooo',1244,'2020-06-30','torneo',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(32,'2020-06-02','11:05 PM','11:05 PM','daf',235,'2020-06-30','13 torneo',264,'2020-06-30','2020-06-30',0,123456,1548468465,'Santander','1',NULL),(33,'0000-00-00','','','en tu cora',2525,'0000-00-00','MEGA TORNEO',235,'2020-06-11','2020-06-11',0,325525,25352,'banco','0',NULL),(34,'2020-06-25','06:27 PM','07:27 PM','Club Campeste',2800,'2020-06-25','9no Torneo',2500,'2020-06-09','2020-06-10',0,123456897,1849865461,'Bancomer','Ana de La B.',NULL);
/*!40000 ALTER TABLE `torneo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-07 22:53:07
