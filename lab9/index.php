<?php
  //Funciones
  function generarTabla($num) 
  {
    echo "<table><thead><tr><td>Numero</td><td>Cuadrado</td><td>Cubo</td></tr></thead><tbody>";
    $x = 1;
    do {
      echo"<tr><td>";
      echo $x;
      echo "</td><td>";
      echo $x*$x;
      echo "</td><td>";
      echo $x*$x*$x;
      echo"</td></tr>";
      $x++;
    }while ($x <= $num);
  echo"</tbody></table>";
  }
  function promedio(){
    $numbers = array(4, 6, 2, 22, 11);
    $promedio =0;
    $suma=0;
    echo"<p>Numeros: ";
    for($x = 0; $x <= count($numbers)-1; $x++) {
      echo $numbers[$x];
      echo", ";
      $suma= $suma+ $numbers[$x];
    }
    $promedio= $suma/ count($numbers);
    echo"</p><br><p><strong>Promedio: </strong>";
    echo $promedio;
    echo "</p>";
  }
  function mediana(){
    $numbers = array(4, 6, 2, 22, 11);
    sort($numbers);
    $mediana=0;
    echo"<p>Numeros: ";
    for($x = 0; $x <= count($numbers)-1; $x++) {
      echo $numbers[$x];
      echo", ";
    }
    if( count($numbers)%2==0){
      $mediana= ($numbers[(count($numbers)/2)]+$numbers[(count($numbers)/2)+1])/2;
    }else{
      $mediana = $numbers[(int)(count($numbers)/2)];
    }
    echo"</p><br><p><strong>Mediana: </strong>";
    echo $mediana;
    echo "</p>";
  }
  function listaNumeros(){
    $numbers = array(4, 6, 2, 22, 11);
    $mediana=0;
    $promedio =0;
    $suma=0;
    echo"<p>Numeros: ";
    for($x = 0; $x <= count($numbers)-1; $x++) {
      echo $numbers[$x];
      echo", ";
      $suma= $suma+ $numbers[$x];
    }
    sort($numbers);
    if( count($numbers)%2==0){
      $mediana= ($numbers[(count($numbers)/2)]+$numbers[(count($numbers)/2)+1])/2;
    }else{
      $mediana = $numbers[(int)(count($numbers)/2)];
    }
    $promedio= $suma/ count($numbers);
    echo"</p><br><li><strong>Promedio: </strong>";
    echo $promedio;
    echo "</li>";
    echo"<li><strong>Media: </strong>";
    echo $mediana;
    echo "</li>";
    echo"<li><strong>Arreglo en orden ascendente: </strong>";
    for($x = 0; $x <= count($numbers)-1; $x++) {
      echo $numbers[$x];
      echo", ";
      $suma= $suma+ $numbers[$x];
    }
    echo "</li>";
    echo"<li><strong>Arreglo en orden descendente: </strong>";
    rsort($numbers);
    for($x = 0; $x <= count($numbers)-1; $x++) {
      echo $numbers[$x];
      echo", ";
      $suma= $suma+ $numbers[$x];
    }
    echo "</li>";
  }
  
  //Estructura HTML
  include("_header.html");
  include("_navbar.html");    
  include("_promedioArreglos.html");
  promedio();
  include("_mediana.html");
  mediana();
  include("_listaNumeros.html");
  listaNumeros();
  include("_tablaCuadradosCubos.html");
  generarTabla(5);
  include("_problemaExtra.html");
  include("_preguntas.html");
  include("_referencias.html");
  include("_footer.html");
?>
