<?php
  require_once("util.php"); 
  
  if ( !(isset($_POST["nombre"]) && isset($_POST["clase"]) && isset($_POST["p_inteligencia"]) && isset($_POST["p_fe"])
  && isset($_POST["p_destreza"]) && isset($_POST["p_fuerza"]) && isset($_POST["p_vida"])
  && isset($_POST["p_aguante"]) && isset($_POST["pacto"]) )) {
    die();
  }

  
  $nombre = htmlspecialchars($_POST["nombre"]);
  $clase= htmlspecialchars($_POST["clase"]);
  $p_int = htmlspecialchars($_POST["p_inteligencia"]);
  $p_fe = htmlspecialchars($_POST["p_fe"]);
  $p_des = htmlspecialchars($_POST["p_destreza"]);
  $p_frz = htmlspecialchars($_POST["p_fuerza"]);
  $p_vd = htmlspecialchars($_POST["p_vida"]);
  $p_agn = htmlspecialchars($_POST["p_aguante"]);
  $pacto = htmlspecialchars($_POST["pacto"]);
  
  include("Vistas/_header.html");  
  include("Vistas/_navbar.html");    
  include("Vistas/_form.html");
  constuirTabla($nombre,$pacto,$p_int,$p_fe,$p_des,$p_frz,$p_vd,$p_agn,$clase);
  include("Vistas/_preguntas.html");
  include("Vistas/_referencias.html");
  include("Vistas/_footer.html");
?> 